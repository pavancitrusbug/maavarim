<?php
/**
 * Template Name: manage-contact
 */
get_header(); 

//get business contacts
$BC_contact_id = '';
$BC_contact_name = '';
$BC_contact_role = '';
$BC_contact_phone = '';
$BC_contact_email = '';
$BC_contact_contact_email = '';
$BC_contact_business_id = '';
global $wpdb;
if(array_key_exists('bcid', $_REQUEST)) {
    $BC_contact_id = $_REQUEST['bcid'];    
    $contact_table = $wpdb->prefix.'business_contacts';
    $query = 'SELECT * FROM '.$contact_table.' WHERE id = '.$BC_contact_id;
    $contacts = $wpdb->get_results($query);
    if(count($contacts) > 0) {
        $contact = $contacts[0];
        $BC_contact_name = $contact->name;
        $BC_contact_role = $contact->name_role;
        $BC_contact_phone = $contact->phone;
        $BC_contact_email = $contact->email;
        $BC_contact_contact_email = $contact->contact_email;
        $BC_contact_business_user_id = $contact->business_id;
    }
}
$checkEmailClass = 'check-email';
if (!empty($BC_contact_email)) {
    $checkEmailClass = '';
}
if(!session_id()) {
    session_start();
}
$user = $_SESSION['login_user_detail'];
$advertise_business_table = $wpdb->prefix.'advertise_business';
$user_table = $wpdb->prefix.'business_users';
$query = 'SELECT id FROM ' . $advertise_business_table . ' WHERE user_id = '.$user->id;
$businesses = $wpdb->get_results($query);
if(count($businesses) > 0) {
    $BC_contact_business_id = $businesses[0]->id;
}
$roles = array(
    '1' => 'Project Manager',
    '2' => 'HR'
);
    // foreach($contacts as $contact) {
?>
<script src="https://jqueryvalidation.org/files/lib/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>
<script src=<?php echo get_template_directory_uri()."/custom.validation.js"; ?>></script>
<section class="company-section">
    <div class="job-post-root-top">
        <div class="business-white-forms01 new-job-form01 new-job-post-form01">

            <div class="container">
                <div class="business-form-root-div">
                    <div class="bs-form-div">
                        <div class="white-header-top addBusiness-form-top color-update-newjob01">
                            <div class="addBusiness-first-line">
                                <h2>םישורדה חולב תורשמ םוסרפ ךרוצל םושיר ךשמה</h2>
                            </div>
                            <div class="addBusiness-second-line">
                                <span>.רשק ישנא/שיא רידגהל שי ,ךלש ליימל םידמעומ לש םייח תורוק תלבק ךרוצל</span>
                            </div>
                        </div>
                        <form class="" action='<?php echo get_template_directory_uri()."/database-process.php"; ?>' method="post" id="business_contact0" name="business_contact0" >
                        <input type="hidden" value="<?php echo $BC_contact_id; ?>" name="BC_business_contact_id" />
                        <input type="hidden" value="<?php echo $BC_contact_business_id; ?>" name="BC_business_id" />
                            <div class="white-body-form-div">
                                <div class="white-row-color-div business_details_body">
                                    <div class="inner-heading-div">
                                        <?php  if ($BC_contact_id != '') {
                                            echo '<h3>  ׳סמ רשק שיא 1 </h3>';
                                        } ?>                                    
                                    </div>
                                    <div class="form-row">
                                        <div class="grid-auto grid-33">
                                            <div class="form-group">
                                                <label class="control-label" for="">שם</label>
                                                <div class="input-field-group">
                                                    <input id="business_contact_name" name="business_contact_name" type="text" placeholder="" class="required form-control input-md" value="<?php echo $BC_contact_name; ?>" >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="grid-auto grid-33">
                                            <div class="form-group">
                                                <label class="col-md-4 control-label" for="">תפקיד</label>
                                                <div class="input-field-group">
                                                    <select id="business_contact_role" name="business_contact_role" class="required">
                                                    <?php 
                                                    foreach($roles as $Rkey=>$Rval) {
                                                            if (!empty($BC_contact_role) && $Rkey == $BC_contact_role) {
                                                                $selected = 'selected="selected"';
                                                                echo '<option value="'.$Rkey.'" '.$selected.'>'.$Rval.'</option>';
                                                            } else {
                                                                echo '<option value="'.$Rkey.'">'.$Rval.'</option>';
                                                            }
                                                        }
                                                    ?>
                                                    </select>
                                                    <span class="dropdown-arrow"></span>
                                                </div>
                                            </div>
                                        </div>						
                                        <div class="grid-auto grid-33">
                                            <div class="form-group">
                                                <label class="control-label" for="">טלפון</label>
                                                <div class="input-field-group">
                                                    <input id="business_contact_phone" name="business_contact_phone" type="text" placeholder="" class="required form-control input-md" value="<?php echo $BC_contact_phone; ?>">
                                                </div>
                                            </div>
                                        </div>						
                                    </div>
                                    <div class="form-row">
                                        <div class="grid-auto grid-50">
                                            <div class="form-group">
                                                <label class="col-md-4 control-label" for="">דוא"ל לקבלת קורות חיים</label>
                                                <div class="input-field-group">
                                                    <input id="business_contact_email_resume" name="business_contact_email_resume" data-identify="business_contacts" field-identify="email" type="email" placeholder="הכנס דוא״ל" class="required form-control input-md <?php echo $checkEmailClass; ?>" value="<?php echo $BC_contact_email; ?>" >
                                                </div>
                                            </div>
                                        </div>						
                                        <div class="grid-auto grid-50">
                                            <div class="form-group">
                                                <label class="col-md-4 control-label" for="">דוא"ל אישי של איש הקשר</label>
                                                <div class="input-field-group">
                                                    <input id="busineess_contact_email" name="busineess_contact_email" data-identify="business_contacts" field-identify="contact_email" type="email" placeholder="ל״אוד סנכה" class="required form-control input-md  <?php echo $checkEmailClass; ?>" value="<?php echo $BC_contact_contact_email; ?>">
                                                </div>
                                            </div>
                                        </div>			
                                    </div>                                
                                    <!-- <div class="form-row">
                                        <div class="grid-auto grid-100">
                                            <div class="form-group">
                                                <div class="input-field-group checkbox-group auto-checkbox-group">
                                                    <div class="checkbox-col">
                                                        <label for="" class="wpuf-checkbox-inline">
                                                            <input type="checkbox" id="" name="" value="1">
                                                            קמעב םירבעממ םיפסונ םינוכדעו רטלזוינ לבקל ת/ןיינועמ ינא
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>                             -->
                                    <!-- <div class="form-row">                                
                                        <div class="grid-auto grid-100">
                                            <div class="form-group btn-add-group-row">
                                                <div class="padding-top-auto">
                                                    <button class="btn btn-add" id="business_contact_save" name="BC_business_contact_save" class="business_contact_add_new"> 
                                                        <i class="edit-plus-icon icon-black pl-5"></i>אישור ושמירת פרטים <i class="icon-arrow-left"></i>
                                                    </button>
                                                </div>	
                                            </div>
                                        </div>
                                    </div> -->
                                    <div class="btn-row-div btn-new-job-submit">
                                        <button class="btn btn-submit btn-job-submit" id="business_contact_save" name="BC_business_contact_save" type="submit">אישור ושמירת פרטים<i class="icon-arrow-left"></i></button>
                                    </div>	                                
                                </div>                            
                                <div class="clearfix"></div>				
                            </div>
                        </form>
                        <div class="clearfix"></div>                        
                    </div>                    
                </div>
            </div>

        </div>
    </div>
</section>
<?php 

get_footer(); 
?>
<?php
/**
 * Theme functions and definitions.
 *
 * Sets up the theme and provides some helper functions
 *
 * When using a child theme (see http://codex.wordpress.org/Theme_Development
 * and http://codex.wordpress.org/Child_Themes), you can override certain
 * functions (those wrapped in a function_exists() call) by defining them first
 * in your child theme's functions.php file. The child theme's functions.php
 * file is included before the parent theme's file, so the child theme
 * functions would be used.
 *
 *
 * For more information on hooks, actions, and filters,
 * see http://codex.wordpress.org/Plugin_API
 *
 * @package OceanWP WordPress theme
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Core Constants
define( 'OCEANWP_THEME_DIR', get_template_directory() );
define( 'OCEANWP_THEME_URI', get_template_directory_uri() );

final class OCEANWP_Theme_Class {

	/**
	 * Main Theme Class Constructor
	 *
	 * @since   1.0.0
	 */
	public function __construct() {

		// Define constants
		add_action( 'after_setup_theme', array( 'OCEANWP_Theme_Class', 'constants' ), 0 );

		// Load all core theme function files
		add_action( 'after_setup_theme', array( 'OCEANWP_Theme_Class', 'include_functions' ), 1 );

		// Load configuration classes
		add_action( 'after_setup_theme', array( 'OCEANWP_Theme_Class', 'configs' ), 3 );

		// Load framework classes
		add_action( 'after_setup_theme', array( 'OCEANWP_Theme_Class', 'classes' ), 4 );

		// Setup theme => add_theme_support, register_nav_menus, load_theme_textdomain, etc
		add_action( 'after_setup_theme', array( 'OCEANWP_Theme_Class', 'theme_setup' ), 10 );

		// Setup theme => Generate the custom CSS file
		add_action( 'admin_bar_init', array( 'OCEANWP_Theme_Class', 'save_customizer_css_in_file' ), 9999 );

		// register sidebar widget areas
		add_action( 'widgets_init', array( 'OCEANWP_Theme_Class', 'register_sidebars' ) );

		// Registers theme_mod strings into Polylang
		if ( class_exists( 'Polylang' ) ) {
			add_action( 'after_setup_theme', array( 'OCEANWP_Theme_Class', 'polylang_register_string' ) );
		}

		/** Admin only actions **/
		if ( is_admin() ) {

			// Load scripts in the WP admin
			add_action( 'admin_enqueue_scripts', array( 'OCEANWP_Theme_Class', 'admin_scripts' ) );

			// Outputs custom CSS for the admin
			add_action( 'admin_head', array( 'OCEANWP_Theme_Class', 'admin_inline_css' ) );

		/** Non Admin actions **/
		} else {

			// Load theme CSS
			add_action( 'wp_enqueue_scripts', array( 'OCEANWP_Theme_Class', 'theme_css' ) );

			// Load his file in last
			add_action( 'wp_enqueue_scripts', array( 'OCEANWP_Theme_Class', 'custom_style_css' ), 9999 );

			// Remove Customizer CSS script from Front-end
			add_action( 'init', array( 'OCEANWP_Theme_Class', 'remove_customizer_custom_css' ) );

			// Load theme js
			add_action( 'wp_enqueue_scripts', array( 'OCEANWP_Theme_Class', 'theme_js' ) );

			// Add a pingback url auto-discovery header for singularly identifiable articles
			add_action( 'wp_head', array( 'OCEANWP_Theme_Class', 'pingback_header' ), 1 );

			// Add meta viewport tag to header
			add_action( 'wp_head', array( 'OCEANWP_Theme_Class', 'meta_viewport' ), 1 );

			// Add an X-UA-Compatible header
			add_filter( 'wp_headers', array( 'OCEANWP_Theme_Class', 'x_ua_compatible_headers' ) );

			// Loads html5 shiv script
			add_action( 'wp_head', array( 'OCEANWP_Theme_Class', 'html5_shiv' ) );

			// Outputs custom CSS to the head
			add_action( 'wp_head', array( 'OCEANWP_Theme_Class', 'custom_css' ), 9999 );

			// Minify the WP custom CSS because WordPress doesn't do it by default
			add_filter( 'wp_get_custom_css', array( 'OCEANWP_Theme_Class', 'minify_custom_css' ) );

			// Alter the search posts per page
			add_action( 'pre_get_posts', array( 'OCEANWP_Theme_Class', 'search_posts_per_page' ) );

			// Alter WP categories widget to display count inside a span
			add_filter( 'wp_list_categories', array( 'OCEANWP_Theme_Class', 'wp_list_categories_args' ) );

			// Add a responsive wrapper to the WordPress oembed output
			add_filter( 'embed_oembed_html', array( 'OCEANWP_Theme_Class', 'add_responsive_wrap_to_oembeds' ), 99, 4 );

			// Adds classes the post class
			add_filter( 'post_class', array( 'OCEANWP_Theme_Class', 'post_class' ) );

			// Add schema markup to the authors post link
			add_filter( 'the_author_posts_link', array( 'OCEANWP_Theme_Class', 'the_author_posts_link' ) );

			// Add support for Elementor Pro locations
			add_action( 'elementor/theme/register_locations', array( 'OCEANWP_Theme_Class', 'register_elementor_locations' ) );

			// Remove the default lightbox script for the beaver builder plugin
			add_filter( 'fl_builder_override_lightbox', array( 'OCEANWP_Theme_Class', 'remove_bb_lightbox' ) );

		}

	}

	/**
	 * Define Constants
	 *
	 * @since   1.0.0
	 */
	public static function constants() {

		$version = self::theme_version();

		// Theme version
		define( 'OCEANWP_THEME_VERSION', $version );

		// Javascript and CSS Paths
		define( 'OCEANWP_JS_DIR_URI', OCEANWP_THEME_URI .'/assets/js/' );
		define( 'OCEANWP_CSS_DIR_URI', OCEANWP_THEME_URI .'/assets/css/' );

		// Include Paths
		define( 'OCEANWP_INC_DIR', OCEANWP_THEME_DIR .'/inc/' );
		define( 'OCEANWP_INC_DIR_URI', OCEANWP_THEME_URI .'/inc/' );

		// Check if plugins are active
		define( 'OCEAN_EXTRA_ACTIVE', class_exists( 'Ocean_Extra' ) );
		define( 'OCEANWP_ELEMENTOR_ACTIVE', class_exists( 'Elementor\Plugin' ) );
		define( 'OCEANWP_BEAVER_BUILDER_ACTIVE', class_exists( 'FLBuilder' ) );
		define( 'OCEANWP_WOOCOMMERCE_ACTIVE', class_exists( 'WooCommerce' ) );
		define( 'OCEANWP_EDD_ACTIVE', class_exists( 'Easy_Digital_Downloads' ) );
		define( 'OCEANWP_LIFTERLMS_ACTIVE', class_exists( 'LifterLMS' ) );
		define( 'OCEANWP_ALNP_ACTIVE', class_exists( 'Auto_Load_Next_Post' ) );
		define( 'OCEANWP_LEARNDASH_ACTIVE', class_exists( 'SFWD_LMS' ) );
	}

	/**
	 * Load all core theme function files
	 *
	 * @since 1.0.0
	 */
	public static function include_functions() {
		$dir = OCEANWP_INC_DIR;
		require_once ( $dir .'helpers.php' );
		require_once ( $dir .'header-content.php' );
		require_once ( $dir .'customizer/controls/typography/webfonts.php' );
		require_once ( $dir .'walker/init.php' );
		require_once ( $dir .'walker/menu-walker.php' );
		require_once ( $dir .'third/class-gutenberg.php' );
		require_once ( $dir .'third/class-elementor.php' );
		require_once ( $dir .'third/class-beaver-themer.php' );
		require_once ( $dir .'third/class-bbpress.php' );
		require_once ( $dir .'third/class-buddypress.php' );
		require_once ( $dir .'third/class-lifterlms.php' );
		require_once ( $dir .'third/class-learndash.php' );
		require_once ( $dir .'third/class-sensei.php' );
		require_once ( $dir .'third/class-social-login.php' );
	}

	/**
	 * Configs for 3rd party plugins.
	 *
	 * @since 1.0.0
	 */
	public static function configs() {

		$dir = OCEANWP_INC_DIR;

		// WooCommerce
		if ( OCEANWP_WOOCOMMERCE_ACTIVE ) {
			require_once ( $dir .'woocommerce/woocommerce-config.php' );
		}

		// Easy Digital Downloads
		if ( OCEANWP_EDD_ACTIVE ) {
			require_once ( $dir .'edd/edd-config.php' );
		}
	}

	/**
	 * Returns current theme version
	 *
	 * @since   1.0.0
	 */
	public static function theme_version() {

		// Get theme data
		$theme = wp_get_theme();

		// Return theme version
		return $theme->get( 'Version' );

	}

	/**
	 * Load theme classes
	 *
	 * @since   1.0.0
	 */
	public static function classes() {

		// Admin only classes
		if ( is_admin() ) {

			// Recommend plugins
			require_once( OCEANWP_INC_DIR .'plugins/class-tgm-plugin-activation.php' );
			require_once( OCEANWP_INC_DIR .'plugins/tgm-plugin-activation.php' );

		}

		// Front-end classes
		else {

			// Breadcrumbs class
			require_once( OCEANWP_INC_DIR .'breadcrumbs.php' );

		}

		// Customizer class
		require_once( OCEANWP_INC_DIR .'customizer/customizer.php' );

	}

	/**
	 * Theme Setup
	 *
	 * @since   1.0.0
	 */
	public static function theme_setup() {

		// Load text domain
		load_theme_textdomain( 'oceanwp', OCEANWP_THEME_DIR .'/languages' );

		// Get globals
		global $content_width;

		// Set content width based on theme's default design
		if ( ! isset( $content_width ) ) {
			$content_width = 1200;
		}

		// Register navigation menus
		register_nav_menus( array(
			'topbar_menu'     => esc_html__( 'Top Bar', 'oceanwp' ),
			'main_menu'       => esc_html__( 'Main', 'oceanwp' ),
			'footer_menu'     => esc_html__( 'Footer', 'oceanwp' ),
			'mobile_menu'     => esc_html__( 'Mobile (optional)', 'oceanwp' )
		) );

		// Enable support for Post Formats
		add_theme_support( 'post-formats', array( 'video', 'gallery', 'audio', 'quote', 'link' ) );

		// Enable support for <title> tag
		add_theme_support( 'title-tag' );

		// Add default posts and comments RSS feed links to head
		add_theme_support( 'automatic-feed-links' );

		// Enable support for Post Thumbnails on posts and pages
		add_theme_support( 'post-thumbnails' );

		/**
		 * Enable support for header image
		 */
		add_theme_support( 'custom-header', apply_filters( 'ocean_custom_header_args', array(
			'width'              => 2000,
			'height'             => 1200,
			'flex-height'        => true,
			'video'              => true,
		) ) );

		/**
		 * Enable support for site logo
		 */
		add_theme_support( 'custom-logo', apply_filters( 'ocean_custom_logo_args', array(
			'height'      => 45,
			'width'       => 164,
			'flex-height' => true,
			'flex-width'  => true,
		) ) );

		/*
		 * Switch default core markup for search form, comment form, comments, galleries, captions and widgets
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
			'widgets',
		) );

		// Declare WooCommerce support.
		add_theme_support( 'woocommerce' );
		add_theme_support( 'wc-product-gallery-zoom' );
		add_theme_support( 'wc-product-gallery-lightbox' );
		add_theme_support( 'wc-product-gallery-slider' );

		// Add editor style
		add_editor_style( 'assets/css/editor-style.min.css' );

		// Declare support for selective refreshing of widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

	}

	/**
	 * Adds the meta tag to the site header
	 *
	 * @since 1.1.0
	 */
	public static function pingback_header() {

		if ( is_singular() && pings_open() ) {
			printf( '<link rel="pingback" href="%s">' . "\n", esc_url( get_bloginfo( 'pingback_url' ) ) );
		}

	}

	/**
	 * Adds the meta tag to the site header
	 *
	 * @since 1.0.0
	 */
	public static function meta_viewport() {

		// Meta viewport
		$viewport = '<meta name="viewport" content="width=device-width, initial-scale=1">';

		// Apply filters for child theme tweaking
		echo apply_filters( 'ocean_meta_viewport', $viewport );

	}

	/**
	 * Load scripts in the WP admin
	 *
	 * @since 1.0.0
	 */
	public static function admin_scripts() {
		global $pagenow;
		if ( 'nav-menus.php' == $pagenow ) {
			wp_enqueue_style( 'oceanwp-menus', OCEANWP_INC_DIR_URI .'walker/assets/menus.css' );
		}
	}

	/**
	 * Load front-end scripts
	 *
	 * @since   1.0.0
	 */
	public static function theme_css() {

		// Define dir
		$dir = OCEANWP_CSS_DIR_URI;
		$theme_version = OCEANWP_THEME_VERSION;

		// Remove font awesome style from plugins
		wp_deregister_style( 'font-awesome' );
		wp_deregister_style( 'fontawesome' );

		// Load font awesome style
		wp_enqueue_style( 'font-awesome', $dir .'third/font-awesome.min.css', false, '4.7.0' );

		// Register simple line icons style
		wp_enqueue_style( 'simple-line-icons', $dir .'third/simple-line-icons.min.css', false, '2.4.0' );

		// Register the lightbox style
		wp_enqueue_style( 'magnific-popup', $dir .'third/magnific-popup.min.css', false, '1.0.0' );

		// Register the slick style
		wp_enqueue_style( 'slick', $dir .'third/slick.min.css', false, '1.6.0' );

		// Main Style.css File
		wp_enqueue_style( 'oceanwp-style', $dir .'style.min.css', false, $theme_version );

		// Register hamburgers buttons to easily use them
		wp_register_style( 'oceanwp-hamburgers', $dir .'third/hamburgers/hamburgers.min.css', false, $theme_version );

		// Register hamburgers buttons styles
		$hamburgers = oceanwp_hamburgers_styles();
		foreach ( $hamburgers as $class => $name ) {
			wp_register_style( 'oceanwp-'. $class .'', $dir .'third/hamburgers/types/'. $class .'.css', false, $theme_version );
		}

		// Get mobile menu icon style
		$mobileMenu = get_theme_mod( 'ocean_mobile_menu_open_hamburger', 'default' );

		// Enqueue mobile menu icon style
		if ( ! empty( $mobileMenu ) && 'default' != $mobileMenu ) {
			wp_enqueue_style( 'oceanwp-hamburgers' );
			wp_enqueue_style( 'oceanwp-'. $mobileMenu .'' );
		}

		// If Vertical header style
		if ( 'vertical' == oceanwp_header_style() ) {
			wp_enqueue_style( 'oceanwp-hamburgers' );
			wp_enqueue_style( 'oceanwp-spin' );
		}

	}

	/**
	 * Returns all js needed for the front-end
	 *
	 * @since 1.0.0
	 */
	public static function theme_js() {

		// Get js directory uri
		$dir = OCEANWP_JS_DIR_URI;

		// Get current theme version
		$theme_version = OCEANWP_THEME_VERSION;

		// Get localized array
		$localize_array = self::localize_array();

		// Comment reply
		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}

		// Add images loaded
		wp_enqueue_script( 'imagesloaded' );

		// Register nicescroll script to use it in some extensions
		wp_register_script( 'nicescroll', $dir .'third/nicescroll.min.js', array( 'jquery' ), $theme_version, true );

		// Enqueue nicescroll script if vertical header style
		if ( 'vertical' == oceanwp_header_style() ) {
			wp_enqueue_script( 'nicescroll' );
		}

		// Register Infinite Scroll script
		wp_register_script( 'infinitescroll', $dir .'third/infinitescroll.min.js', array( 'jquery' ), $theme_version, true );

		// WooCommerce scripts
		if ( OCEANWP_WOOCOMMERCE_ACTIVE
			&& 'yes' != get_theme_mod( 'ocean_woo_remove_custom_features', 'no' ) ) {
			wp_enqueue_script( 'oceanwp-woocommerce', $dir .'third/woo/woo-scripts.min.js', array( 'jquery' ), $theme_version, true );
		}

		// Load the lightbox scripts
		wp_enqueue_script( 'magnific-popup', $dir .'third/magnific-popup.min.js', array( 'jquery' ), $theme_version, true );
		wp_enqueue_script( 'oceanwp-lightbox', $dir .'third/lightbox.min.js', array( 'jquery' ), $theme_version, true );

		// Load minified js
		wp_enqueue_script( 'oceanwp-main', $dir .'main.min.js', array( 'jquery' ), $theme_version, true );
		
		// Localize array
		wp_localize_script( 'oceanwp-main', 'oceanwpLocalize', $localize_array );

	}

	/**
	 * Functions.js localize array
	 *
	 * @since 1.0.0
	 */
	public static function localize_array() {

		// Create array
		$sidr_side 		= get_theme_mod( 'ocean_mobile_menu_sidr_direction', 'left' );
		$sidr_side 		= $sidr_side ? $sidr_side : 'left';
		$sidr_target 	= get_theme_mod( 'ocean_mobile_menu_sidr_dropdown_target', 'icon' );
		$sidr_target 	= $sidr_target ? $sidr_target : 'icon';
		$vh_target 		= get_theme_mod( 'ocean_vertical_header_dropdown_target', 'icon' );
		$vh_target 		= $vh_target ? $vh_target : 'icon';
		$array = array(
			'isRTL'                 => is_rtl(),
			'menuSearchStyle'       => oceanwp_menu_search_style(),
			'sidrSource'       		=> oceanwp_sidr_menu_source(),
			'sidrDisplace'       	=> get_theme_mod( 'ocean_mobile_menu_sidr_displace', true ) ? true : false,
			'sidrSide'       		=> $sidr_side,
			'sidrDropdownTarget'    => $sidr_target,
			'verticalHeaderTarget'  => $vh_target,
			'customSelects'         => '.woocommerce-ordering .orderby, #dropdown_product_cat, .widget_categories select, .widget_archive select, .single-product .variations_form .variations select',
		);

		// WooCart
		if ( OCEANWP_WOOCOMMERCE_ACTIVE ) {
			$array['wooCartStyle'] 	= oceanwp_menu_cart_style();
		}

		// Apply filters and return array
		return apply_filters( 'ocean_localize_array', $array );
	}

	/**
	 * Add headers for IE to override IE's Compatibility View Settings
	 *
	 * @since 1.0.0
	 */
	public static function x_ua_compatible_headers( $headers ) {
		$headers['X-UA-Compatible'] = 'IE=edge';
		return $headers;
	}

	/**
	 * Load HTML5 dependencies for IE8
	 *
	 * @since 1.0.0
	 */
	public static function html5_shiv() {
		wp_register_script( 'html5shiv', OCEANWP_JS_DIR_URI . '/third/html5.min.js', array(), OCEANWP_THEME_VERSION, false );
		wp_enqueue_script( 'html5shiv' );
		wp_script_add_data( 'html5shiv', 'conditional', 'lt IE 9' );
	}

	/**
	 * Registers sidebars
	 *
	 * @since   1.0.0
	 */
	public static function register_sidebars() {

		$heading = 'h4';
		$heading = apply_filters( 'ocean_sidebar_heading', $heading );

		// Default Sidebar
		register_sidebar( array(
			'name'			=> esc_html__( 'Default Sidebar', 'oceanwp' ),
			'id'			=> 'sidebar',
			'description'	=> esc_html__( 'Widgets in this area will be displayed in the left or right sidebar area if you choose the Left or Right Sidebar layout.', 'oceanwp' ),
			'before_widget'	=> '<div id="%1$s" class="sidebar-box %2$s clr">',
			'after_widget'	=> '</div>',
			'before_title'	=> '<'. $heading .' class="widget-title">',
			'after_title'	=> '</'. $heading .'>',
		) );

		// Left Sidebar
		register_sidebar( array(
			'name'			=> esc_html__( 'Left Sidebar', 'oceanwp' ),
			'id'			=> 'sidebar-2',
			'description'	=> esc_html__( 'Widgets in this area are used in the left sidebar region if you use the Both Sidebars layout.', 'oceanwp' ),
			'before_widget'	=> '<div id="%1$s" class="sidebar-box %2$s clr">',
			'after_widget'	=> '</div>',
			'before_title'	=> '<'. $heading .' class="widget-title">',
			'after_title'	=> '</'. $heading .'>',
		) );

		// Search Results Sidebar
		if ( get_theme_mod( 'ocean_search_custom_sidebar', true ) ) {
			register_sidebar( array(
				'name'			=> esc_html__( 'Search Results Sidebar', 'oceanwp' ),
				'id'			=> 'search_sidebar',
				'description'	=> esc_html__( 'Widgets in this area are used in the search result page.', 'oceanwp' ),
				'before_widget'	=> '<div id="%1$s" class="sidebar-box %2$s clr">',
				'after_widget'	=> '</div>',
				'before_title'	=> '<'. $heading .' class="widget-title">',
				'after_title'	=> '</'. $heading .'>',
			) );
		}

		// Footer 1
		register_sidebar( array(
			'name'			=> esc_html__( 'Footer 1', 'oceanwp' ),
			'id'			=> 'footer-one',
			'description'	=> esc_html__( 'Widgets in this area are used in the first footer region.', 'oceanwp' ),
			'before_widget'	=> '<div id="%1$s" class="footer-widget %2$s clr">',
			'after_widget'	=> '</div>',
			'before_title'	=> '<'. $heading .' class="widget-title">',
			'after_title'	=> '</'. $heading .'>',
		) );

		// Footer 2
		register_sidebar( array(
			'name'			=> esc_html__( 'Footer 2', 'oceanwp' ),
			'id'			=> 'footer-two',
			'description'	=> esc_html__( 'Widgets in this area are used in the second footer region.', 'oceanwp' ),
			'before_widget'	=> '<div id="%1$s" class="footer-widget %2$s clr">',
			'after_widget'	=> '</div>',
			'before_title'	=> '<'. $heading .' class="widget-title">',
			'after_title'	=> '</'. $heading .'>',
		) );

		// Footer 3
		register_sidebar( array(
			'name'			=> esc_html__( 'Footer 3', 'oceanwp' ),
			'id'			=> 'footer-three',
			'description'	=> esc_html__( 'Widgets in this area are used in the third footer region.', 'oceanwp' ),
			'before_widget'	=> '<div id="%1$s" class="footer-widget %2$s clr">',
			'after_widget'	=> '</div>',
			'before_title'	=> '<'. $heading .' class="widget-title">',
			'after_title'	=> '</'. $heading .'>',
		) );

		// Footer 4
		register_sidebar( array(
			'name'			=> esc_html__( 'Footer 4', 'oceanwp' ),
			'id'			=> 'footer-four',
			'description'	=> esc_html__( 'Widgets in this area are used in the fourth footer region.', 'oceanwp' ),
			'before_widget'	=> '<div id="%1$s" class="footer-widget %2$s clr">',
			'after_widget'	=> '</div>',
			'before_title'	=> '<'. $heading .' class="widget-title">',
			'after_title'	=> '</'. $heading .'>',
		) );

	}

	/**
	 * Registers theme_mod strings into Polylang.
	 *
	 * @since 1.1.4
	 */
	public static function polylang_register_string() {

		if ( function_exists( 'pll_register_string' ) && $strings = oceanwp_register_tm_strings() ) {
			foreach( $strings as $string => $default ) {
				pll_register_string( $string, get_theme_mod( $string, $default ), 'Theme Mod', true );
			}
		}

	}

	/**
	 * All theme functions hook into the oceanwp_head_css filter for this function.
	 *
	 * @since 1.0.0
	 */
	public static function custom_css( $output = NULL ) {
			    
	    // Add filter for adding custom css via other functions
		$output = apply_filters( 'ocean_head_css', $output );

		// If Custom File is selected
		if ( 'file' == get_theme_mod( 'ocean_customzer_styling', 'head' ) ) {

			global $wp_customize;
			$upload_dir = wp_upload_dir();

			// Render CSS in the head
			if ( isset( $wp_customize ) || ! file_exists( $upload_dir['basedir'] .'/oceanwp/custom-style.css' ) ) {

				 // Minify and output CSS in the wp_head
				if ( ! empty( $output ) ) {
					echo "<!-- OceanWP CSS -->\n<style type=\"text/css\">\n" . wp_strip_all_tags( oceanwp_minify_css( $output ) ) . "\n</style>";
				}
			}

		} else {

			// Minify and output CSS in the wp_head
			if ( ! empty( $output ) ) {
				echo "<!-- OceanWP CSS -->\n<style type=\"text/css\">\n" . wp_strip_all_tags( oceanwp_minify_css( $output ) ) . "\n</style>";
			}

		}

	}

	/**
	 * Minify the WP custom CSS because WordPress doesn't do it by default.
	 *
	 * @since 1.1.9
	 */
	public static function minify_custom_css( $css ) {

		return oceanwp_minify_css( $css );

	}

	/**
	 * Save Customizer CSS in a file
	 *
	 * @since 1.4.12
	 */
	public static function save_customizer_css_in_file( $output = NULL ) {

		// If Custom File is not selected
		if ( 'file' != get_theme_mod( 'ocean_customzer_styling', 'head' ) ) {
			return;
		}

		// Get all the customier css
	    $output = apply_filters( 'ocean_head_css', $output );

	    // Get Custom Panel CSS
	    $output_custom_css = wp_get_custom_css();

	    // Minified the Custom CSS
		$output .= oceanwp_minify_css( $output_custom_css );
			
		// We will probably need to load this file
		require_once( ABSPATH . 'wp-admin' . DIRECTORY_SEPARATOR . 'includes' . DIRECTORY_SEPARATOR . 'file.php' );
		
		global $wp_filesystem;
		$upload_dir = wp_upload_dir(); // Grab uploads folder array
		$dir = trailingslashit( $upload_dir['basedir'] ) . 'oceanwp'. DIRECTORY_SEPARATOR; // Set storage directory path

		WP_Filesystem(); // Initial WP file system
		$wp_filesystem->mkdir( $dir ); // Make a new folder 'oceanwp' for storing our file if not created already.
		$wp_filesystem->put_contents( $dir . 'custom-style.css', $output, 0644 ); // Store in the file.

	}

	/**
	 * Include Custom CSS file if present.
	 *
	 * @since 1.4.12
	 */
	public static function custom_style_css( $output = NULL ) {

		// If Custom File is not selected
		if ( 'file' != get_theme_mod( 'ocean_customzer_styling', 'head' ) ) {
			return;
		}

		global $wp_customize;
		$upload_dir = wp_upload_dir();

		// Get all the customier css
	    $output = apply_filters( 'ocean_head_css', $output );

	    // Get Custom Panel CSS
	    $output_custom_css = wp_get_custom_css();

	    // Minified the Custom CSS
		$output .= oceanwp_minify_css( $output_custom_css );

		// Render CSS from the custom file
		if ( ! isset( $wp_customize ) && file_exists( $upload_dir['basedir'] .'/oceanwp/custom-style.css' ) && ! empty( $output ) ) { 
		    wp_enqueue_style( 'oceanwp-custom', trailingslashit( $upload_dir['baseurl'] ) . 'oceanwp/custom-style.css', false, null );	    			
		}		
	}

	/**
	 * Remove Customizer style script from front-end
	 *
	 * @since 1.4.12
	 */
	public static function remove_customizer_custom_css() {

		// If Custom File is not selected
		if ( 'file' != get_theme_mod( 'ocean_customzer_styling', 'head' ) ) {
			return;
		}
		
		global $wp_customize;

		// Disable Custom CSS in the frontend head
		remove_action( 'wp_head', 'wp_custom_css_cb', 11 );
		remove_action( 'wp_head', 'wp_custom_css_cb', 101 );

		// If custom CSS file exists and NOT in customizer screen
		if ( isset( $wp_customize ) ) {
			add_action( 'wp_footer', 'wp_custom_css_cb', 9999 );
		}
	}

	/**
	 * Adds inline CSS for the admin
	 *
	 * @since 1.0.0
	 */
	public static function admin_inline_css() {
		echo '<style>div#setting-error-tgmpa{display:block;}</style>';
	}


	/**
	 * Alter the search posts per page
	 *
	 * @since 1.3.7
	 */
	public static function search_posts_per_page( $query ) {
		$posts_per_page = get_theme_mod( 'ocean_search_post_per_page', '8' );
		$posts_per_page = $posts_per_page ? $posts_per_page : '8';

		if ( $query->is_main_query() && is_search() ) {
			$query->set( 'posts_per_page', $posts_per_page );
		}
	}

	/**
	 * Alter wp list categories arguments.
	 * Adds a span around the counter for easier styling.
	 *
	 * @since 1.0.0
	 */
	public static function wp_list_categories_args( $links ) {
		$links = str_replace( '</a> (', '</a> <span class="cat-count-span">(', $links );
		$links = str_replace( ' )', ' )</span>', $links );
		return $links;
	}

	/**
	 * Alters the default oembed output.
	 * Adds special classes for responsive oembeds via CSS.
	 *
	 * @since 1.0.0
	 */
	public static function add_responsive_wrap_to_oembeds( $cache, $url, $attr, $post_ID ) {

		// Supported video embeds
		$hosts = apply_filters( 'ocean_oembed_responsive_hosts', array(
			'vimeo.com',
			'youtube.com',
			'blip.tv',
			'money.cnn.com',
			'dailymotion.com',
			'flickr.com',
			'hulu.com',
			'kickstarter.com',
			'vine.co',
			'soundcloud.com',
			'#http://((m|www)\.)?youtube\.com/watch.*#i',
	        '#https://((m|www)\.)?youtube\.com/watch.*#i',
	        '#http://((m|www)\.)?youtube\.com/playlist.*#i',
	        '#https://((m|www)\.)?youtube\.com/playlist.*#i',
	        '#http://youtu\.be/.*#i',
	        '#https://youtu\.be/.*#i',
	        '#https?://(.+\.)?vimeo\.com/.*#i',
	        '#https?://(www\.)?dailymotion\.com/.*#i',
	        '#https?://dai\.ly/*#i',
	        '#https?://(www\.)?hulu\.com/watch/.*#i',
	        '#https?://wordpress\.tv/.*#i',
	        '#https?://(www\.)?funnyordie\.com/videos/.*#i',
	        '#https?://vine\.co/v/.*#i',
	        '#https?://(www\.)?collegehumor\.com/video/.*#i',
	        '#https?://(www\.|embed\.)?ted\.com/talks/.*#i'
		) );

		// Supports responsive
		$supports_responsive = false;

		// Check if responsive wrap should be added
		foreach( $hosts as $host ) {
			if ( strpos( $url, $host ) !== false ) {
				$supports_responsive = true;
				break; // no need to loop further
			}
		}

		// Output code
		if ( $supports_responsive ) {
			return '<p class="responsive-video-wrap clr">' . $cache . '</p>';
		} else {
			return '<div class="oceanwp-oembed-wrap clr">' . $cache . '</div>';
		}

	}

	/**
	 * Adds extra classes to the post_class() output
	 *
	 * @since 1.0.0
	 */
	public static function post_class( $classes ) {

		// Get post
		global $post;

		// Add entry class
		$classes[] = 'entry';

		// Add has media class
		if ( has_post_thumbnail()
			|| get_post_meta( $post->ID, 'ocean_post_oembed', true )
			|| get_post_meta( $post->ID, 'ocean_post_self_hosted_media', true )
			|| get_post_meta( $post->ID, 'ocean_post_video_embed', true )
		) {
			$classes[] = 'has-media';
		}

		// Return classes
		return $classes;

	}

	/**
	 * Add schema markup to the authors post link
	 *
	 * @since 1.0.0
	 */
	public static function the_author_posts_link( $link ) {

		// Add schema markup
		$schema = oceanwp_get_schema_markup( 'author_link' );
		if ( $schema ) {
			$link = str_replace( 'rel="author"', 'rel="author" '. $schema, $link );
		}

		// Return link
		return $link;

	}

	/**
	 * Add support for Elementor Pro locations
	 *
	 * @since 1.5.6
	 */
	public static function register_elementor_locations( $elementor_theme_manager ) {
		$elementor_theme_manager->register_all_core_location();
	}

	/**
	 * Add schema markup to the authors post link
	 *
	 * @since 1.1.5
	 */
	public static function remove_bb_lightbox() {
		return true;
	}

}

#--------------------------------------------------------------------------------
#region Freemius - This logic will only be executed when Ocean Extra is active and has the Freemius SDK
#--------------------------------------------------------------------------------

if ( ! function_exists( 'owp_fs' ) ) {
    if ( class_exists( 'Ocean_Extra' ) &&
         defined( 'OE_FILE_PATH' ) &&
         file_exists( dirname( OE_FILE_PATH ) . '/includes/freemius/start.php' )
    ) {
        // Create a helper function for easy SDK access.
        function owp_fs() {
            global $owp_fs;

            if ( ! isset( $owp_fs ) ) {
                // Include Freemius SDK.
                require_once dirname( OE_FILE_PATH ) . '/includes/freemius/start.php';

                $owp_fs = fs_dynamic_init( array(
                    'id'                => '3752',
                    'bundle_id'         => '3767',
                    'slug'              => 'oceanwp',
                    'type'              => 'theme',
                    'public_key'        => 'pk_043077b34f20f5e11334af3c12493',
                    'bundle_public_key' => 'pk_c334eb1ae413deac41e30bf00b9dc',
                    'is_premium'        => false,
                    'has_addons'        => true,
                    'has_paid_plans'    => true,
                    'menu'              => array(
                        'slug'    => 'oceanwp-panel',
                        'account' => true,
                        'contact' => false,
                        'support' => false,
                    ),
                    'navigation'        => 'menu',
                    'is_org_compliant'  => true,
                ) );
            }

            return $owp_fs;
        }

        // Init Freemius.
        owp_fs();
        // Signal that SDK was initiated.
        do_action( 'owp_fs_loaded' );
    }
}

#endregion

new OCEANWP_Theme_Class;


function login_register_shortcode($args) {
$html = '';
$link = get_template_directory_uri().'/logout.php';
$html .= '<h2 class="elementor-heading-title elementor-size-default">';
if(!session_id()) {
	session_start();
}
if(array_key_exists('login_user_detail', $_SESSION)) {	
	$html .= '<div class="user-div">		
		<div class="dropdown register-link inner-dropdown1">
		<a href="#" class="dropbtn">התחברות</a>
		<div class="dropdown-content dropdown-top">
			<div class="dropdown-inner-content">
				<div class="dropdown-body">
					<a href="' . $link . '" class="link-drop">Logout<i class="icon-arrow-left"></i></a>
					<a href="'.get_permalink(651).'" class="link-drop">הרשמת עסק<i class="icon-arrow-left"></i></a>
				</div>
			</div>	
		</div>
		</div> 
		<i class="icon-user"></i>
	</div>';
} else { 
	if (!is_user_logged_in()) {
	$html .= '<div class="user-div">
		<div class="dropdown register-link">
		<a href="#" class="dropbtn">הרשמה</a>
			<div class="dropdown-content dropdown-top">
				<div class="dropdown-inner-content">
					<div class="dropdown-header">
						<h3>הרשמה לאתר</h3>
					</div>
					<div class="dropdown-body">
						<a href="#" class="link-drop">הרשמת משתמש <i class="icon-arrow-left"></i></a>
						<a href="'.get_permalink(505).'" class="link-drop">הרשמת עסק<i class="icon-arrow-left"></i></a>
					</div>
				</div>	
			</div>
		</div> 
		<span class="slash-span">|</span> 
		<div class="dropdown login-link">
		<a href="#" class="dropbtn">התחברות</a>
			<div class="dropdown-content dropdown-top">
				<div class="dropdown-inner-content">
					<div class="dropdown-header">
						<h3>התחברות לאתר</h3>
					</div>
					<form id="loginForm" autocomplete="off">
						<div class="dropdown-body">
							<div class="form-group">
							<label>כתובת דוא״ל</label>
							<input type="text" class="form-control" name="login_email" required />
							</div>  
							<div class="form-group">
							<label>סיסמה...</label>
							<input type="password" class="form-control" name="login_pass" required />
							</div>  
						</div> 
						<div class="dropdown-footer">
							<button class="submit-link" type="button" id="loginButton">התחברות <i class="icon-arrow-left"></i></button>
						</div>
					</form>
				</div>	
			</div>
		</div> 
		<i class="icon-user"></i>
	</div>';
	}
}
$html .= '<div class="phone-call-div">
	<i class="icon-phone"></i>
	<a href="tel:04-6544726">04-6544726</a>
</div>
</h2>';
return $html;
}
add_shortcode( 'helloworld', 'login_register_shortcode' );

function page_title_sc( ){
   return get_the_title();
}
add_shortcode( 'page_title', 'page_title_sc' );

function inner_banner () {
	if (!is_front_page()) {
		$pageid = get_the_ID();
		return '<div class="inner_banner mainbanner-box-section">
		<div class="header_section_seperate_menu">
			<div class="section_top">
				<i class="icon-campus"></i>
			</div>
			<div class="section_botton">
				<p>קמפוס מעברים</p>
				<i class="icon-arrow-left"></i>
			</div>
		</div>
		<div class="header_section_banner">
			<img src="'.get_the_post_thumbnail_url($pageid).'" class="header_section_image" alt="" title="" />		
			<div class="banner-indextop">
				<h2>'.get_the_title().'</h2>
			</div>
		</div>
		</div>';
	} else {
		return '<div class="inner_banner mainbanner-box-section">
		<div class="header_section_seperate_menu">
			<div class="section_top">
				<i class="icon-campus"></i>
			</div>
			<div class="section_botton">
				<p>קמפוס מעברים</p>
				<i class="icon-arrow-left"></i>
			</div>
		</div>
		<div class="header_section_banner">
			<img src="'.site_url().'/wp-content/uploads/2019/08/banner.png" class="header_section_image" alt="" title="" />		
			<div class="banner-indextop">
				<h2>מעברים בעמק</h2>
				<h4>קריירה. עסקים. קהילה</h4>
			</div>
		</div>
		</div>';
	}
}
add_shortcode( 'inner_banner', 'inner_banner' );

function business_registration_shortcode() {
	if(!session_id()) {
		session_start();
	}	
	if(array_key_exists('login_user_detail', $_SESSION) && !array_key_exists('bid', $_REQUEST)) {
		$pageLink = get_permalink(542);
		$user = $_SESSION['login_user_detail'];
		global $wpdb;
		$table = $wpdb->prefix.'advertise_business';
		$query = 'SELECT * FROM '.$table.' WHERE user_id = "'.$user->id.'"';
		$result = $wpdb->get_results($query);
		if(count($result) > 0) {
			$pageLink = get_permalink(651);
		}
		return '<div class="inner_business_box_row">
			<div class="width50_box">
				<div class="two_section_left">
					<div class="top-section-box">
						<h3>המשך רישום לצורך פרסום העסק בלוח עסקים בעמקים</h3>
					</div>	
					<div class="two_section_buttons">
						<a href="' . $pageLink . '"><i class="icon-arrow-left"></i></a>
					</div>
				</div>
			</div>
			<div class="width50_box">
				<div class="two_section_left change-color2">
					<div class="top-section-box">
						<h3>המשך רישום לצורך פרסום משרות בלוח הדרושים</h3>
					</div>	
					<div class="two_section_buttons">
						<a href="'.get_permalink(654).'"> <i class="icon-arrow-left"></i></a>
					</div>
				</div>	
			</div>
		</div>';
	} else {
		return do_shortcode('[business_registration_form]');
	}	
}
add_shortcode( 'business_registration', 'business_registration_shortcode' );

function business_publish_shortcode() {
	if(!session_id()) {
		session_start();
	}
	if(array_key_exists('login_user_detail', $_SESSION)) {
	    return '<div class="custom-form-business-publish">'.do_shortcode('[business_advertise_form]').'</div>';
	} else {
		echo '<script> location.href="'.get_site_url().'"; </script>';
	}	
}
add_shortcode( 'business_publish', 'business_publish_shortcode' );

function new_job_page_shortcode() {
	if(!session_id()) {
		session_start();
	}
	if(array_key_exists('login_user_detail', $_SESSION) || is_user_logged_in()) {
	    return '<div class="business-white-forms01 new-job-form01">'.do_shortcode('[business_contacts_form]').'</div>';
	} else {
		echo '<script> location.href="'.get_site_url().'"; </script>';
	}	
}
add_shortcode( 'new_job_page', 'new_job_page_shortcode' );








// Forms html business registration // shortcode == business_registration_form
function business_registration_from_shortcode() {
	$dbprocess = get_template_directory_uri().'/database-process.php';
	$businessRegistrationFrom = '';
	$BR_business_id = '';
	$BR_business_name = '';
	$BR_business_service = '';
	$BR_locality = '';
	$BR_business_email = '';
	$BR_business_phone = '';
	$BR_business_size = '';
	$BR_is_received_newsletter = '';
	$BR_readonly = '';
	if(array_key_exists('bid', $_REQUEST)) {
		//get business contacts
		$user = $_SESSION['login_user_detail'];
		global $wpdb;
		$user_table = $wpdb->prefix.'business_users';
		$query = 'SELECT * FROM ' . $user_table . ' WHERE id = '.$_REQUEST['bid'];
		$regitered_business = $wpdb->get_results($query);
		if(count($regitered_business) > 0) {
			$regitered_business = $regitered_business[0];
			$BR_business_id = !empty($regitered_business->id) ? $regitered_business->id : '';
			$BR_business_name = !empty($regitered_business->business_name) ? $regitered_business->business_name : '';
			$BR_business_service = !empty($regitered_business->business_service) ? $regitered_business->business_service : '';
			$BR_locality = !empty($regitered_business->locality) ? $regitered_business->locality : '';
			$BR_business_email = !empty($regitered_business->email) ? $regitered_business->email : '';
			$BR_business_phone = !empty($regitered_business->phone) ? $regitered_business->phone : '';
			$BR_business_size = !empty($regitered_business->size) ? $regitered_business->size : '';
			$BR_is_received_newsletter = ($regitered_business->is_received_newsletter == 0) ? '' : 'checked="checked"';
			$BR_readonly = 'readonly="readonly"';
		} else {
			echo '<script> location.href="'.get_site_url().'"; </script>';
        	die;
		}
	}
	$selected = 'selected="selected"';
	$localities = array(
	    '1' => 'א.ת. אלון תבור',
	    '2' => 'א.ת. יקנעם',
	    '3' => 'א.ת. מבוא כרמל',
	    '4' => 'א.ת. מבואות גלבוע',
	);
	$roles = array(
		'1' => 'Project Manager',
		'2' => 'HR'
	);
	$business_sizes = array(
		'1' => 'עסק זעיר - עד 5 עובדים',
		'2' => 'עסק קטן - 6-50 עובדים',
		'3' => 'עסק בינוני - 51-100 עובדים',
		'4' => 'עסק גדול - מעל 100 עובדים',
	);
	$businessRegistrationFrom .= '
	<script src="https://jqueryvalidation.org/files/lib/jquery.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>
	<script src="'.get_template_directory_uri().'/custom.validation.js" ></script>
	<form class="wpuf-form-add wpuf-style business-blue-forms01" action="'.$dbprocess.'" method="post" id="" name="business_registration">
		<input type="hidden" name="BR_registered_business" value="'.$BR_business_id.'" />
		<input type="hidden" name="BR_business_registration_form" value="" />
		<div class="business-form-root-div">
			<div class="bs-form-div">				
				<div class="form-row">
					<div class="grid-auto grid-18">
						<div class="form-title-heading">
							<span class="form-title">פרטי העסק</span>
						</div>		
					</div>
					<div class="grid-auto grid-41">
						<div class="form-group">
							<label for="business_name">שם העסק <span class="required">*</span></label>
							<div class="input-field-group">
								<input class="form-control" id="business_name" type="text" required data-type="text" name="business_name" placeholder="שם העסק" value="'.$BR_business_name.'" size="40">
								<span class="wpuf-wordlimit-message wpuf-help"></span>
							</div>
						</div>
					</div>
					<div class="grid-auto grid-41">
						<div class="form-group">
							<label for="businsess_service_field">תחום עיסוק / מוצר / שירות <span class="required">*</span></label>
							<div class="input-field-group">
								<input class="form-control" id="businsess_service_field" type="text" required data-type="text" name="service_field" placeholder="טקסט חופשי..." value="'.$BR_business_service.'" size="100">
								<span class="wpuf-wordlimit-message wpuf-help"></span>
							</div>
						</div>
					</div>
				</div>			
				<div class="clearfix"></div>
				<div class="form-row">
					<div class="grid-auto grid-25">
						<div class="form-group">
							<label for="business_locality">יישוב <span class="required">*</span></label>
							<div class="input-field-group">
								<select class="wpuf_business_locality" id="business_locality" name="business_locality" required data-type="select">
									<option value="">יישוב</option>';
									foreach($localities as $Lkey=>$Lval) {
											if ($Lkey == $BR_locality) {
												$businessRegistrationFrom .= '
													<option value="'.$Lkey.'" '.$selected.'>'.$Lval.'</option>
												';
											} else {
												$businessRegistrationFrom .= '
													<option value="'.$Lkey.'">'.$Lval.'</option>
												';
											}
									}
									$businessRegistrationFrom .= '
								</select>
								<span class="dropdown-arrow"></span>
							</div>
						</div>
					</div>
					<div class="grid-auto grid-25">
						<div class="form-group">
							<label for="business_phone">טלפון <span class="required">*</span></label>
							<div class="input-field-group">
								<input class="form-control" id="business_phone" type="text" data-duplicate="no" required data-type="text" name="business_phone" placeholder="הכנס מספר כאן" value="'.$BR_business_phone.'" size="40">
								<span class="wpuf-wordlimit-message wpuf-help"></span>
							</div>
						</div>
					</div>
					<div class="grid-auto grid-25">
						<div class="form-group">
							<label for="business_email">דואר אלקטרוני <span class="required">*</span></label>
							<div class="input-field-group">
								<input id="business_email" type="email" class="form-control check-email" data-duplicate="no" data-identify="business_users" field-identify="email" required data-type="email" '.$BR_readonly.' name="business_email" placeholder="הכנס דואר אלקטרוני כאן" value="'.$BR_business_email.'" size="100" autocomplete="email">
								<span class="wpuf-help"></span>
							</div>
						</div>
					</div>
					<div class="grid-auto grid-25">
						<div class="form-group">
							<label for="business_size">גודל העסק <span class="required">*</span></label>
							<div class="input-field-group">
								<select class="wpuf_business_size" id="business_size" name="business_size" required data-type="select">
									<option value="">בינוני</option>';
									foreach($business_sizes as $Skey=>$Sval) {
											if ($Skey == $BR_business_size) {
												$businessRegistrationFrom .= '
													<option value="'.$Skey.'" '.$selected.'>'.$Sval.'</option>
												';
											} else {
												$businessRegistrationFrom .= '
													<option value="'.$Skey.'">'.$Sval.'</option>
												';
											}
									}
									$businessRegistrationFrom .= '
								</select>
								<span class="dropdown-arrow"></span>
							</div>
						</div>
					</div>
				</div>
				<div class="form-row">
					<div class="grid-auto grid-30 grid-tl-100">
						<div class="form-group">
							<label for="business_password">סיסמה - כניסה לאתר <span class="required">*</span></label>
							<div class="input-field-group">
								<input class="form-control" id="business_password" type="password" data-duplicate="no" required data-type="text" name="business_password" placeholder="הכנס סיסמה" value="" size="40">
								<span class="wpuf-wordlimit-message wpuf-help"></span>
							</div>
						</div>
					</div>
					<div class="grid-auto grid-50">
						<div class="form-group form-group-checkbox">
							<label class="wpuf-checkbox-inline pt-40">
								<input type="checkbox" class="wpuf_business_is_received_newsletter" name="business_is_received_newsletter" value="business_is_received_newsletter" '.$BR_is_received_newsletter.'>
									אני מעוניין/ת לקבל ניוזלטר ועדכונים נוספים ממעברים בעמק                        
							</label>
						</div>	
					</div>
					<div class="grid-auto grid-15 btn-grid">
						<div class="btn-submit-group">
							<button class="btn btn-submit" name="business_registration">רשמו אותי לאתר<i class="icon-arrow-left"></i></button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>';
	return $businessRegistrationFrom;
}
add_shortcode( 'business_registration_form', 'business_registration_from_shortcode' );


// Forms html advertising business // shortcode == business_advertise_form
function business_advertise_form_shortcode() {
	$dbprocess = get_template_directory_uri().'/database-process.php';
	$businessArry = [];
	$businessArry['bid'] = '';
	$businessArry['owner_name'] = '';
	$businessArry['about'] = '';
	$businessArry['services'] = '';
	$businessArry['promotions'] = '';
	$businessArry['web_address'] = '';
	$businessArry['facebook_link'] = '';
	$businessArry['insta_link'] = '';
	$businessArry['whatsapp_link'] = '';
	$businessArry['youtube_link'] = '';
	$businessArry['email_publish'] = '';
	$businessArry['phone_publish'] = '';
	$businessArry['is_received_newsletter'] = '';
	$businessArry['images'] = [];
	// $AB_readonly = '';
	if(array_key_exists('b', $_REQUEST)) {
		//get business contacts
		if(!session_id()) {
			session_start();
		}
		$user = $_SESSION['login_user_detail'];
		global $wpdb;
		$advertise_business_table = $wpdb->prefix.'advertise_business';
		$user_table = $wpdb->prefix.'business_users';
		$business_gallery = $wpdb->prefix.'business_gallery';
		$query = 'SELECT AB.*, BG.name as image_name, BG.image_type as image_type, BG.id as image_id FROM ' . $advertise_business_table . ' AB INNER JOIN ' . $user_table . ' BU ON BU.id = AB.user_id INNER JOIN '.$business_gallery.' BG ON AB.id = BG.business_id WHERE AB.id = '.$_REQUEST['b'];
		$businesses = $wpdb->get_results($query);
		if(count($businesses) > 0) {
			foreach($businesses as $business) {
				$businessArry['bid'] = $business->id;
				$businessArry['owner_name'] = $business->owner_name;
				$businessArry['about'] = $business->about;
				$businessArry['services'] = $business->services;
				$businessArry['promotions'] = $business->promotions;
				$businessArry['web_address'] = $business->web_address;
				$businessArry['facebook_link'] = $business->facebook_link;
				$businessArry['insta_link'] = $business->insta_link;
				$businessArry['whatsapp_link'] = $business->whatsapp_link;
				$businessArry['youtube_link'] = $business->youtube_link;
				$businessArry['email_publish'] = $business->email_publish;
				$businessArry['phone_publish'] = $business->phone_publish;
				$businessArry['is_received_newsletter'] = ($business->is_received_newsletter == 0) ? '' : 'checked="checked"';
				$businessArry['images'][$business->image_type][$business->image_id] = $business->image_name;
			}
		} else {
			echo '<script> location.href="http://faceland.co.il"; </script>';
        	die;
		}
	}
	$businessAdvertiseFrom = '';	
	$businessAdvertiseFrom .= '
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
	<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
	<script src="https://jqueryvalidation.org/files/lib/jquery.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>
	<script src="'.get_template_directory_uri().'/custom.validation.js" ></script>
	<link href="https://lara.gesmansys.com/assets/stylesheets/plugins/select2/select2.css" rel="stylesheet" />
	<script src="https://lara.gesmansys.com/assets/javascripts/plugins/select2/select2.js"></script>
	<form class="wpuf-form-add wpuf-style business-white-forms01 advertising-forms01" action="'.$dbprocess.'" method="post" id="" name="advertise_business" enctype="multipart/form-data" >
		<input type="hidden" name="BR_advertise_business" value="'. $businessArry['bid'] .'" id="BR_advertise_business" />
		<input type="hidden" name="delete_business_images" id="delete_business_images" value="" />
		<div class="business-form-root-div">
			<div class="bs-form-div">
				<div class="white-header-top addBusiness-form-top">
					<div class="addBusiness-first-line">
						<h2>המשך רישום לצורך פרסום העסק בלוח עסקים בעמקים</h2>
					</div>
					<div class="addBusiness-second-line">
						<span>כדי שהעסק שלך יפורסם בלוח עסקים בעמקים, יש למלא פרטים נוספים:</span>
					</div>
				</div>
				<div class="white-body-form-div">
					<div class="white-row-color-div border-bottom01">
						<div class="form-row">
							<div class="grid-auto grid-50">
								<div class="form-group">
									<label for="business_owner_name_539">שם בעל/ת העסק <span class="required">*</span></label>
									<div class="input-field-group">
										<input type="text" class="form-control" placeholder="שם בעל/ת העסק" name="business_owner_name" value="'.$businessArry['owner_name'].'"/>
									</div>
								</div>
							</div>	
						</div>
						<div class="form-row">
							<div class="grid-auto grid-100">
								<div class="form-group">
									<label>אודות העסק (מומלץ לכתוב 2 משפטים) <span class="required">*</span></label>
									<div class="input-field-group">
										<textarea class="textarea-form" placeholder="טקסט חופשי..." name="about_the_business" value="" >'.$businessArry['about'].'</textarea>
									</div>
								</div>
							</div>	
						</div>
						<div class="form-row">
							<div class="grid-auto grid-100">
								<div class="form-group">
									<label>שירותים / מוצרים (מומלץ לכתוב 4-3 סעיפים) <span class="required">*</span></label>
									<div class="input-field-group">
										<textarea class="textarea-form" placeholder="טקסט חופשי..." name="business_services" value="" >'.$businessArry['services'].'</textarea>
									</div>
								</div>
							</div>	
						</div>
						<div class="form-row">
							<div class="grid-auto grid-100">
								<div class="form-group">
									<label>הודעות ומבצעים (מומלץ לכתוב 2 משפטים) <span class="required">*</span></label>
									<div class="input-field-group">
										<textarea class="textarea-form" placeholder="טקסט חופשי..." name="business_post_promotions" value="" >'.$businessArry['promotions'].'</textarea>
									</div>
								</div>
							</div>	
						</div>
						<div class="form-row">
						
							<div class="grid-auto grid-100">
								<div class="form-group">
									<label>בחר קטגוריות <span class="required">*</span></label>
									<div class="input-field-group">
										<input type="text" name="business_category[]" class="advertise_select_category" />
									</div>
								</div>
							</div>
							<div class="grid-auto grid-100">
								<div class="form-group">
									<label>בחר קטגוריות משנה <span class="required">*</span></label>
									<div class="input-field-group">
										<input type="text" name="business_subcategory[]" class="advertise_select_sub_category" />
									</div>
								</div>
							</div>

						</div>
					</div>
					<div class="clearfix"></div>
					<div class="white-row-color-div border-bottom01">
						<div class="inner-heading-div">
							<h3>עוד על העסק:</h3>
						</div>
						<div class="clearfix"></div>
						<div class="form-row">
							<div class="grid-auto grid-50">
								<div class="form-group">
									<label>כתובת אתר אינטרנט <span class="required">*</span></label>
									<div class="input-field-group">
										<input type="text" class="form-control" placeholder="הכנס דוא״ל" name="business_web_address" value="'.$businessArry['web_address'].'" />
									</div>
								</div>
							</div>
							<div class="grid-auto grid-50">
								<div class="form-group">
									<label>קישור לפייסבוק <span class="required">*</span></label>
									<div class="input-field-group">
										<input type="text" class="form-control" placeholder="הכנס דוא״ל" name="business_facebook_link" value="'.$businessArry['facebook_link'].'" />
									</div>
								</div>
							</div>	
						</div>
						<div class="form-row">
							<div class="grid-auto grid-50">
								<div class="form-group">
									<label>קישור לאפליקציה <span class="required">*</span></label>
									<div class="input-field-group">
										<input type="text" class="form-control" placeholder="" name="business_whatsapp_link" value="'.$businessArry['whatsapp_link'].'" />
									</div>
								</div>
							</div>
							<div class="grid-auto grid-50">
								<div class="form-group">
									<label>קישור לאינסטגרם <span class="required">*</span></label>
									<div class="input-field-group">
										<input type="text" class="form-control" placeholder="הכנס דוא״ל" name="business_insta_link" value="'.$businessArry['insta_link'].'" />
									</div>
								</div>
							</div>
						</div>
						<div class="form-row">
							<div class="grid-auto grid-50">
								<div class="form-group">
									<label>קישור ליו-טיוב <span class="required">*</span></label>
									<div class="input-field-group">
										<input type="text" class="form-control" placeholder="" name="business_youtube_link" value="'.$businessArry['youtube_link'].'" />
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="clearfix"></div>
					<div class="white-row-color-div border-bottom01">
						<div class="inner-heading-div">
							<h3>פרטים ליצירת קשר: <span class="required">*</span></h3>
						</div>
						<div class="clearfix"></div>
						<div class="form-row">
							<div class="grid-auto grid-50">
								<div class="form-group">
									<label>טלפון <span class="required">*</span></label>
									<div class="input-field-group">
										<input type="text" class="form-control" placeholder="הכנס דוא״ל" name="business_publish_phone" value="'.$businessArry['phone_publish'].'" />
									</div>
								</div>
							</div>
							<div class="grid-auto grid-50">
								<div class="form-group">
									<label>דואר אלקטרוני <span class="required">*</span></label>
									<div class="input-field-group">
										<input data-identify="advertise_business" field-identify="email_publish" type="email" class="form-control check-email" name="business_email_publish" value="'.$businessArry['email_publish'].'" />
									</div>
								</div>
							</div>	
						</div>
						<div class="form-row">
							<div class="grid-auto grid-100">
								<div class="form-group form-group-checkbox">
									<label class="wpuf-checkbox-inline pt-40">
										<input type="checkbox" class="wpuf_business_is_received_newsletter" name="business_publish_get_newsletter" value="business_is_received_newsletter" value="'.$businessArry['is_received_newsletter'].'" />
											אני מעוניין/ת לקבל ניוזלטר ועדכונים נוספים ממעברים בעמק                        
									</label>
								</div>	
							</div>
						</div>
					</div>
					<div class="clearfix"></div>					
					<div class="white-row-color-div">
						<div class="inner-heading-div">
							<h3>הוספת קבצים: </h3>
						</div>
						<div class="clearfix"></div>
						<div class="form-row">
							<div class="grid-auto grid-50">
								<div class="image-input-group-root">';
								if(count($businessArry['images']) > 0 && array_key_exists('logo', $businessArry['images'])) {
									foreach($businessArry['images']['logo'] as $imgKey=>$image) {
									$imgurl = content_url('businessGallery/').$image; 
									$businessAdvertiseFrom .= '
										<div class="form-group fileinput-group fileinput-group-right">
											<label class="control-label" for="business_logo">הוסף לוגו עסקי <i class="icon-plus"></i></label>
											<div class="input-field-group">
												<input id="business_logo" name="business_logo" type="file" class="form-control input-md" />
											</div>
											<div class="show-image-div imgshow">
												<img src="'.$imgurl.'" title="'.$businessArry['owner_name'].'" alt="business-logo-image" class="img-responsive img-gallery-add uploaded_business_logo">
											</div>
										</div>';
									}
								} else {
									$businessAdvertiseFrom .= '
										<div class="form-group fileinput-group fileinput-group-right">
											<label class="control-label" for="business_logo">הוסף לוגו עסקי <i class="icon-plus"></i></label>
											<div class="input-field-group">
												<input id="business_logo" name="business_logo" type="file" class="required form-control input-md" />
											</div>
											<div class="show-image-div imgshow imgshow1">
												<img src="#" title="Business logo" alt="Business logo" style="display:none;" class="img-responsive img-gallery-add create_business_logo" />
											</div>
										</div>';
								}
									// At edit form time get the no. of clicks (count)(images) 
								$NoBlocks = (array_key_exists('images', $businessArry) && array_key_exists('gallery', $businessArry['images'])) ? count($businessArry['images']['gallery']) : 0;
								$businessAdvertiseFrom .= '
								</div>
							</div>								
							<div class="grid-auto grid-50">
								<div class="leftinput-root-img" id="businessImages">	
								<label class="control-label business_image_add_btn" for="business_gallery">הוספת תמונות שונות<i class="icon-plus"></i></label>
								<input type="hidden" value="'.$NoBlocks.'" id="currentBlockNo" />
								';
								if(count($businessArry['images']) > 0 && array_key_exists('gallery', $businessArry['images'])) {
									$CC = 1;
									foreach($businessArry['images']['gallery'] as $imgKey=>$image) {
									$imgurl = content_url('businessGallery/').$image; 
									$businessAdvertiseFrom .= '
										<div class="width-auto-right" id="image'.$CC.'" name="image'.$CC.'">
											<div class="image-input-group-root">
												<div class="width-imge">
													<div class="form-group fileinput-group fileinput-group-left show-image-group-root">
														<button type="button" id="remove'.$CC.'" data-delete-id="'.$imgKey.'" data-value="image'.$CC.'" class="btn btn-danger remove-me" onclick="removeElement(this)">
															<span class="remove-icon fa fa-close"></span>
														</button>
														<label class="control-label" for="business_gallery'.$CC.'" style="display:none;">Add Image</label>
														<div class="input-field-group">
															<input id="business_gallery'.$CC.'" name="business_gallery[]" type="file" class="form-control input-md">
														</div>
														<div class="show-image-div">
															<img src="'.$imgurl.'" title="'.$businessArry['owner_name'].'" alt="business-gallery-image" class="img-responsive img-gallery-add">
														</div>
													</div>
												</div>
											</div>
										</div>';
										$CC++;
									}
								} else {
									$businessAdvertiseFrom .= '
										<div id="image0" name="image0">
																												
										</div>
									';
								}
								$businessAdvertiseFrom .= '
								</div>
							</div>	
						</div>
					</div>
					<div class="clearfix"></div>					
					<div class="btn-row-div">
						<button class="btn btn-submit" name="advertise_business" value="advertise_business" type="submit">פרסום העסק בלוח העסקים
							<i class="icon-arrow-left" ></i>
						</button>
					</div>				
				</div>
			</div>
		</div>
	</form>
	';
	if (!empty($businessArry['bid'])) {
	$businessAdvertiseFrom .= '
		<script>
			jQuery(document).ready(function(){
				categoriesRun();
			});
		</script>
	';	
	}
	return $businessAdvertiseFrom;
}
add_shortcode( 'business_advertise_form', 'business_advertise_form_shortcode' );


// Forms html business contacts // shortcode == business_contacts_form
function business_contacts_form_shortcode() {
	// get added business
	$selected = 'selected="selected"';
	$localities = array(
		'1' => 'א.ת. אלון תבור',
		'2' => 'א.ת. יקנעם',
		'3' => 'א.ת. מבוא כרמל',
		'4' => 'א.ת. מבואות גלבוע',
	);
	$roles = array(
		'1' => 'Project Manager',
		'2' => 'HR'
	);
	$business_sizes = array(
		'1' => 'עסק זעיר - עד 5 עובדים',
		'2' => 'עסק קטן - 6-50 עובדים',
		'3' => 'עסק בינוני - 51-100 עובדים',
		'4' => 'עסק גדול - מעל 100 עובדים',
	);
	$educations = array(
		'1' => 'Graduation',
		'2' => 'Post Graduation'
	);
	$experiences = array(
		'1' => 'less than a year',
		'2' => '1 Year',
		'3' => '2 Years',
		'4' => '3 Years',
		'5' => '4 Years'
	);
	$professional_fields = array(
		'1' => 'Professional fields 01',
		'2' => 'Professional fields 02',
		'3' => 'Professional fields 03',
		'4' => 'Professional fields 04',
		'5' => 'Professional fields 05'
	);
	
	if(!session_id()) {
		session_start();
	}
	if(array_key_exists('login_user_detail', $_SESSION) || is_user_logged_in()) {
		$user = $_SESSION['login_user_detail'];
		global $wpdb;
		$table = $wpdb->prefix.'advertise_business';
		$query = 'SELECT * FROM '.$table.' WHERE user_id = "'.$user->id.'"';
		$result = $wpdb->get_results($query);
		if(count($result) > 0) {
		$resultArr = $result[0];
		$dbprocess = get_template_directory_uri().'/database-process.php';
		$businessAdvertiseFrom = '';	
		$businessAdvertiseFrom .= '
		<div class="business-form-root-div">
			<div class="bs-form-div">
				<div class="white-header-top addBusiness-form-top color-update-newjob01">
					<div class="addBusiness-first-line">
						<h2>המשך רישום לצורך פרסום העסק בלוח עסקים בעמקים</h2>
					</div>
					<div class="addBusiness-second-line">
						<span>כדי שהעסק שלך יפורסם בלוח עסקים בעמקים, יש למלא פרטים נוספים:</span>
					</div>
				</div>
				<div class="white-body-form-div">
					<div class="white-row-color-div business_details_body">
						<div class="form-row">
							<div class="grid-auto grid-50">
								<div class="form-group">
									<label for="business_detail_name">שם העסק </label>
									<div class="input-field-group">
										<input type="text" class="form-control" readonly="readonly" id="business_detail_name" value="'.$resultArr->owner_name.'" />
									</div>
								</div>
							</div>	
						</div>
						<div class="form-row">
							<div class="grid-auto grid-50">
								<div class="form-group business_detail_work_place_place_error">
									<label for="business_detail_work_place">מקום העבודה  <span class="required">*</span></label>
									<div class="input-field-group">
										<select name="business_detail_work_place" id="business_detail_work_place" class="job_business_location001">
											<option value="">בינוני</option>';
											foreach($localities as $Lkey=>$Lval) {
												if ($Lkey == $user->locality) {
													$businessAdvertiseFrom .= '<option value="'.$Lkey.'" '.$selected.'>'.$Lval.'</option>';
												} else {
													$businessAdvertiseFrom .= '<option value="'.$Lkey.'">'.$Lval.'</option>';
												}
											}
											$businessAdvertiseFrom .= '
										</select>
										<span class="dropdown-arrow"></span>
									</div>
								</div>
							</div>	
						</div>
					</div>
					<div class="form-row">
						<div class="grid-auto grid-50">
							<div class="form-group business_detail_contacts_person_error">
								<label for="business_detail_contact">בחירת איש קשר לצורך קבלת קורות חיים  <span class="required">*</span></label>
								<div class="input-field-group">
									<select name="business_detail_contact" id="business_detail_contact">
										<option value=""></option>
									</select>
									<span class="dropdown-arrow"></span>
								</div>
							</div>
						</div>
						<div class="grid-auto grid-50">
							<div class="form-group btn-add-group-row">
								<div class="padding-top-auto">
									<button class="btn btn-add" id="business_contact_add_new" class="business_contact_add_new"> <i class="edit-plus-icon icon-black pl-5"></i>הוספת איש קשר <i class="icon-arrow-left"></i></button>
								</div>	
							</div>
						</div>
					</div>
					<div class="clearfix"></div>				
				</div>
				<div class="clearfix"></div>				
			</div>
		</div>
		<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
		<script src="https://jqueryvalidation.org/files/lib/jquery.js"></script>		
		<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
		<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>
		<script src="'.get_template_directory_uri().'/custom.validation.js"></script>		
		<div id="field">	
			<form id="field0" name="field0">
				
			</form>
		</div>
		<form class="wpuf-form-add wpuf-style business-white-forms01" action="'.$dbprocess.'" method="post" id="add_new_job" name="add_new_job" enctype="multipart/form-data" >
			<div class="add_new_job_form">
				<div class="white-body-form-div white-body-space-form-div new-job-section3">
					<div class="white-row-color-div border-bottom01">
						<input type="hidden" name="BR_job_create" value="" />
						<input type="hidden" id="job_business_name" name="job_business_name" value="'.$resultArr->owner_name.'" />
						<input type="hidden" id="job_business_id" name="job_business_id" value="'.$resultArr->id.'" />
						<input type="hidden" id="job_business_location" name="job_business_location" value="" />
						<input type="hidden" id="job_business_contact_selected" name="job_business_contact_selected" value="" />
						<div class="form-row">
							<div class="grid-auto grid-50">
								<div class="form-group">
									<label for="job_professional_field" class="control-label">תחום מקצוע <span class="required">*</span> </label>
									<div class="input-field-group">
										<select name="job_professional_field" id="job_professional_field" required >
											<option value="">תחום מקצוע </option>';
											foreach($professional_fields as $PFkey=>$PFval) {
												$businessAdvertiseFrom .= '
													<option value="'.$PFkey.'">'.$PFval.'</option>
												';
											}
											$businessAdvertiseFrom .= '
										</select>
									</div>
								</div>
							</div>
							<div class="grid-auto grid-50">
								<div class="form-group">
									<label for="job_role" class="control-label">תפקיד <span class="required">*</span> </label>
									<div class="input-field-group">
										<select name="job_role" id="job_role" >
											<option value="">רשימה</option>';
											$businessAdvertiseFrom .= '
										</select>
										<span class="dropdown-arrow"></span>
									</div>
								</div>
							</div>
						</div>
						<div class="form-row">
							<div class="grid-auto grid-25">
								<div class="form-group">
									<label for="education" class="control-label">השכלה  <span class="required">*</span></label>
									<div class="input-field-group">
										<select id="job_education" name="job_education">
											<option value=""> </option>';											
											foreach($educations as $Ekey=>$Eval) {
												$businessAdvertiseFrom .= '
													<option value="'.$Ekey.'">'.$Eval.'</option>
												';
											}
											$businessAdvertiseFrom .= '
										</select>
										<span class="dropdown-arrow"></span>
									</div>
								</div>
							</div>
							<div class="grid-auto grid-25">
								<div class="form-group">
									<label for="job_experience" class="control-label">שנות ניסיון  <span class="required">*</span></label>
									<div class="input-field-group">
										<select id="job_experience" name="job_experience">
											<option value=""> </option>';											
											foreach($experiences as $Exkey=>$Exval) {
												$businessAdvertiseFrom .= '
													<option value="'.$Exkey.'">'.$Exval.'</option>
												';
											}
											$businessAdvertiseFrom .= '
										</select>
										<span class="dropdown-arrow"></span>
									</div>
								</div>
							</div>						
							<div class="grid-auto grid-50 grid-tablet100">
								<div class="form-group">
									<label for="job_scope" class="control-label">היקף המשרה  <span class="required">*</span></label>
									<div class="input-field-group checkbox-group checkbox03">
										<div class="checkbox-col">
											<label for="job_scope_full" class="wpuf-checkbox-inline">
												<input type="checkbox" id="job_scope_full" id="job_scope[]" value="1" />
												מלא
											</label>
										</div>
										<div class="checkbox-col">
											<label for="job_scope_partial" class="wpuf-checkbox-inline">
												<input type="checkbox" id="job_scope_partial" name="job_scope[]" value="2" />
												חלקי
											</label>
										</div>
										<div class="checkbox-col">
											<label for="job_scope_flexible" class="wpuf-checkbox-inline">
												<input type="checkbox" id="job_scope_flexible" name="job_scope[]" value="3" />
												גמיש
											</label>
										</div>
										<div class="checkbox-col">
											<label for="job_scope_temporary" class="wpuf-checkbox-inline">
												<input type="checkbox" id="job_scope_temporary" name="job_scope[]" value="4" />
												משרה זמנית
											</label>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="form-row">
							<div class="grid-auto grid-50">
								<div class="form-group">
									<label for="job_area" class="control-label">אזור <span class="required">*</span></label>
									<div class="input-field-group">
										<select id="job_area" name="job_area">
											<option value=""> אזור</option>';
											foreach($localities as $Lkey=>$Lval) {
												$businessAdvertiseFrom .= '
													<option value="'.$Lkey.'">'.$Lval.'</option>
												';
											}
											$businessAdvertiseFrom .= '
										</select>
										<span class="dropdown-arrow"></span>
									</div>
								</div>
							</div>
						</div>
						<div class="form-row">
							<div class="grid-auto grid-100">
								<div class="form-group">
									<label class="control-label">היקף המשרה <span class="required">*</span></label>
									<div class="input-field-group checkbox-group auto-checkbox-group checkbox01">
										<div class="checkbox-col">
											<label for="job_position_favorite" class="wpuf-checkbox-inline">
												<input type="checkbox" id="job_position_favorite" name="job_position[]" value="1">
												עבודה מועדפת – חיילים משוחררים
											</label>
										</div>
										<div class="checkbox-col">
											<label for="job_position_parent" class="wpuf-checkbox-inline">
												<input type="checkbox" id="job_position_parent" name="job_position[]" value="2">
												משרות אם – הורים עובדים
											</label>
										</div>
										<div class="checkbox-col">
											<label for="job_position_student" class="wpuf-checkbox-inline">
												<input type="checkbox" id="job_position_student" name="job_position[]" value="3">
												משרות לסטודנטים
											</label>
										</div>
									</div>
								</div>
							</div>
						</div>	
						
						<div class="form-row">
							<div class="grid-auto grid-50">
								<div class="form-group">
									<label for="job_title">תאריך התחלה <span class="required">*</span></label>
									<div class="input-field-group">
										<input type="text" class="form-control" id="job_start_date" name="job_start_date" value="" />
									</div>
								</div>
							</div>
							<div class="grid-auto grid-50">
								<div class="form-group">
									<label for="job_title">תאריך סיום <span class="required">*</span></label>
									<div class="input-field-group">
										<input type="text" class="form-control" id="job_end_date" name="job_end_date" value="" />
									</div>
								</div>
							</div>	
						</div>
						
					</div>	
					<div class="clearfix"></div>
					<div class="white-row-color-div ">
						<div class="form-row">
							<div class="grid-auto grid-50">
								<div class="form-group">
									<label for="job_title">כותרת המשרה * (ניתן להקליד עד 60 תוים) <span class="required">*</span></label>
									<div class="input-field-group">
										<input type="text" class="form-control" id="job_title" name="job_title" value="" />
									</div>
								</div>
							</div>	
						</div>
						<div class="form-row">
							<div class="grid-auto grid-100">
								<div class="form-group">
									<label for="job_description ">תיאור התפקיד (משימות ותחומי עיסוק מרכזיים)  <span class="required">*</span></label>
									<div class="input-field-group">
										<textarea class="textarea-form" placeholder="טקסט חופשי..." id="job_description" name="job_description"></textarea>
									</div>
								</div>
							</div>	
						</div>
						<div class="form-row">
							<div class="grid-auto grid-100">
								<div class="form-group">
									<label for="job_requirement">דרישות התפקיד (השכלה, יכולות וניסיון קודם)  <span class="required">*</span></label>
									<div class="input-field-group">
										<textarea class="textarea-form" placeholder="טקסט חופשי..." id="job_requirement" name="job_requirement"></textarea>
									</div>
								</div>
							</div>	
						</div>
						<div class="form-row">
							<div class="grid-auto grid-100">
								<div class="form-group">
									<label for="job_remarks">הערות  <span class="required">*</span></label>
									<div class="input-field-group">
										<textarea class="textarea-form" placeholder="טקסט חופשי..." id="job_remarks" name="job_remarks"></textarea>
									</div>
								</div>
							</div>	
						</div>
						<div class="form-row">
							<div class="grid-auto grid-100">
								<div class="inn-questions-forms-div checkbox02">
									<h4>הוספת שאלות לבחינת התאמה לתפקיד  <span class="required">*</span></h4>
									<p>
										<span class="span-block">באפשרותך להוסיף שלוש שאלות (שאלות כן/לא בלבד!), אשר רק מועמדים שישיבו על שלושתן בחיוב יוכלו לשלוח את קורות החיים שלהם למשרה זו.</span>							
										<span class="span-block">(שאלות לדוגמה: האם יש לך ניסיון בניהול עובדים? האם עסקת בתפקידי הדרכה?)</span>
									</p>									
									<div class="input-log-group-root">
										<div class="input-row-full">
											<div class="per-20">שאלה מס׳ 1</div>
											<div class="per-80"><input type="text" class="form-control" id="question1" name="question1" /></div>
										</div>
										<div class="input-row-full">
											<div class="per-20">שאלה מס׳ 2</div>
											<div class="per-80"><input type="text" class="form-control" id="question2" name="question2" /></div>
										</div>
										<div class="input-row-full">
											<div class="per-20">שאלה מס׳ 3</div>
											<div class="per-80"><input type="text" class="form-control" id="question3" name="question3" /></div>
										</div>
									</div>
								</div>	
							</div>	
						</div>
					</div>
					<div class="clearfix"></div>					
					<div class="btn-row-div btn-new-job-submit">
						<button class="btn btn-submit btn-job-submit" name="new_job_create" type="submit">פרסום משרה <i class="icon-arrow-left"></i></button>
					</div>	
				</div>
			</div>
		</form>';
		$businessAdvertiseFrom .= '
			<script>
				jQuery("#job_start_date").datepicker({
					numberOfMonths: 2,
					onSelect: function(selected) {
						jQuery("#job_end_date").datepicker("option","minDate", selected)
					}
				});
				jQuery("#job_end_date").datepicker({ 
					numberOfMonths: 2,
					onSelect: function(selected) {
						jQuery("#job_start_date").datepicker("option","maxDate", selected)
					}
				});
			</script>
		';

		return $businessAdvertiseFrom;
		} else {
			echo '<script> location.href="'.get_permalink(651).'?need=business" </script>';
		}
	}
}
add_shortcode( 'business_contacts_form', 'business_contacts_form_shortcode' );


function search_bar_shortcode() {	
	$html = '';
	$html .= '<div class="company-div company-div-header">
            <div class="container">
                <div class="clearfix"></div>
                <div class="business-white-forms01">
                    <div class="white-body-form-div white-body-space-form-div">
                        <div class="white-row-color-div form-white-minus-300">                        
                            <div class="form-row-title form-col-10">
                                <div class="form-title-heading">
                                    <span class="form-title">חיפוש</span>
                                </div>	
                            </div>
                            <div class="form-row form-row-20 form-col-90">
                                <div class="grid-auto grid-33">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label"> תחום העסק:</label>
                                        <div class="input-field-group">
                                            <select id="search_business_area" name="search_business_area">
                                                <option value="">בחר</option>
                                            </select>
                                            <span class="dropdown-arrow"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="grid-auto grid-33">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="business_contact_role">שירות / מוצר:</label>
                                        <div class="input-field-group">
                                            <select id="search_service_product" name="search_service_product">
                                                <option value="">בחר</option>
                                            </select>
                                            <span class="dropdown-arrow"></span>
                                        </div>
                                    </div>
                                </div>						
                                <div class="grid-auto grid-33">
                                    <div class="form-group">
                                        <label class="control-label">טקסט חופשי:</label>
                                        <div class="input-field-group">
                                            <input type="text" placeholder="...ןאכ ללמה תא סנכה" class="form-control input-md" id="search_free_text" name="search_free_text">
                                        </div>
                                    </div>
                                </div>						
                            </div>                          						
                        </div>                      
                        <div class="form-row form-row-btn-300">
                            <div class="button-abs-bl-auto">
                                <div class="btn-submit-group">
                                    <button class="btn btn-submit" id="search_business_btn" name="search_business_btn" type="button">חיפוש עסק<i class="icon-arrow-left"></i></button>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
		</div>';
		
		return $html;
}
add_shortcode( 'business_search_bar', 'search_bar_shortcode' );

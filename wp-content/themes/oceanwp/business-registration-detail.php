<?php
/**
 * Template Name: business-registration-detail
 */


get_header(); 
if(!session_id()) {
	session_start();
}
if (array_key_exists('login_user_detail', $_SESSION)) {
$localities = array(
    '1' => 'א.ת. אלון תבור',
    '2' => 'א.ת. יקנעם',
    '3' => 'א.ת. מבוא כרמל',
    '4' => 'א.ת. מבואות גלבוע',
);
$business_sizes = array(
    '1' => 'עסק זעיר - עד 5 עובדים',
    '2' => 'עסק קטן - 6-50 עובדים',
    '3' => 'עסק בינוני - 51-100 עובדים',
    '4' => 'עסק גדול - מעל 100 עובדים',
);
?>
<section class="business-profile-section">
    <div class="business-profile-div">
        <div class="container">
            <h1>פרופיל עסק</h1>
            <div class="clearfix"></div>
            <div class="profilebox-row-root">
                <div class="clearfix"></div>
                <?php
                    //get business User details
                    $user = $_SESSION['login_user_detail'];
                    global $wpdb;
                    $user_table = $wpdb->prefix.'business_users';
                    $query = 'SELECT * FROM '.$user_table.' WHERE id = '.$user->id;
                    $regitered_business = $wpdb->get_results($query);
                    if(count($regitered_business) > 0) {
                        $regitered_business = $regitered_business[0];
                ?>
                <div class="profilebox-div profilebox-div01">
                    <div class="profilebox-header-div">
                        <div class="left-side-header">
                            <a href="<?php echo get_permalink(505).'?bid='.$regitered_business->id; ?>" class="link-btn">עריכת פרטי העסק <i class="icon-arrow-left"></i></a>
                            <a href="<?php echo get_permalink(505).'?bid='.$regitered_business->id; ?>" class="icon-btn"><i class="icon-edit"></i></a>
                        </div>
                        <div class="right-side-header">
                            <h3><?php echo $regitered_business->business_name; ?></h3>
                        </div>
                    </div>                    
                    <div class="profilebox-body-div">
                        <div class="row-div mb-10">
                            <h5><?php echo $regitered_business->business_service; ?></h5>
                            <?php
                            $locality = '--';
                            foreach($localities as $Lkey=>$Lval) {
                                if ($Lkey == $regitered_business->locality) {
                                    $locality = $Lval;                                        
                                }
                            }
                            ?>
                            <p><?php echo $locality; ?></p>
                        </div>
                        <div class="row-div">
                            <p class="mb-5"><a href="mailto:<?php echo $regitered_business->email; ?>"><?php echo $regitered_business->email; ?></a></p>
                            <?php
                            $business_size = '--';
                            foreach($business_sizes as $Skey=>$sval) {
                                if ($Skey == $regitered_business->size) {
                                    $business_size = $Lval;                                        
                                }
                            }
                            ?>
                            <p><?php echo $business_size; ?></p>
                        </div>    
                    </div>
                </div>
                <?php 
                    }
                ?>
                <div class="clearfix"></div>
                <div class="gridbox-two-div">
                    <div class="box-section-02 color-box-01">
                        <div class="inner_business_box_row">
                            <div class="width50_box">
                                <div class="two_section_left">
                                    <div class="top-section-box">
                                        <h3> <span class="span-block">פרסום העסק </span><span class="span-block">בלוח עסקים בעמקים</span> </h3>
                                    </div>
                                    <?php 
                                        $pageLink = get_permalink(542);
                                        $user = $_SESSION['login_user_detail'];
                                        global $wpdb;
                                        $table = $wpdb->prefix.'advertise_business';
                                        $query = 'SELECT * FROM '.$table.' WHERE user_id = "'.$user->id.'"';
                                        $result = $wpdb->get_results($query);
                                        if(count($result) > 0) {
                                            $pageLink = get_permalink(651);
                                        }
                                    ?>
                                    <div class="two_section_buttons">
                                        <a href="<?php echo $pageLink; ?>"><i class="icon-arrow-left"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="width50_box">
                                <div class="two_section_left change-color2">
                                    <div class="top-section-box">
                                        <h3> <span class="span-block"> פרסום משרות </span> <span class="span-block">בלוח הדרושים</span> </h3>
                                    </div>	
                                    <div class="two_section_buttons">
                                        <a href="<?php echo get_permalink(654); ?>"><i class="icon-arrow-left"></i></a>
                                    </div>
                                </div>	
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>


            </div>
        </div>
    </div>
</section>

<?php } else {  ob_start();
    echo '<script> location.href="'.get_site_url().'" </script>';
} get_footer(); ?>
jQuery(".eael-post-elements-readmore-btn").html('<i class="icon-arrow-left"></i>');
jQuery(".eael-post-list-post .eael-post-list-content").append('<i class="icon-arrow-left"></i>');
jQuery('.wpuf-submit').append('<i class="icon-arrow-left"></i>');
jQuery(".select-element .wpuf-fields").append('<span class="dropdown-arrow"></span>');

// AJAX for login process
jQuery('#loginButton').on('click', function () {
    var getFormData = jQuery('#loginForm').serializeArray();
    jQuery.ajax({
        url: DATABSE_PROCESS_URL,
        type: 'post',
        data: getFormData,
        cache: false,
        dataType: "json",
        success: function (response) {
            if (response.success) {
                jQuery('#loginForm input').css('border', 'none');
                jQuery('#desc').html(response.message);
                launch_toast();
                location.href = 'http://faceland.co.il/business-profile/';
            } else {
                jQuery('#desc').html(response.message);
                launch_toast();
                jQuery('#loginForm input').css('border', '1.5px solid red');
            }
        }
    });
});

function loader(e) {
    jQuery(e).after('<div class="loadersmall"></div>');
}

function removeLoader() {
    jQuery('.loadersmall').remove();
}

function launch_toast() {
    var x = document.getElementById("toast")
    x.className = "show";
    setTimeout(function () {
        x.className = x.className.replace("show", "");
        // Reload the current page without the browser cache
    }, 5000);
}

jQuery("#mobile-menu-icon").on('click', function () {
    // open a menu
    if (jQuery("#sidr").hasClass('right') == true) {
        jQuery("#sidr").removeClass('right');
        jQuery("#sidr").addClass('mobilemenu');

        jQuery(".sidr-class-toggle-sidr-close").on('click', function () {
            jQuery("#sidr").addClass('right');
            jQuery("#sidr").removeClass('mobilemenu');
        });

    } else { // close a menu
        jQuery("#sidr").addClass('right');
        jQuery("#sidr").removeClass('mobilemenu');
    }
});

jQuery(document).ready(function () {
    //action dynamic childs
    var next = 0;
    jQuery("#business_contact_add_new").click(function (e) {
        e.preventDefault();
        console.log(next);
        var addto = "#field" + next;
        var addRemove = "#field" + (next);
        next = next + 1;
        var removeBtn = '<button id="remove' + (next - 1) + '" data-value="business_contact' + next + '" class="btn btn-danger remove-me" type="button" onclick="removeContactBlock(this)"><span class="remove-icon fa fa-close"></span></button>';
        var newIn = '<form id="field' + next + '" name="business_contact' + next + '">' +
            '<input type="hidden" name="BR_contact_create" value="" />' +
            '<input type="hidden" name="business_id" class="dynamic_businessid" />' +
            '<div class="white-body-form-div white-body-space-form-div">' +
            '<div class="white-row-color-div">' +
            '<div class="inner-heading-div">' +
            '<h3> הוספת איש קשר לצורך קבלת קורות חיים </h3>' + removeBtn +
            '</div>' +
            '<div class="form-row">' +
            '<div class="grid-auto grid-33">' +
            '<div class="form-group">' +
            '<label class="control-label" for="business_contact_name">שם <span class="required">*</span></label>' +
            '<div class="input-field-group">' +
            '<input required id="business_contact_name' + next + '" name="business_contact_name" type="text" placeholder="שם" class="form-control input-md">' +
            '</div></div></div>' +
            '<div class="grid-auto grid-33">' +
            '<div class="form-group">' +
            '<label class="col-md-4 control-label" for="business_contact_role">תפקיד <span class="required">*</span></label>' +
            '<div class="input-field-group">' +
            '<select id="business_contact_role' + next + '" name="business_contact_role">' +
            '<option value="">רשימה</option>' +
            '<option value="1">Project Manager</option>' +
            '<option value="2">HR</option>' +
            '</select>' +
            '<span class="dropdown-arrow"></span>' +
            '</div></div></div>' +
            '<div class="grid-auto grid-33">' +
            '<div class="form-group">' +
            '<label class="control-label" for="business_contact_phone">טלפון <span class="required">*</span></label>' +
            '<div class="input-field-group">' +
            '<input id="business_contact_phone' + next + '" name="business_contact_phone" type="text" placeholder="טלפון" class="form-control input-md">' +
            '</div></div></div></div>' +
            '<div class="form-row">' +
            '<div class="grid-auto grid-50">' +
            '<div class="form-group">' +
            '<label class="col-md-4 control-label" for="busineess_contact_email">דוא"ל אישי של איש הקשר <span class="required">*</span></label>' +
            '<div class="input-field-group">' +
            '<input required id="busineess_contact_email' + next + '" name="busineess_contact_email" type="email" placeholder="הכנס דוא״ל" class="form-control input-md">' +
            '</div></div></div>' +
            '<div class="grid-auto grid-50">' +
            '<div class="form-group">' +
            '<label class="col-md-4 control-label" for="business_contact_email_resume">דוא"ל לקבלת קורות חיים <span class="required">*</span></label>' +
            '<div class="input-field-group">' +
            '<input required id="business_contact_email_resume' + next + '" name="business_contact_email_resume" type="email" placeholder="הכנס דוא״ל" class="form-control input-md">' +
            '</div></div></div></div>' +
            '<div class="form-row">' +
            '<div class="button-abs-bl-auto">' +
            '<div class="btn-submit-group">' +
            '<button class="btn btn-submit" type="button" data-value="field' + next + '" onclick="submitForm(this)" name="business_contact_save">רשמו אותי לאתר<i class="icon-arrow-left"></i></button>' +
            '</div></div></div></div>' +
            '<div class="clearfix"></div></div>' +
            '</div></form>';
        var newInput = jQuery(newIn);
        var removeButton = jQuery(removeBtn);
        jQuery(addto).after(newInput);
        // jQuery(addRemove).after(removeButton);
        jQuery("#field" + next).attr('data-source', jQuery(addto).attr('data-source'));
        jQuery("#count").val(next);
        jQuery('.dynamic_businessid').val(jQuery("#job_business_id").val());
        jQuery('.remove-me').click(function (e) {
            e.preventDefault();
            var fieldNum = this.id.charAt(this.id.length - 1);
            var fieldID = "#field" + fieldNum;
            jQuery(this).remove();
            jQuery(fieldID).remove();
        });
    });
});

// Dynamic form for business Gallery 
jQuery(document).ready(function () {
    //action dynamic childs
    var next = parseInt(jQuery('#currentBlockNo').val());
    jQuery(".business_image_add_btn").click(function (e) {
        e.preventDefault();
        console.log(next);
        var addto = "#image" + next;
        // var addRemove = "#image" + (next);
        next = next + 1;
        var removeBtn = '<button id="remove' + (next) + '" data-delete-id="' + next + '" data-value="image' + (next) + '" class="btn btn-danger remove-me" onclick="removeElement(this)" >' +
            '<span id="image' + next + '" class="remove-icon fa fa-close"></span></button>';
        var newIn = '<div class="width-auto-right" id="image' + next + '" name="image' + next + '">' +
            '<div class="image-input-group-root ">' +
            '<div class="width-imge">' +
            '<div class="form-group fileinput-group fileinput-group-left show-image-group-root">' +
            removeBtn +
            '<label class="control-label" for="business_gallery' + next + '">Add Image  <span class="required">*</span></label>' +
            '<div class="input-field-group">' +
            '<input id="business_gallery' + next + '" name="business_gallery[]" onchange="uploadImage(this)" type="file" class="required form-control input-md" >' +
            '</div>' +
            '<div class="show-image-div business_gallery' + next + 'dynamic">' +
            '<img src="" alt="img" id="business_gallery' + next + 'image" class="img-responsive img-gallery-add" style="display:none;">' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>';
        '</div>';

        var newInput = jQuery(newIn);
        var removeButton = jQuery(removeBtn);
        jQuery(addto).after(newInput);
        // jQuery(addRemove).after(removeButton);
        jQuery("#image" + next).attr('data-source', jQuery(addto).attr('data-source'));
        jQuery("#count").val(next);
        jQuery('.remove-me').click(function (e) {
            e.preventDefault();
            var fieldNum = this.id.charAt(this.id.length - 1);
            var fieldID = "#image" + fieldNum;
            jQuery(this).remove();
            jQuery(fieldID).remove();
        });
    });
});

// set business work place and business contact in job create form at page load
setPropertiesInJobForm(jQuery('#business_detail_contact').val(), jQuery('#business_detail_work_place').val());
// function to get business contact from database at page load
getAllExistingBusinessesContacts();
// function to get business contact from database
function getAllExistingBusinessesContacts(selected = '') {
    console.log('fatch data from business contact');
    var $businessContacts = '';
    jQuery('#business_detail_contact').html('');
    jQuery.ajax({
        url: DATABSE_PROCESS_URL,
        type: 'post',
        data: {
            get_all_businesses_contacts: jQuery("#job_business_id").val()
        },
        cache: false,
        dataType: "json",
        success: function (response) {
            if (response.success == true) {
                jQuery.each(response.data, function (index, val) {
                    $businessContacts = '<option value="' + val.id + '">' + val.name + '</option>';
                    jQuery('#business_detail_contact').append($businessContacts);
                });
                // set business contact in drop down which is lastly added
                jQuery('#business_detail_contact').val(selected);
                setPropertiesInJobForm(selected, '');
            } else {
                console.log('Error while fatching data of business contact');
                $businessContacts = '<option value="">לא נמצא קשר עסקי</option>';
                jQuery('#business_detail_contact').append($businessContacts);
            }
        }
    });
}

// Event trigger when drop down of buiness work place will be changed and set values in job create form
jQuery('#business_detail_work_place').change(function () {
    var business_work_place = jQuery(this).val();
    var businessContact = jQuery('#business_detail_contact').val();
    setPropertiesInJobForm(businessContact, business_work_place);
});

jQuery("#business_detail_contact").change(function () {
    var selectedBusinessContact = jQuery("#business_detail_contact").val();
    var business_work_place = jQuery('#business_detail_work_place').val();
    setPropertiesInJobForm(selectedBusinessContact, business_work_place);
});

// function for add valus in job create form such aswork locaion in busines contact person
function setPropertiesInJobForm(businessContact = '', business_work_place = '') {
    if (businessContact != '') {
        //set business contact id in create new job form
        jQuery("#job_business_contact_selected").val(businessContact);
    }
    if (business_work_place != '') {
        //set business word location in create new job form
        jQuery("#job_business_location").val(business_work_place);
    }
}

// Save business contacts process Jquery
function submitForm(e) {
    var btnDataValue = jQuery(e).attr('data-value');
    jQuery('#' + btnDataValue).valid();
    if (jQuery('#' + btnDataValue).valid()) {
        var btnDataValue = jQuery(e).attr('data-value');
        var businessContactFromData = jQuery('#' + btnDataValue).serializeArray();
        jQuery.ajax({
            url: DATABSE_PROCESS_URL,
            type: 'post',
            data: businessContactFromData,
            cache: false,
            dataType: "json",
            success: function (response) {
                if (response.success == true) {
                    console.log(response.message);
                    getAllExistingBusinessesContacts(response.data);
                    jQuery('#desc').html(response.message);
                    launch_toast();
                    jQuery('#' + btnDataValue).empty();
                } else {
                    jQuery('#desc').html(response.message);
                    launch_toast();
                }
            }
        });
    }
}

// upload business gallery image using ajax
function uploadImage(e) {
    var businessGalleryImage = new FormData();
    var files = jQuery(e)[0].files[0];
    var getId = jQuery(e).attr('id');
    loader(e); // add loading
    businessGalleryImage.append('FILE_upload_business_image', 'FILE_upload_business_image');
    businessGalleryImage.append('businessLogo', files);
    jQuery.ajax({
        url: DATABSE_PROCESS_URL,
        type: 'post',
        data: businessGalleryImage,
        contentType: false,
        processData: false,
        dataType: "json",
        success: function (response) {
            if (response.success == true) {
                console.log(response);
                removeLoader();
                jQuery('#' + getId + 'image').css('display', 'block');
                jQuery('#' + getId + 'image').attr('src', response.data);
                jQuery('label[for="' + getId + '"]').hide();
            } else {
                removeLoader();
                jQuery('#desc').html(response.message);
                launch_toast();
            }
        }
    });
}

// upload business logo using ajax
jQuery("#business_logo").on('change', function (e) {
    var businessLogo = new FormData();
    var files = jQuery(this)[0].files[0];
    businessLogo.append('FILE_upload_business_image', 'FILE_upload_business_image');
    businessLogo.append('businessLogo', files);
    loader(this);
    jQuery.ajax({
        url: DATABSE_PROCESS_URL,
        type: 'post',
        data: businessLogo,
        contentType: false,
        processData: false,
        dataType: "json",
        success: function (response) {
            if (response.success == true) {
                removeLoader();
                console.log(response.message);
                if (jQuery('input[name="BR_advertise_business"]').val() == '') {
                    jQuery('.create_business_logo').css('display', 'block');
                    jQuery('.create_business_logo').attr('src', response.data);
                } else {
                    jQuery('.uploaded_business_logo').attr('src', response.data);
                }
            } else {
                removeLoader();
                jQuery('#desc').html(response.message);
                launch_toast();
            }
        }
    });
});

function removeContactBlock(e) {
    var blockId = jQuery(e).attr('data-value');
    jQuery('form[name="' + blockId + '"]').empty();
}

var blankArr = [];

function removeElement(e) {
    var blockId = jQuery(e).attr('data-value');
    var deleteImageId = jQuery(e).attr('data-delete-id');
    blankArr.push(deleteImageId);
    jQuery('#delete_business_images').val(blankArr.toString());
    console.log(blankArr);
    jQuery('div[name="' + blockId + '"]').empty();
}

// check email address exists or not?
jQuery(".check-email").focusout(function () {
    var f = jQuery(this);
    var emailAddress = jQuery(this).val();
    var table = jQuery(this).attr('data-identify');
    var field = jQuery(this).attr('field-identify');
    console.log(emailAddress);
    jQuery.ajax({
        url: DATABSE_PROCESS_URL,
        type: 'post',
        data: {
            email: emailAddress,
            table: table,
            field: field,
            checkEmail: 1
        },
        dataType: "json",
        success: function (response) {
            console.log(response);
            if (response.success == true) {
                if (f.parent().find('label.unique').length > 0) {
                    f.next().remove();
                }
                f.after('<label class="error unique"> ' + response.message + ' </label>');
                jQuery('.btn-submit').prop('disabled', true);
            } else {
                if (f.parent().find('label.unique').length > 0) {
                    f.next().remove();
                    jQuery('.btn-submit').prop('disabled', false);
                }
            }
        }
    });
});

jQuery('.btn-submit').on('click', function () {
    if (jQuery(this).attr('disabled') != "disabled") {
        jQuery(this).prop('disabled', true);
        jQuery('.btn-submit').submit();
    }
    setTimeout(function () {
        jQuery(this).prop('disabled', false);
        jQuery('.btn-submit').submit();
    }, 5000);
});

// professionsla fields
jQuery("#job_professional_field").on('change', function () {
    var job_professional_field = jQuery(this).val();
    console.log('job_professional_field: ', job_professional_field);
    jQuery.ajax({
        url: DATABSE_PROCESS_URL,
        type: 'post',
        data: {
            professional_field: job_professional_field
        },
        dataType: "json",
        success: function (response) {
            console.log(response);
            if (response.success == true) {
                jQuery("#job_role").html('');
                $.each(response.data, function (index, val) {
                    jQuery("#job_role").append('<option value="' + index + '">' + val + '</option>');
                });
            } else {

            }
        }
    });
});

function categoriesRun() {
    jQuery(".advertise_select_category").select2({
        minimumInputLength: 1,
        tags: true,
        maximumSelectionSize: 5,
        minimumResultsForSearch: Infinity,
        multiple: true,
        allowClear: true,
        initSelection: function (element, callback) {
            jQuery.ajax({
                url: DATABSE_PROCESS_URL,
                type: 'post',
                data: {
                    edit_business: jQuery("#BR_advertise_business").val()
                },
                dataType: "json",
                success: function (response) {
                    callback(response.categories);
                }
            });
        },
        ajax: {
            url: DATABSE_PROCESS_URL,
            dataType: 'json',
            type: "POST",
            data: function (term) {
                return {
                    category: term
                };
            },
            results: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.name,
                            id: item.id
                        }
                    })
                };

            },
            cache: false
            // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
        }
    }).select2('val', []);

    jQuery(".advertise_select_sub_category").select2({
        tags: true,
        maximumSelectionSize: 10,
        minimumResultsForSearch: Infinity,
        multiple: true,
        initSelection: function (element, callback) {
            jQuery.ajax({
                url: DATABSE_PROCESS_URL,
                type: 'post',
                data: {
                    edit_business: jQuery("#BR_advertise_business").val()
                },
                dataType: "json",
                success: function (response) {
                    console.log(response.subcategories);
                    callback(response.subcategories);
                }
            });
        },
        ajax: {
            url: DATABSE_PROCESS_URL,
            dataType: 'json',
            type: "POST",
            data: function (term) {
                return {
                    subcategory: term,
                    selected_category: jQuery(".advertise_select_category").select2('val')
                };
            },
            results: function (data) {
                console.log(data);
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.name,
                            id: item.id
                        }
                    })
                };
            },
            cache: false
            // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
        }
    }).select2('val', []);

    jQuery(".advertise_select_category").on('change', function () {
        if (jQuery(this).val().length == 0) {
            jQuery(".advertise_select_sub_category").select2("val", "");
        }
    });
}

// ==============================================================================//
// ==================== Search business result process ==========================//
// ==============================================================================//

function getBusinessCategories(e) {
    // get Result using Ajax for business categories
    loader(e);
    jQuery.ajax({
        url: DATABSE_PROCESS_URL,
        type: 'post',
        data: {
            get_business_category: 'get_business_category'
        },
        dataType: "json",
        success: function (response) {
            if (response.success == true) {
                var $html = '';
                jQuery.each(response.data, function (index, val) {
                    $html += '<div class="grid-lg-25 grid-lg-auto search_page_category" data-value="' + val.id + '" >' +
                        '<div class="find-bs-card-box">' +
                        '<div class="img-thumb-group">' +
                        '<img src="http://faceland.co.il/wp-content/themes/oceanwp/assets/img/find-bs01.jpg" alt="find-bs" class="img-responsive img-find" />' +
                        '</div>' +
                        '<div class="text-content">' +
                        '<h3>' + val.title + '</h3>' +
                        '</div>' +
                        '</div>' +
                        '</div>';
                });
                jQuery('#search_business_categories').html($html);
                removeLoader();
                jQuery('.search_page_category').on('click', function () {
                    var catid = jQuery(this).attr('data-value');
                    getBusinessesByCategory(catid, 1);
                });
            } else {
                console.warm(response.message);
            }
        }
    });
}

// get businesses using category
function getBusinessesByCategory(categoryid = '', offset = 1) {
    // get Result using Ajax for business categories
    loader(jQuery('#search_business_lsiting'));
    jQuery.ajax({
        url: DATABSE_PROCESS_URL,
        type: 'post',
        data: {
            get_businesses_by_category: categoryid,
            offset: offset
        },
        dataType: "json",
        success: function (response) {
            if (response.success == true) {
                var $html = '';
                jQuery.each(response.data, function (index, val) {
                    $html += '<a href="' + val.link + '">' +
                        '<div class="grid-lg-50 grid-lg-auto business_detail" data-value="' + val.id + '">' +
                        '<div class=" post-article-box">' +
                        '<div class="img-thumb-right">' +
                        '<img src="' + val.image_url + '" class="img-responsive img-post" alt="">' +
                        '</div>' +
                        '<div class="content-txt-left">' +
                        '<h2>' + val.owner_name + '</h2>' +
                        '<h6>' + val.services + '</h6>' +
                        '<p><a href="tel:' + val.phone_publish + '">' + val.phone_publish + '</a></p>' +
                        '<p>' + val.promotions + '</p>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</a>';
                });
                jQuery('#search_business_lsiting').attr('data-value', (response.offset == 0) ? response.offset + 1 : response.offset);
                jQuery('#search_business_lsiting').attr('data-total', response.total_pages);
                jQuery('#search_business_lsiting').attr('data-cat', response.categoryid);
                jQuery('#search_business_lsiting').html($html);
                removeLoader();
                if (response.total_pages > 0) {
                    jQuery('#search_business_pagination').html('');
                    jQuery('#search_business_pagination').append(
                        '<li class="prev-li"><a data-value="' + 1 + '" class="pagination_item prev-link"><i class="icon-arrow-left"></i></a></li>'
                    );
                    for (let i = 1; i <= response.total_pages; i++) {
                        var actionPage = '';
                        if (jQuery('#search_business_lsiting').attr('data-value') == i) {
                            actionPage = 'active';
                        }
                        jQuery('#search_business_pagination').append(
                            '<li class="' + actionPage + '"><a data-value="' + i + '" class="pagination_item">' + i + '</a></li>'
                        );
                    }
                    jQuery('#search_business_pagination').append(
                        '<li class="next-li"><a data-value="' + response.total_pages + '" class="pagination_item next-link"><i class="icon-arrow-left"></i></a></li>'
                    );
                    jQuery(".pagination_item").on('click', function () {
                        var no = jQuery(this).attr('data-value');
                        var cat = jQuery('#search_business_lsiting').attr('data-cat');
                        getBusinessesByCategory(cat, no);
                    });
                }
            } else {
                console.log(response.message);
            }
        }
    });
}

// function getBusinessDetails(businessid = '', cat = '') {
//     // get Result using Ajax
//     jQuery.ajax({
//         url: DATABSE_PROCESS_URL,
//         type: 'post',
//         data: {
//             business_id: businessid,
//             cat_id: cat,
//             get_business_detail: 'get_business_detail',
//         },
//         dataType: "json",
//         success: function (response) {
//             console.log(response);
//             if (response.success == true) {
//                 jQuery("#job_role").html('');
//                 $.each(response.data, function (index, val) {
//                     jQuery("#job_role").append('<option value="' + index + '">' + val + '</option>');
//                 });
//             } else {

//             }
//         }
//     });
// }

// function searchBusinessResult(businessArea = '', businessService = '', freeText = '') {
//     // get Result using Ajax
//     jQuery.ajax({
//         url: DATABSE_PROCESS_URL,
//         type: 'post',
//         data: {
//             businessArea: businessArea,
//             businessService: businessService,
//             freeText: freeText,
//         },
//         dataType: "json",
//         success: function (response) {
//             console.log(response);
//             if (response.success == true) {
//                 jQuery("#job_role").html('');
//                 $.each(response.data, function (index, val) {
//                     jQuery("#job_role").append('<option value="' + index + '">' + val + '</option>');
//                 });
//             } else {

//             }
//         }
//     });
// }
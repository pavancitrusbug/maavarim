<?php
/**
 * Template Name: template-search-business
 */


get_header(); ?>

    <section class="company-section">

        <!-- ***** Search Filters ***** -->
        <div class="company-div company-div-header">
            <div class="container">
                <div class="clearfix"></div>
                <div class="business-white-forms01">
                    <div class="white-body-form-div white-body-space-form-div">
                        <div class="white-row-color-div form-white-minus-300">                        
                            <div class="form-row-title form-col-10">
                                <div class="form-title-heading">
                                    <span class="form-title">חיפוש</span>
                                </div>	
                            </div>
                            <div class="form-row form-row-20 form-col-90">
                                <div class="grid-auto grid-33">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label"> תחום העסק:</label>
                                        <div class="input-field-group">
                                            <select id="search_business_area" name="search_business_area">
                                                <option value="">בחר</option>
                                            </select>
                                            <span class="dropdown-arrow"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="grid-auto grid-33">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="business_contact_role">שירות / מוצר:</label>
                                        <div class="input-field-group">
                                            <select id="search_service_product" name="search_service_product">
                                                <option value="">בחר</option>
                                            </select>
                                            <span class="dropdown-arrow"></span>
                                        </div>
                                    </div>
                                </div>						
                                <div class="grid-auto grid-33">
                                    <div class="form-group">
                                        <label class="control-label">טקסט חופשי:</label>
                                        <div class="input-field-group">
                                            <input type="text" placeholder="...ןאכ ללמה תא סנכה" class="form-control input-md" id="search_free_text" name="search_free_text">
                                        </div>
                                    </div>
                                </div>						
                            </div>                          						
                        </div>                      
                        <div class="form-row form-row-btn-300">
                            <div class="button-abs-bl-auto">
                                <div class="btn-submit-group">
                                    <button class="btn btn-submit" id="search_business_btn" name="search_business_btn" type="button">חיפוש עסק<i class="icon-arrow-left"></i></button>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="company-div company-div-body">
            <div class="container">
                <div class="clearfix"></div>
                <div class="grid-lg-row" id="search_business_categories">

                    <!-- <div class="grid-lg-25 grid-lg-auto">
                        <div class="find-bs-card-box">
                            <div class="img-thumb-group">
                                <img src="http://faceland.co.il/wp-content/themes/oceanwp/assets/img/find-bs01.jpg" alt="find-bs" class="img-responsive img-find" />
                            </div>
                            <div class="text-content">
                                <h3>םיעוריא</h3>
                            </div>
                        </div>
                    </div> -->

                </div>
                <div class="clearfix"></div>
            </div>
        </div>

        <!-- Business lsiting -->
        <div class="company-div company-div-body company-articles-body">
            <div class="container">
                <div class="clearfix"></div>
                <div class="box-section-06 articles-section">
                    <div class="grid-lg-row" id="search_business_lsiting">  

                        <!-- <div class="grid-lg-50 grid-lg-auto">                            
                            <div class=" post-article-box">
                                <div class="img-thumb-right">
                                    <img src="http://faceland.co.il/wp-content/themes/oceanwp/assets/img/post.jpg" class="img-responsive img-post" alt="">
                                </div>
                                <div class="content-txt-left">
                                    <h2>םירייוצמ תורצוא – קימיל</h2>
                                    <h6>דועו תונתמלו דרשמל ,תיבל םירייואמ ריינ ירצומ גתומ</h6>
                                    <p><a href="tel:0528657448">0528657448</a></p>
                                    <p>םיכנעתהו עובלג תדלומ בשומ</p>
                                    <a href="#" class="link-arrow"><i class="icon-arrow-left"></i></a>
                                </div>
                            </div>                            
                        </div> -->

                    </div>
                    <div class="grid-lg-row pagination-lg-row">
                        <div class="grid-lg-100 grid-lg-auto">
                            <div class="pagination-box">
                                <ul class="pagination-ul" id="search_business_pagination">
                                    <!-- <li class="prev-li"><a href="#" class="prev-link"><i class="icon-arrow-left"></i></a></li>
                                    <li class="active"><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a href="#">4</a></li>
                                    <li class="next-li"><a href="#" class="next-link"><i class="icon-arrow-left"></i></a></li> -->
                                </ul>                                
                            </div>                            
                        </div>
                    </div>

                </div>
                <div class="clearfix"></div>
            </div>
        </div>


        <div class="company-details-body">
            <div class="container">
                <div class="clearfix"></div>
                <div class="grid-lg-row">
                    <div class="grid-lg-25 grid-lg-auto rightside-section">                    
                        <div class="img-thumb-group-top">
                            <img src="http://faceland.co.il/wp-content/themes/oceanwp/assets/img/post.jpg" class="img-responsive img-company" alt="">
                        </div>
                        <div class="content-detailsrow-div border-bottom01">
                            <h3>:קסעה לע דוע</h3>
                            <div class="btn-group-div">
                                <div class="btn-group-row">
                                    <a class="btn btn-add btn-social" >
                                        <span class="right-span"><i class="social-icon icon-s01"></i><span class="txtspan">טנרטניא רתא</span></span> 
                                        <span class="left-span"><i class="icon-arrow-left"></i></span>
                                    </a>
                                </div>
                                <div class="btn-group-row">
                                    <a class="btn btn-add btn-social" >
                                        <span class="right-span"><i class="social-icon icon-s02"></i><span class="txtspan">קובסייפ</span></span>
                                        <span class="left-span"><i class="icon-arrow-left"></i></span>
                                    </a>
                                </div>
                                <div class="btn-group-row">
                                    <a class="btn btn-add btn-social" >
                                        <span class="right-span"><i class="social-icon icon-s03"></i><span class="txtspan">םרגטסניא</span></span> 
                                        <span class="left-span"><i class="icon-arrow-left"></i></span>
                                    </a>
                                </div>
                            </div>
                        </div>                        
                        <div class="content-detailsrow-div border-bottom01">
                            <h3>!רשק רוציל םינמזומ</h3>
                            <div class="btn-group-div">
                                <div class="text-row">
                                    <p><a href="tel:052-1234567">052-1234567</a></p>
                                    <p><a href="mailto:limorbit@gmail.com">limorbit@gmail.com</a></p>
                                </div>
                                <div class="btn-group-row">
                                    <a class="btn btn-add btn-social" >
                                        <span class="right-span"><i class="social-icon icon-s04"></i><span class="txtspan">פאסטאוו</span></span> 
                                        <span class="left-span"><i class="icon-arrow-left"></i></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="content-detailsrow-div content-formrow-div">
                            <h3>:רתאה ךרד תונפל םג רשפא</h3>
                            <div class="business-white-forms01">
                                <div class="white-body-form-div ">
                                    <div class="white-row-color-div">
                                        <div class="form-row">
                                            <div class="grid-auto grid-100">
                                                <div class="form-group">
                                                    <label class="control-label"> :םש</label>
                                                    <div class="input-field-group">
                                                        <input type="text" class="form-control input-md">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="grid-auto grid-100">
                                                <div class="form-group">
                                                    <label class="control-label"> :ןופלט</label>
                                                    <div class="input-field-group">
                                                        <input type="text" class="form-control input-md">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="grid-auto grid-100">
                                                <div class="form-group">
                                                    <label class="control-label"> :ל״אוד</label>
                                                    <div class="input-field-group">
                                                        <input type="text" class="form-control input-md">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="grid-auto grid-100">
                                                <div class="form-group">
                                                    <label class="control-label"> :תורועה</label>
                                                    <div class="input-field-group">
                                                        <textarea></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>    
                                    </div>  
                                    <div class="btn-row-div">
                                        <button class="btn btn-submit" type="submit">חלש<i class="icon-arrow-left"></i></button>
                                    </div>  
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="grid-lg-75 grid-lg-auto leftside-section">
                        <div class="describe-root-txt-div">
                            <div class="row-repeat">
                                <h3>םירייוצמ תורצוא – קימיל</h3>
                                <h4>תונתמלו דרשמל ,תיבל םירייואמ ריינ ירצומ גתומ</h4>
                                <p>תדלומ בשומ</p>
                            </div>
                            <div class="row-repeat">
                                <h4>:קסעה ת/לעב</h4>
                                <p>ןוסלטיפ רומיל</p>
                            </div>
                            <div class="row-repeat">
                                <h4>:קסעה תודוא</h4>
                                <p>מה לש םיירוקמ םיבוציעו םירויאב םיוולמ קימיל לש ריינה ירצומ  ,בוציע ,רויאל התבהא תא תבלשמה ןוסלטיפ ןטיב רומיל תבצעמהו תרייא -ו הריצי diy .בל יבוש םירויאבו דפקומ בוציעב םיישומיש םירצומ ידכל </p>
                            </div>
                            <div class="row-repeat">
                                <h4>:םירצומ / םיתוריש</h4>
                                <p>ללימיק חנות מקוונת וסטודיו במושב מולדת בו ניתן למצוא פריטים מיוחדים מנייר לשדרוג פינת העבודה והבית, לחדרי הילדים, לעבודות יצירה ולמתנות מיוחדות לאנשים שאוהבים במיוחד. מה בחנות: פנקסים, מחברות, לוחות תכנון, דפי מדבקות, גלויות מאויירות, הדפסים ופוסטרים למסגור ותלייה, ניירות לעטיפה ולעבודות יצירה, מארזי מתנה מהודרים, קבצי פרינטבלס דיגיטליים להורדה ולהדפסה ביתית ועוד. </p>
                            </div>
                            <div class="row-repeat">
                                <h4>הודעות ומבצעים: </h4>
                                <p>חדש בלימיק - מבחר מארזי מתנה שווים ומפנקים לכבוד החג עם מבחרמשובח של מוצרי נייר מתוקים במחיר משתלם ומשלוח עד הבית. לפרטים נוספים והזמנות היכנסו לאתר או צרו קשר</p>
                            </div>
                        </div>
                        <div class="gallery-grid-div">
                            <div class="grid-lg-row">
                                <div class="grid-lg-33 grid-lg-auto">
                                    <div class="img-thumb">
                                        <img src="http://faceland.co.il/wp-content/themes/oceanwp/assets/img/company-gallery.jpg" alt="gallery-bs" class="img-responsive img-gallery" />
                                    </div>
                                </div>  
                                <div class="grid-lg-33 grid-lg-auto">
                                    <div class="img-thumb">
                                        <img src="http://faceland.co.il/wp-content/themes/oceanwp/assets/img/company-gallery.jpg" alt="gallery-bs" class="img-responsive img-gallery" />
                                    </div>
                                </div>  
                                <div class="grid-lg-33 grid-lg-auto">
                                    <div class="img-thumb">
                                        <img src="http://faceland.co.il/wp-content/themes/oceanwp/assets/img/company-gallery.jpg" alt="gallery-bs" class="img-responsive img-gallery" />
                                    </div>
                                </div>  
                                <div class="grid-lg-33 grid-lg-auto">
                                    <div class="img-thumb">
                                        <img src="http://faceland.co.il/wp-content/themes/oceanwp/assets/img/company-gallery.jpg" alt="gallery-bs" class="img-responsive img-gallery" />
                                    </div>
                                </div>  
                                <div class="grid-lg-33 grid-lg-auto">
                                    <div class="img-thumb">
                                        <img src="http://faceland.co.il/wp-content/themes/oceanwp/assets/img/company-gallery.jpg" alt="gallery-bs" class="img-responsive img-gallery" />
                                    </div>
                                </div>  
                                <div class="grid-lg-33 grid-lg-auto">
                                    <div class="img-thumb">
                                        <img src="http://faceland.co.il/wp-content/themes/oceanwp/assets/img/company-gallery.jpg" alt="gallery-bs" class="img-responsive img-gallery" />
                                    </div>
                                </div>    
                            </div>    
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </section>
    <script>
    jQuery(document).ready(function(){
        getBusinessCategories(jQuery('#search_business_categories'));
    });
    </script>
<?php get_footer(); ?>
<?php
/**
 * Template Name: database-process
 */

// Login peocess
include ('../../../wp-config.php');
global $wpdb;
// print_r($_POST); die;
if(empty($_POST)) {
    header("Location:".get_site_url()."?error=error");
}
// Code for registration process =======
if (array_key_exists('BR_business_registration_form', $_POST)) {
    $BR_business_id = array_key_exists('BR_registered_business', $_POST) ? $_POST['BR_registered_business'] : '';
    $business_name = array_key_exists('business_name', $_POST) ? $_POST['business_name'] : '';
    $business_service = array_key_exists('service_field', $_POST) ? $_POST['service_field'] : '';
    $business_locality = array_key_exists('business_locality', $_POST) ? $_POST['business_locality'] : '';
    $business_email = array_key_exists('business_email', $_POST) ? $_POST['business_email'] : '';
    $business_phone = array_key_exists('business_phone', $_POST) ? $_POST['business_phone'] : '';
    $business_size = array_key_exists('business_size', $_POST) ? $_POST['business_size'] : '';
    $business_password = array_key_exists('business_password', $_POST) ? $_POST['business_password'] : '';
    $business_is_received_newsletter = array_key_exists('business_is_received_newsletter', $_POST) ? 1 : 0;

    $table = $wpdb->prefix.'business_users';
    $data = array(
            'business_name'             => $business_name,
            'locality'                  => $business_locality,
            'business_service'          => $business_service,
            'email'                     => $business_email,
            'phone'                     => $business_phone,
            'size'                      => $business_size,
            'password'                  => md5($business_password), // encrypt password
            'is_received_newsletter'    => $business_is_received_newsletter,
            'created_at'                => 'CURRENT_TIMESTAMP',
            'updated_at'                => 'CURRENT_TIMESTAMP'
            );       
    $format = array('%s','%d');
    // update if business id available in post data
    if(!empty($BR_business_id)) {
        $wpdb->update($table, $data, array('id' => $BR_business_id));
        $my_id = $wpdb->rows_affected;
        if(is_integer($my_id)) {
            header("Location:".get_permalink(651)."?user=update");
        }
    } else {
        $wpdb->insert($table, $data,$format);
        $my_id = $wpdb->insert_id;
        if(is_integer($my_id)) {
            header("Location:".get_site_url()."?user=success");
        }
    } 
}

// Code for advertise business =======
if (array_key_exists('BR_advertise_business', $_POST)) {
    if(!session_id()) {
		session_start();
	}
	if(array_key_exists('login_user_detail', $_SESSION)) {
        $user = $_SESSION['login_user_detail'];
        $BR_advertise_business = array_key_exists('BR_advertise_business', $_POST) ? $_POST['BR_advertise_business'] : '';
        $business_owner_name = array_key_exists('business_owner_name', $_POST) ? $_POST['business_owner_name'] : '';
        $about_the_business = array_key_exists('about_the_business', $_POST) ? $_POST['about_the_business'] : '';
        $business_services = array_key_exists('business_services', $_POST) ? $_POST['business_services'] : '';
        $business_post_promotions = array_key_exists('business_post_promotions', $_POST) ? $_POST['business_post_promotions'] : '';
        $business_web_address = array_key_exists('business_web_address', $_POST) ? $_POST['business_web_address'] : '';
        $business_facebook_link = array_key_exists('business_facebook_link', $_POST) ? $_POST['business_facebook_link'] : '';
        $business_whatsapp_link = array_key_exists('business_whatsapp_link', $_POST) ? $_POST['business_whatsapp_link'] : '';
        $business_insta_link = array_key_exists('business_insta_link', $_POST) ? $_POST['business_insta_link'] : '';
        $business_youtube_link = array_key_exists('business_youtube_link', $_POST) ? $_POST['business_youtube_link'] : '';
        $business_email_publish = array_key_exists('business_email_publish', $_POST) ? $_POST['business_email_publish'] : '';
        $business_publish_phone = array_key_exists('business_publish_phone', $_POST) ? $_POST['business_publish_phone'] : '';
        $business_category = array_key_exists('business_category', $_POST) ? $_POST['business_category'] : [];
        $business_subcategory = array_key_exists('business_subcategory', $_POST) ? $_POST['business_subcategory'] : [];
        $business_publish_get_newsletter = array_key_exists('business_publish_get_newsletter', $_POST) ? 1 : 0;
        $table = $wpdb->prefix.'advertise_business';
        $data = array(
                'owner_name'                => $business_owner_name,
                'phone_publish'             => $business_publish_phone,   
                'services'                  => $business_services,
                'promotions'                => $business_post_promotions,
                'web_address'               => $business_web_address,
                'facebook_link'             => $business_facebook_link,
                'whatsapp_link'             => $business_whatsapp_link,
                'insta_link'                => $business_insta_link,
                'youtube_link'              => $business_youtube_link,
                'email_publish'             => $business_email_publish,
                'about'                     => $about_the_business,
                'user_id'                   => $user->id,
                'is_received_newsletter'    => $business_publish_get_newsletter
                );            
        $format = array('%s','%d');
        // update if business id available in post data
        if(!empty($BR_advertise_business)) {
            $wpdb->update($table, $data, array('id' => $BR_advertise_business));            
            $my_id = $wpdb->rows_affected;
            if($my_id) {
                // header("Location:".get_permalink(651));
            }
        } else {
            $wpdb->insert($table, $data,$format);
            $my_id = $wpdb->insert_id;
        }
        if(is_integer($my_id) || is_integer($BR_advertise_business)) {
            $_business_id = ($BR_advertise_business > 0) ? $BR_advertise_business : $my_id;
            
            $BCRtable = $wpdb->prefix.'business_category_relation';
            // add or edit the categories for business
            if (strlen($business_category[0]) > 0) {       
                // deleting all business category relation records
                $rows = $wpdb->get_results( 'DELETE FROM ' . $BCRtable . ' WHERE business_id=' . $_business_id . ' ' );
                // adding new business sub category relation records
                $business_subcategory = explode(",", $business_subcategory[0]);
                foreach($business_subcategory as $BSCkey=>$BSCval) {
                    $data = array(
                        'business_subcategory_id' => $BSCval,
                        'business_id'             => $_business_id  
                        );            
                    $format = array('%s','%d');
                    $wpdb->insert($BCRtable, $data, $format);
                    $cat_rec_id = $wpdb->insert_id;
                }
                // adding new business category relation records
                $business_category = explode(",", $business_category[0]);
                foreach($business_category as $BCkey=>$BCval) {
                    $data = array(
                        'business_category_id'    => $BCval,
                        'business_id'             => $_business_id  
                        );            
                    $format = array('%s','%d');
                    $wpdb->insert($BCRtable, $data, $format);
                    $cat_rec_id = $wpdb->insert_id;
                }
            } else {
                // deleting all business category relation records
                $rows = $wpdb->get_results( 'DELETE FROM ' . $BCRtable . ' WHERE business_id=' . $_business_id . ' ' );
            }
            // delete images from business gallery
            if (array_key_exists('delete_business_images', $_POST) && strlen($_POST['delete_business_images']) > 0) {
                if(count(explode(",", $_POST['delete_business_images'])) > 0) {
                    $table = $wpdb->prefix.'business_gallery';
                    $myrows = $wpdb->get_results( 'DELETE FROM '.$table.' WHERE business_id = ' . $BR_advertise_business . ' AND image_type LIKE "gallery" AND id IN ('.$_POST['delete_business_images'].')' );
                }
            }
            // if image upload then it will create record in db
            if (count($_FILES) > 0) {
                $record_id = !empty($BR_advertise_business) ? $BR_advertise_business : $my_id ;
                if(array_key_exists('business_gallery', $_FILES)) {
                    for($i=0; $i<sizeof($_FILES['business_gallery']['name']); $i++) {
                        if($_FILES['business_gallery']['name'][$i] != "") {
                            $location = "../../businessGallery/".$_FILES['business_gallery']['name'][$i];
                            move_uploaded_file($_FILES['business_gallery']['tmp_name'][$i], $location);
                            // insert image in table
                            $table = $wpdb->prefix.'business_gallery';
                            $data = array(
                                    'name'          => $_FILES['business_gallery']['name'][$i],
                                    'business_id'   => $record_id,
                                    'image_type'    => 'gallery',
                                    );            
                            $format = array('%s','%d');
                            $wpdb->insert($table, $data, $format);
                            $rec_id = $wpdb->insert_id;  
                        }
                    }
                }
                if(array_key_exists('business_logo', $_FILES)) {
                    // delete all logo which are exists in db    
                    if (array_key_exists('name', $_FILES['business_logo']) && !empty($_FILES['business_logo']['name'])) {              
	                    $table = $wpdb->prefix.'business_gallery';
	                    $myrows = $wpdb->get_results( 'DELETE FROM '.$table.' WHERE business_id = ' . $BR_advertise_business . ' AND image_type LIKE "logo"');
	                    
	                    if($_FILES['business_logo']['name'] != "") {
	                        $location = "../../businessGallery/".$_FILES['business_logo']['name'];
	                        move_uploaded_file($_FILES['business_logo']['tmp_name'], $location);
	                        // insert image in table                        
	                        $data = array(
	                                'name'          => $_FILES['business_logo']['name'],
	                                'business_id'   => $record_id,
	                                'image_type'    => 'logo',
	                                );            
	                        $format = array('%s','%d');
	                        $wpdb->insert($table, $data, $format);
	                        $rec_id = $wpdb->insert_id;  
	                    }
                    }
                }
            }
            echo '<script> location.href="' . get_permalink(651) . '" </script>';
        }
    }
    echo '<script> location.href="' . get_permalink(651) . '" </script>';
}

// Code for add new business contacts process =======
if (array_key_exists('BR_contact_create', $_POST)) {
    $arr = [
        'success' => false,
        'message' => 'Error while fatching data'
    ];
    $business_contact_name = array_key_exists('business_contact_name', $_POST) ? $_POST['business_contact_name'] : '';
    $business_contact_role = array_key_exists('business_contact_role', $_POST) ? $_POST['business_contact_role'] : '';
    $business_contact_phone = array_key_exists('business_contact_phone', $_POST) ? $_POST['business_contact_phone'] : '';
    $busineess_contact_email = array_key_exists('busineess_contact_email', $_POST) ? $_POST['busineess_contact_email'] : '';
    $business_contact_email_resume = array_key_exists('business_contact_email_resume', $_POST) ? $_POST['business_contact_email_resume'] : '';
    $business_id = $_POST['business_id'];

    $table = $wpdb->prefix.'business_contacts';
    $data = array(
            'name'              => $business_contact_name,            
            'business_id'       => $business_id,
            'name_role'         => $business_contact_role,
            'phone'             => $business_contact_phone,
            'email'             => $busineess_contact_email,
            'contact_email'     => $business_contact_email_resume
            );         
    $format = array('%s','%d');
    $wpdb->insert($table, $data,$format);
    $my_id = $wpdb->insert_id;    
    if(is_integer($my_id)) {
        echo json_encode([
            'success' => true,
            'message' => 'Add business contact successdully!',
            'data' => $my_id
        ]);
        die;
    }
    echo json_encode($arr);
}


// Edit business contacts =======
if (array_key_exists('BC_business_id', $_POST)) {    
    $business_contact_name = array_key_exists('business_contact_name', $_POST) ? $_POST['business_contact_name'] : '';
    $business_contact_role = array_key_exists('business_contact_role', $_POST) ? $_POST['business_contact_role'] : '';
    $business_contact_phone = array_key_exists('business_contact_phone', $_POST) ? $_POST['business_contact_phone'] : '';
    $busineess_contact_email = array_key_exists('busineess_contact_email', $_POST) ? $_POST['busineess_contact_email'] : '';
    $business_contact_email_resume = array_key_exists('business_contact_email_resume', $_POST) ? $_POST['business_contact_email_resume'] : '';
    $business_id = array_key_exists('BC_business_id', $_POST) ? $_POST['BC_business_id'] : '';
    $business_contact_id = $_POST['BC_business_contact_id'];

    $table = $wpdb->prefix.'business_contacts';
    $data = array(
            'name'              => $business_contact_name,
            'business_id'       => $business_id,
            'name_role'         => $business_contact_role,
            'phone'             => $business_contact_phone,
            'email'             => $busineess_contact_email,
            'contact_email'     => $business_contact_email_resume
            );         
     $format = array('%s','%d');
    // update if business id available in post data
    if(!empty($business_contact_id)) {
        $wpdb->update($table, $data, array('id' => $business_contact_id));            
        $my_id = $wpdb->rows_affected;
    } else {
        $wpdb->insert($table, $data, $format);
        $my_id = $wpdb->insert_id;
    }
    if(is_integer($my_id)) {
        header("Location:".get_permalink(651)."?contact=success");
    }
}

// Insert functionality for business contacts
if (array_key_exists('get_all_businesses_contacts', $_POST)) {    
    $arr = [
        'success' => false,
        'message' => 'Error while fatching contacts',
        'data'  => []
    ];
    if ($_POST['get_all_businesses_contacts'] != 'undefined') {
        // get all advertise business
        $user = $_SESSION['login_user_detail'];
        global $wpdb;
        $table = $wpdb->prefix.'business_contacts';
        $query = 'SELECT `id`,`name` FROM '.$table.' WHERE business_id ='.$_POST['get_all_businesses_contacts'];
        $result = $wpdb->get_results($query);
        if(count($result) > 0) {
            echo json_encode([
                'success' => true,
                'message' => 'got contacts',
                'data'  => $result
            ]);
            die;
        }
    }
    echo json_encode($arr);
}


// insert functionality for create new job
if (array_key_exists('BR_job_create', $_POST)) {
    // print_r($_POST); die;
    $job_business_location  = array_key_exists('job_business_location', $_POST) ? $_POST['job_business_location'] : '';
    $job_business_contact_selected = array_key_exists('job_business_contact_selected', $_POST) ? $_POST['job_business_contact_selected'] : '';
    $job_professional_field = array_key_exists('job_professional_field', $_POST) ? $_POST['job_professional_field'] : '';
    $job_role               = array_key_exists('job_role', $_POST) ? $_POST['job_role'] : '';
    $job_education          = array_key_exists('job_education', $_POST) ? $_POST['job_education'] : '';
    $job_experience         = array_key_exists('job_experience', $_POST) ? $_POST['job_experience'] : '';
    $job_scope              = array_key_exists('job_scope', $_POST) ? $_POST['job_scope'] : '';
    $job_area               = array_key_exists('job_area', $_POST) ? $_POST['job_area'] : '';
    $job_position           = array_key_exists('job_position', $_POST) ? $_POST['job_position'] : '';
    $job_title              = array_key_exists('job_title', $_POST) ? $_POST['job_title'] : '';
    $job_description        = array_key_exists('job_description', $_POST) ? $_POST['job_description'] : '';
    $job_requirement        = array_key_exists('job_requirement', $_POST) ? $_POST['job_requirement'] : '';
    $job_remarks            = array_key_exists('job_remarks', $_POST) ? $_POST['job_remarks'] : '';
    $question1              = array_key_exists('question1', $_POST) ? $_POST['question1'] : '';
    $question2              = array_key_exists('question2', $_POST) ? $_POST['question2'] : '';
    $question3              = array_key_exists('question3', $_POST) ? $_POST['question3'] : '';
    $job_business_id        = $_POST['job_business_id'];
    
    $table = $wpdb->prefix.'jobs';
    
    $data = array(
            'professional_field'    => $job_professional_field,
            'role'                  => $job_role,
            'education'             => $job_education,
            'years_of_experience'   => $job_experience,
            'scope_of_job'          => json_encode($job_scope),
            'position'              => json_encode($job_position),
            'area'                  => $job_area,
            'job_title'             => $job_title,
            'job_description'       => $job_description,
            'job_requirements'      => $job_requirement,
            'remarks'               => $job_remarks,
            'question1'             => $question1,
            'question2'             => $question2,
            'question3'             => $question3,
            'business_id'           => $job_business_id,
            'business_contact_id'   => $job_business_contact_selected
            );
    $format = array('%s','%d');
    $wpdb->insert($table, $data, $format);
    $my_id = $wpdb->insert_id;
    if(is_integer($my_id)) {
        header("Location:".get_permalink(651)."?job=success");
    }
}

// Code for login process
if(array_key_exists('login_email', $_POST) && array_key_exists('login_pass', $_POST)) {
    $arr = [
        'success' => false,
        'message' => 'Kindly check the credentials!'
    ];
    $table = $wpdb->prefix.'business_users';
    $query = 'SELECT * FROM '.$table.' WHERE email LIKE "'.$_POST['login_email'].'" AND password LIKE "'.md5($_POST['login_pass']).'" ';
    $result = $wpdb->get_results($query);
    if(count($result[0]) > 0) {
        session_start();        
        $_SESSION['login_user_detail'] = $result[0];
        echo json_encode([
            'success' => true,
            'message' => 'Login successfully'
        ]);
        die;
    }
    echo json_encode($arr);
}   

if(array_key_exists('FILE_upload_business_image', $_POST)) {
    $arr = [
        'success' => false,
        'message' => 'Error while uploading file'
    ];
    if (array_key_exists('businessLogo', $_FILES)) {
        $businessLogo = $_FILES['businessLogo'];
        $imageName = $businessLogo['name'];
        // image temp path
        $tempLocation = content_url('businessGallery/temp/').$imageName;
        $targetLocation = '../../businessGallery/temp/'.$imageName;
        // check the file is already exists on temp path
        if(!file_exists($targetLocation)) {
            move_uploaded_file($businessLogo['tmp_name'], $targetLocation);
            echo json_encode([
                'success'   => true,
                'message'   => 'File Upload Successfully',
                'data'      => $tempLocation
            ]);
            die;
        } else {
            // remove the existing file
            unlink($targetLocation);
            move_uploaded_file($businessLogo['tmp_name'], $targetLocation);
            echo json_encode([
                'success'   => true,
                'message'   => 'File Upload Successfully',
                'data'      => $tempLocation
            ]);
            die;
        }
        echo json_encode($arr);      
    }
}



// Code for check email address is exists or not in database =======
if (array_key_exists('checkEmail', $_POST)) {
    $arr = [
        'success' => false,
        'message' => 'This email address is unique'
    ];
    // check wather email address is exists or not from database
    if ($_POST['email'] != '') {
        $query = 'SELECT * FROM ' . $table_prefix.$_POST['table'] . ' WHERE ' . $_POST['field'] . ' LIKE "'. $_POST['email'] .'"';
        $result = $wpdb->get_results($query);
        if(count($result) > 0) {
            $arr = [
                'success' => true,
                'message' => 'This email is already exists'
            ];
            echo json_encode($arr);
            die;
        }
    }
    echo json_encode($arr);
}


if (array_key_exists('professional_field', $_POST)) {
    $arr = [
        'success' => false,
        'message' => 'Error: while fetching data for roles field in NEW CREATE JOB From'
    ];
    if ($_POST['professional_field'] != '') {
        $sub_professional_fields = array(
            '1' => [
                '1' => 'Sub Professional fields 01',
                '2' => 'Sub Professional fields 02',
                '2' => 'Sub Professional fields 03',
            ],
            '2' => [
                '1' => 'Sub Professional fields 001',
                '2' => 'Sub Professional fields 002',
                '2' => 'Sub Professional fields 003',
            ]
        );

        if(count($sub_professional_fields) > 0) {
            $arr = [
                'success' => true,
                'message' => 'Data for roles field',
                'data'  => $sub_professional_fields[$_POST['professional_field']]
            ];
            echo json_encode($arr);
            die;
        }
    }
    echo json_encode($arr);
}

if(array_key_exists('category', $_POST)) {
    $categories = [];
    $table = $wpdb->prefix.'business_categories';
    $query = 'SELECT * FROM '.$table.' WHERE parent_id IN (0) AND title LIKE "%'.$_POST['category'].'%" ';
    $Categoriess = $wpdb->get_results($query);
    foreach($Categoriess as $key=>$val) {
        array_push($categories, array(
            'id'    =>  $val->id,
            'text'  =>  $val->title
        ));
    }
    $arr = [];
    foreach($categories as $Akey=>$Aval) {
        array_push($arr, array(
            'id' => $Aval['id'],
            'name' => $Aval['text']
        ));
    }
    echo json_encode($arr);    
}

if(array_key_exists('subcategory', $_POST)) {    
    $sub_caregories = [];
    if ($_POST['selected_category'] > 0) {
        $catStr = implode(',', array_unique($_POST['selected_category']));    
        $table = $wpdb->prefix.'business_categories';
        $query = 'SELECT * FROM '.$table.' WHERE parent_id IN ('.$catStr.')';
        $subCategories = $wpdb->get_results($query);
        foreach($subCategories as $key=>$val) {
            array_push($sub_caregories, array(
                'id'    =>  $val->id,
                'text'  =>  $val->title
            ));
        }
        $arr = [];
        foreach($sub_caregories as $Akey=>$Aval) {
            array_push($arr, array(
                'id' => $Aval['id'],
                'name' => $Aval['text']
            ));
        }
        echo json_encode($arr);
    }
}

// when edit advertise business
if(array_key_exists('edit_business', $_POST)) {
    $arr = [
        'success' => false,
        'message' => 'Kindly check the credentials!'
    ];
    $resuls = [];
    $Cates = [];
    $subCates = [];
    $BCtable = $wpdb->prefix.'business_categories';
    $BCRtable = $wpdb->prefix.'business_category_relation';
    $query = 'SELECT BCR.*, BC.* FROM '.$BCRtable.' BCR INNER JOIN '.$BCtable.' BC ON BC.id = BCR.business_subcategory_id WHERE BCR.business_id ='.$_POST['edit_business'];
    $subCategories = $wpdb->get_results($query);

    foreach($subCategories as $key=>$val) {
        array_push($subCates, array(
            'id'    =>  $val->id,
            'text'  =>  $val->title
        ));
        array_push($Cates, $val->parent_id);
    }
    $catStr = implode(',', array_unique($Cates));
    $query = 'SELECT * FROM '.$table.' WHERE id IN ('.$catStr.')';
    $query = 'SELECT BCR.*, BC.* FROM '.$BCRtable.' BCR INNER JOIN '.$BCtable.' BC ON BC.id = BCR.business_category_id WHERE BCR.business_id ='.$_POST['edit_business'];
    $categories = $wpdb->get_results($query);
    $Cates = [];
    foreach($categories as $Ckey=>$Cval) {
        array_push($Cates, array(
            'id'    =>  $Cval->id,
            'text'  =>  $Cval->title
        ));
    }
    $resuls['categories'] = $Cates;
    $resuls['subcategories'] = $subCates;

    echo json_encode($resuls);
}

// search business process form db
// if (array_key_exists('businessArea', $_POST) && array_key_exists('businessService', $_POST)) {
//     $businessArea = array_key_exists('businessArea', $_POST) ? $_POST['businessArea'] : '';
//     $businessService = array_key_exists('businessService', $_POST) ? $_POST['businessService'] : '';
//     $freeText = array_key_exists('freeText', $_POST) ? $_POST['freeText'] : '';

//     $arr = [
//         'success' => false,
//         'message' => 'Kindly check the credentials!'
//     ];

//     // $LocalityTable = $wpdb->prefix.'master_localities';
//     // $query = 'SELECT * FROM  '.$LocalityTable.' ';
//     // $localities = $wpdb->get_results($query);

//     $resuls = [];
//     $ABtable = $wpdb->prefix.'advertise_business';
//     $query = 'SELECT title FROM '.$ABtable.' WHERE status = 1';
//     $businesses = $wpdb->get_results($query);
//     foreach($businesses as $key=>$val) {

//     }
//     echo json_encode($resuls);
// }

// get business category
if(array_key_exists('get_business_category', $_POST)) {    
    $arr = [
        'success' => false,
        'message' => 'Error: Search Result: while fetching date for business categories',
        'data'  => []
    ];
    $results = [];
    $BCtable = $wpdb->prefix.'business_categories';
    $query = 'SELECT id, title FROM '.$BCtable.' WHERE parent_id IN (0)';
    $categories = $wpdb->get_results($query);

    foreach($categories as $key=>$val) {
        array_push($results, array(
            'id' => $val->id,
            'title' => $val->title
        ));
    }
    if (!empty($results)) {
        $arr = [
            'success' => true,
            'message' => 'Success: Search Result: fetching date for business categories',
            'data'  => $results
        ];
    }
    echo json_encode($arr);
}

// get businesses by category
if(array_key_exists('get_businesses_by_category', $_POST)) {    
    $arr = [
        'success' => false,
        'message' => 'Error: Search Result: while fetching date for business listing',
        'data'  => []
    ];
    $results = [];
    $no_of_records_per_page = 2;
    $offset = ($_POST['offset']-1) * $no_of_records_per_page; 
    $end = 2;
    $BCRtable = $wpdb->prefix.'business_category_relation';
    $ABtable = $wpdb->prefix.'advertise_business';
    $BGtable = $wpdb->prefix.'business_gallery';
    $query1 = 'SELECT COUNT(*) as total_pages FROM '.$ABtable.' WHERE status = 1 ';
    $total_businesses = $wpdb->get_results($query1);

    $query = 'SELECT AB.*, BG.name as image_name FROM '.$ABtable.' AB INNER JOIN '. $BCRtable .' BCR ON BCR.business_id = AB.id INNER JOIN '. $BGtable .' BG ON BG.business_id = AB.id WHERE AB.status = 1 AND BG.image_type LIKE "logo" AND BCR.business_category_id = '.$_POST['get_businesses_by_category'].' LIMIT '.$no_of_records_per_page.' OFFSET '.$offset.' ';
    $businesses = $wpdb->get_results($query);

    foreach($businesses as $key=>$val) {
        array_push($results, array(
            'link' => get_permalink(675).'?b='.md5($val->id),
            'id' => md5($val->id),
            'owner_name' => $val->owner_name,
            'about' => $val->about,
            'services' => $val->services,
            'promotions' => $val->promotions,
            'web_address' => $val->web_address,
            'facebook_link' => $val->facebook_link,
            'insta_link' => $val->insta_link,
            'whatsapp_link' => $val->whatsapp_link,
            'youtube_link' => $val->youtube_link,
            'email_publish' => $val->email_publish,
            'phone_publish' => $val->phone_publish,
            'user_id' => $val->user_id,
            'logo' => $val->logo,
            'image_url' => content_url('businessGallery').'/'.$val->image_name
        ));
    }
    if (!empty($results)) {
        $arr = [
            'success' => true,
            'message' => 'Success: Search Result: fetching date for business listing',
            'data'  => $results,
            'total' => $total_businesses[0]->total_pages,
            'offset'      => $offset,
            'total_pages' => ceil($total_businesses[0]->total_pages / $no_of_records_per_page),
            'categoryid' => $_POST['get_businesses_by_category']
        ];
    }
    echo json_encode($arr);
}

if (array_key_exists('business_detail_contact_from_data', $_POST)) {
    $BD_remark = array_key_exists('BD_remark', $_POST) ? $_POST['BD_remark'] : '';
    $BD_email = array_key_exists('BD_email', $_POST) ? $_POST['BD_email'] : '';
    $BD_phone = array_key_exists('BD_phone', $_POST) ? $_POST['BD_phone'] : '';
    $BD_name = array_key_exists('BD_name', $_POST) ? $_POST['BD_name'] : '';
    $BD_business = array_key_exists('BD_business', $_POST) ? $_POST['BD_business'] : '';

    $BCtable = $wpdb->prefix.'business_contact_form';
    $data = array(
            'bc_remark'     => $BD_remark,
            'bc_email'      => $BD_email,
            'bc_phone'      => $BD_phone,
            'bc_name'       => $BD_name,
            'business_id'   => $BD_business
            );       
    $format = array('%s','%d');
    // update if business id available in post data
    $wpdb->insert($BCtable, $data, $format);
    $my_id = $wpdb->insert_id;
    if(is_integer($my_id)) {
        echo '
        <script>
            jQuery("#desc").html("תודה שפנית אלינו.");
            launch_toast();
            jQuery("#business_detail_contact_form").trigger("reset");
        </script>
        ';
    }
}


// get business details
// if (array_key_exists('get_business_detail', $_POST)) {
//     $business_id = $_POST['business_id'];
//     $cat_id = $_POST['cat_id'];
//     $ABtable = $wpdb->prefix.'advertise_business';
//     $query = 'SELECT AB.owner_name, AB.id FROM '.$ABtable.' WHERE AB.status = 1 ';
//     $businesses = $wpdb->get_results($query);
// }


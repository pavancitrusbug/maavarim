<?php
/**
 * The Header for our theme.
 *
 * @package OceanWP WordPress theme
 */ ?>

<!DOCTYPE html>
<html class="<?php echo esc_attr( oceanwp_html_classes() ); ?>" <?php language_attributes(); ?><?php oceanwp_schema_markup( 'html' ); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
	<style>
	#toast {
    visibility: hidden;
    max-width: 50px;
    height: 50px;
    /*margin-left: -125px;*/
    margin: auto;
    background-color: #333;
    color: #fff;
    text-align: center;
    border-radius: 2px;

    position: fixed;
    z-index: 1;
    left: 0;right:0;
    bottom: 30px;
    font-size: 17px;
    white-space: nowrap;
}
#toast #img{
	width: 50px;
	height: 50px;
    
    float: left;
    
    padding-top: 16px;
    padding-bottom: 16px;
    
    box-sizing: border-box;

    
    background-color: #111;
    color: #fff;
}
#toast #desc{

    
    color: #fff;
   
    padding: 16px;
    
    overflow: hidden;
	white-space: nowrap;
}

#toast.show {
    visibility: visible;
    -webkit-animation: fadein 0.5s, expand 0.5s 0.5s,stay 3s 1s, shrink 0.5s 2s, fadeout 0.5s 2.5s;
    animation: fadein 0.5s, expand 0.5s 0.5s,stay 3s 1s, shrink 0.5s 4s, fadeout 0.5s 4.5s;
}

@-webkit-keyframes fadein {
    from {bottom: 0; opacity: 0;} 
    to {bottom: 30px; opacity: 1;}
}

@keyframes fadein {
    from {bottom: 0; opacity: 0;}
    to {bottom: 30px; opacity: 1;}
}

@-webkit-keyframes expand {
    from {min-width: 50px} 
    to {min-width: 350px}
}

@keyframes expand {
    from {min-width: 50px}
    to {min-width: 350px}
}
@-webkit-keyframes stay {
    from {min-width: 350px} 
    to {min-width: 350px}
}

@keyframes stay {
    from {min-width: 350px}
    to {min-width: 350px}
}
@-webkit-keyframes shrink {
    from {min-width: 350px;} 
    to {min-width: 50px;}
}

@keyframes shrink {
    from {min-width: 350px;} 
    to {min-width: 50px;}
}

@-webkit-keyframes fadeout {
    from {bottom: 30px; opacity: 1;} 
    to {bottom: 60px; opacity: 0;}
}

@keyframes fadeout {
    from {bottom: 30px; opacity: 1;}
    to {bottom: 60px; opacity: 0;}
}

    .width-auto-right {
        float: right;
        width: 33.3%;
    }
    .business-white-forms01 .white-body-form-div label.control-label.business_image_add_btn {
        width: 100%;
        float: right;
    }
    .image-input-group-root .show-image-div img.img-responsive.img-gallery-add {
        padding: 5px;
        border-radius: 24px;
        height: 65px !important;
        width: 130px !important;
    }
    .business-white-forms01 .white-body-form-div .image-input-group-root .form-group .btn.btn-danger.remove-me { border: none; color: #fff; position: relative; background-color: rgba(0, 0, 0, 0.2); z-index: 999; border-radius: 100px; line-height: 18px; display: flex; align-items: center; height: 18px; width: 18px; justify-content: center; float: right; margin: 7px 0 0 0; }
    .business-white-forms01 .white-body-form-div .image-input-group-root .form-group {
        min-height: 70px;
        margin: 0 0 0px 0 !important;
    }
label.control-label:hover { cursor: pointer; }
.loadersmall { border: 3px solid #f3f3f3;-webkit-animation: spin 1s linear infinite;animation: spin 1s linear infinite;border-top: 3px solid #555;border-radius: 50%;width: 30px; height: 30px;}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}

.image-input-group-root .show-image-div {
    height: 65px !important;
    width: 130px !important;
    display: flex !important;
    align-items: center;
    justify-content: center;
    border-radius: 24px;
}
    

    @media only screen and (max-width: 767px){
        .width-auto-right { float: right; width: 50%; }
    }
    
    @media only screen and (max-width: 1024px) and (min-width: 768px){
        .width-auto-right { float: right; width: 50%; }
    }

	</style>
        <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
        <link href="<?php bloginfo('template_url'); ?>/styles.imageuploader.css" media="all" rel="stylesheet" type="text/css" />
	    <link href="<?php bloginfo('template_url'); ?>/style.css" media="all" rel="stylesheet" type="text/css" />     
        <link href="https://fonts.googleapis.com/css?family=Assistant:200,300,400,600,700,800&display=swap&subset=hebrew" rel="stylesheet">
        <script>
            var DATABSE_PROCESS_URL = '<?php bloginfo('template_url'); ?>/database-process.php';
        </script>
</head>

<body <?php body_class(); ?>>
	<?php do_action( 'ocean_before_outer_wrap' ); ?>

	<div id="outer-wrap" class="site clr">

		<?php do_action( 'ocean_before_wrap' ); ?>

		<div id="wrap" class="clr">

			<?php do_action( 'ocean_top_bar' ); ?>

			<?php do_action( 'ocean_header' ); ?>

			<?php do_action( 'ocean_before_main' ); ?>
			
			<main id="main" class="site-main clr"<?php oceanwp_schema_markup( 'main' ); ?>>

				<?php do_action( 'ocean_page_header' ); ?>
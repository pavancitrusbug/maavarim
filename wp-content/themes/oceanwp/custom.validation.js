// Validation for all forms

jQuery(document).ready(function () {
    // Validations for New Create Job fields
    jQuery("form[name='add_new_job']").validate({
        ignore: [],
        // Specify validation rules
        rules: {
            job_professional_field: "required",
            job_role: "required",
            job_education: "required",
            job_experience: "required",
            'job_scope[]': "required",
            'job_position[]': "required",
            job_title: "required",
            job_description: "required",
            job_requirement: "required",
            job_remarks: "required",
            question1: "required",
            question2: "required",
            question3: "required",
            job_start_date: {
                required: true
            },
            job_end_date: {
                required: true
            },
            job_business_location: "required",
            job_business_contact_selected: "required",
        },
        // Specify validation error messages
        messages: {
            job_professional_field: "Please enter professional field",
            job_role: "Please select job role",
            job_education: "Please select job education",
            job_experience: "Please select job experience",
            'job_scope[]': "Please select job scope",
            'job_position[]': "Please select job position",
            job_title: "Please enter job title",
            job_description: "Please enter job description",
            job_requirement: "Please enter job requirement",
            job_remarks: "Please enter job remarks",
            question1: "Please enter question 1",
            question2: "Please enter question 2",
            question3: "Please enter question 3",
            job_start_date: {
                required: "Please select start date"
            },
            job_end_date: {
                required: "Please select end date"
            },
            job_business_location: "Please select job location",
            job_business_contact_selected: "Please select business contact",
        },
        errorPlacement: function (error, element) {
            var placement = jQuery(element).data('error');
            if (element.attr('id') == 'job_business_location') {
                jQuery('#job_business_location-error').remove();
                jQuery('.job_business_location001').after(error);
            } else if (element.attr('id') == 'job_business_contact_selected') {
                jQuery('#job_business_contact_selected-error').remove();
                jQuery('.business_detail_contacts_person_error').append(error);
            } else if (element.attr('id') == 'job_scope_partial') {
                jQuery('.checkbox03').append(error);
            } else if (element.attr('id') == 'job_position_favorite') {
                jQuery('.checkbox01').append(error);
            } else if (placement) {
                jQuery(placement).append(error);
            } else {
                error.insertAfter(element);
            }
        },
        // Make sure the form is submitted to the destination defined
        // in the "action" attribute of the form when valid
        submitHandler: function (form) {
            form.submit();
        }
    });

    // Validations for New Create Job fields
    jQuery('form[name="advertise_business"]').validate({
        ignore: [],
        // Specify validation rules
        rules: {
            business_owner_name: "required",
            about_the_business: "required",
            business_services: "required",
            business_post_promotions: "required",
            business_web_address: "required",
            business_facebook_link: "required",
            business_insta_link: "required",
            business_email_publish: "required",
            business_publish_phone: "required",
            question2: "required",
            question3: "required",
            job_business_location: "required",
            // job_business_contact_selected: "required",
        },
        // Specify validation error messages
        messages: {
            business_owner_name: "Please enter business owner name",
            about_the_business: "Please enter about business",
            business_services: "Please enter business services",
            business_post_promotions: "Please enter about business posts and promotions",
            business_web_address: "Please enter business web address",
            business_facebook_link: "Please enter business facebook link",
            business_insta_link: "Please enter business instagram link",
            business_email_publish: "Please enter email address",
            business_publish_phone: "Please enter phone number",
            question2: "Please enter question 2",
            question3: "Please enter question 3",
            job_business_location: "Please select job location",
            // job_business_contact_selected: "Please select business contact",
        },
        // Make sure the form is submitted to the destination defined
        // in the "action" attribute of the form when valid
        submitHandler: function (form) {
            form.submit();
        }
    });

    // Validations for New Create Job fields
    jQuery("form[name='business_registration']").validate({
        // Specify validation rules
        rules: {
            business_name: "required",
            service_field: "required",
            business_locality: "required",
            business_phone: {
                required: true,
                number: true
            },
            business_email: "required",
            business_size: "required",
            business_password: "required"
        },
        // Specify validation error messages
        messages: {
            business_name: "Please enter business name",
            service_field: "Please enter service field",
            business_locality: "Please select locality",
            business_phone: {
                required: "Please enter business phone",
                number: "Please enter only digits"
            },
            business_email: "Please enter business email",
            business_size: "Please select business size",
            business_password: "Please enter password"
        },
        // Make sure the form is submitted to the destination defined
        // in the "action" attribute of the form when valid
        submitHandler: function (form) {
            form.submit();
        }
    });

    // Validations for New Create Job fields
    jQuery("form[name='business_contact0']").validate({
        // Specify validation rules
        rules: {
            business_contact_name: "required",
            business_contact_role: "required",
            business_contact_phone: {
                required: true,
                number: true
            },
            business_contact_email_resume: "required",
            busineess_contact_email: "required"
        },
        // Specify validation error messages
        messages: {
            business_contact_name: "Please enter name",
            business_contact_role: "Please enter role",
            business_contact_phone: {
                required: "Please enter phone no.",
                number: "Please enter only digits"
            },
            business_contact_email_resume: "Please enter email for resume",
            busineess_contact_email: "Please enter email address"
        },
        // Make sure the form is submitted to the destination defined
        // in the "action" attribute of the form when valid
        submitHandler: function (form) {
            form.submit();
        }
    });

    jQuery("#business_detail_contact_form").validate({
        // Specify validation rules
        rules: {
            BD_name: "required",
            BD_email: "required",
            BD_phone: {
                number: true
            }
        },
        // Specify validation error messages
        messages: {
            BD_name: "Please enter name",
            BD_email: "Please enter email address",
            BD_phone: {
                number: "Please enter only digits"
            }
        },
        // Make sure the form is submitted to the destination defined
        // in the "action" attribute of the form when valid
        submitHandler: function (form) {
            jQuery('form .submit_business_detail_form').trigger('click');
        }
    });


});
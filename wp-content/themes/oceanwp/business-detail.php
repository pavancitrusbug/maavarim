<?php
/**
 * Template Name: business-detail
 */

get_header(); 
if(!session_id()) {
	session_start();
}
if (array_key_exists('login_user_detail', $_SESSION)) {

$localities = array(
    '1' => 'locality 001',
    '2' => 'locality 002',
    '3' => 'locality 003',
    '4' => 'locality 004',
);
$roles = array(
    '1' => 'Project Manager',
    '2' => 'HR'
);
$business_sizes = array(
    '1' => 'עסק זעיר - עד 5 עובדים',
    '2' => 'עסק קטן - 6-50 עובדים',
    '3' => 'עסק בינוני - 51-100 עובדים',
    '4' => 'עסק גדול - מעל 100 עובדים',
);
?>
<section class="business-profile-section">
    <div class="business-profile-div">
        <div class="container">
            <h1>פרופיל עסק</h1>
            <div class="clearfix"></div>
            <div class="profilebox-row-root">
                <div class="clearfix"></div>
                <?php
                    //get business User details
                    $user = $_SESSION['login_user_detail'];
                    global $wpdb;
                    $user_table = $wpdb->prefix.'business_users';
                    $query = 'SELECT * FROM '.$user_table.' WHERE id = '.$user->id;
                    $regitered_business = $wpdb->get_results($query);
                    if(count($regitered_business) > 0) {
                        $regitered_business = $regitered_business[0];
                ?>
                <div class="profilebox-div profilebox-div01">
                    <div class="profilebox-header-div">
                        <div class="left-side-header">
                            <a href="<?php echo get_permalink(505).'?bid='.$regitered_business->id; ?>" class="link-btn">עריכת פרטי העסק <i class="icon-arrow-left"></i></a>
                            <a href="<?php echo get_permalink(505).'?bid='.$regitered_business->id; ?>" class="icon-btn"><i class="icon-edit"></i></a>
                        </div>
                        <div class="right-side-header">
                            <h3><?php echo $regitered_business->business_name; ?></h3>
                        </div>
                    </div>                    
                    <div class="profilebox-body-div">
                        <div class="row-div mb-10">
                            <h5><?php echo $regitered_business->business_service; ?></h5>
                            <?php
                            $locality = '--';
                            foreach($localities as $Lkey=>$Lval) {
                                if ($Lkey == $regitered_business->locality) {
                                    $locality = $Lval;                                        
                                }
                            }
                            ?>
                            <p><?php echo $locality; ?></p>
                        </div>
                        <div class="row-div">
                            <p class="mb-5"><a href="mailto:<?php echo $regitered_business->email; ?>"><?php echo $regitered_business->email; ?></a></p>
                            <?php
                            $business_size = '--';
                            foreach($business_sizes as $Skey=>$sval) {
                                if ($Skey == $regitered_business->size) {
                                    $business_size = $Lval;                                        
                                }
                            }
                            ?>
                            <p><?php echo $business_size; ?></p>
                        </div>    
                    </div>
                </div>
                <?php 
                    }
                ?>
                <div class="clearfix"></div>
                <div class="gridbox-two-div">
                    <div class="box-section-02 color-box-01">
                        <div class="inner_business_box_row">
                            <div class="width50_box">
                                <div class="two_section_left">
                                    <div class="top-section-box">
                                        <h3> <span class="span-block">פרסום העסק </span><span class="span-block">בלוח עסקים בעמקים</span> </h3>
                                    </div>
                                    <?php 
                                        $pageLink = get_permalink(505);
                                        $user = $_SESSION['login_user_detail'];
                                        global $wpdb;
                                        $table = $wpdb->prefix.'advertise_business';
                                        $query = 'SELECT * FROM '.$table.' WHERE user_id = "'.$user->id.'"';
                                        $result = $wpdb->get_results($query);
                                        if(count($result) > 0) {
                                            $pageLink = get_permalink(651);
                                        }
                                    ?>
                                    <div class="two_section_buttons">
                                        <a href="<?php echo $pageLink; ?>"><i class="icon-arrow-left"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="width50_box">
                                <div class="two_section_left change-color2">
                                    <div class="top-section-box">
                                        <h3> <span class="span-block"> פרסום משרות </span> <span class="span-block">בלוח הדרושים</span> </h3>
                                    </div>	
                                    <div class="two_section_buttons">
                                        <a href="<?php echo get_permalink(654); ?>"><i class="icon-arrow-left"></i></a>
                                    </div>
                                </div>	
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                
                <?php
                    //get business contacts listing
                    $user = $_SESSION['login_user_detail'];
                    global $wpdb;
                    $user_table = $wpdb->prefix.'business_users';
                    $contact_table = $wpdb->prefix.'business_contacts';
                    $advertise_business_table = $wpdb->prefix.'advertise_business';
                    $query = 'SELECT BC.* FROM '.$contact_table.' BC INNER JOIN '.$advertise_business_table.' AB ON AB.id = BC.business_id INNER JOIN '.$user_table.' BU ON BU.id = AB.user_id WHERE BU.id = '.$user->id.' ORDER BY id DESC';
                    $contacts = $wpdb->get_results($query);
                    if(count($contacts) > 0) {
                ?>
                <div class="profilebox-div profilebox-div02">
                    <div class="profilebox-header-div">
                        <div class="left-side-header">
                            <a href="<?php echo get_permalink(661); ?>" class="link-btn">הוספת איש קשר <i class="icon-arrow-left"></i></a>
                            <a href="<?php echo get_permalink(661); ?>" class="icon-btn"><i class="edit-plus-icon"></i></a>
                        </div>
                        <div class="right-side-header">
                            <h3 class="ml-40">אנשי קשר</h3>
                            <p class="font-400">(לצורך פרסום משרות בלוח הדרושים)</p>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="profilebox-body-div profilebox-body-div02">
                    <?php
                        foreach($contacts as $contact) {
                    ?>
                        <div class="row-div-data">
                            <div class="right-side-col">
                                <h3><?php echo $contact->name; ?></h3>                                
                                <?php 
                                    $role = '--';
                                    foreach($roles as $Rkey=>$Rval) {
                                        if ($Rkey == $contact->name_role) {
                                            $role = $Rval;                                                
                                        }
                                    }                                   
                                    $contact_details = $contact->phone. ' | ' . $role . ' | ' . $contact->email. ' | ' . $contact->contact_email;
                                    echo '<p>'.$contact_details.'</p>';
                                ?>
                            </div>
                            <div class="left-side-col">
                                <a href="<?php echo get_permalink(661).'?bcid='.$contact->id; ?>" class="link-btn link-icon-btn">עריכת פרטי קשר <i class="icon-arrow-left"></i></a>
                            </div>
                        </div>
                    <?php
                            } // end of foreach loop
                    ?>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
                <?php 
                    }
                    //get business contacts
                    $user = $_SESSION['login_user_detail'];
                    global $wpdb;
                    $jobs_table = $wpdb->prefix.'jobs';
                    $user_table = $wpdb->prefix.'business_users';
                    $advertise_business_table = $wpdb->prefix.'advertise_business';
                    $query = 'SELECT J.id as job_id, J.job_title as job_title, BC.name as contact_name FROM '.$user_table.' BR INNER JOIN '.$advertise_business_table.' AB ON AB.user_id = BR.id INNER JOIN '.$jobs_table.' J ON J.business_id = AB.id  INNER JOIN '.$contact_table.' BC ON BC.id = J.business_contact_id WHERE BR.id ='.$user->id;
                    
                    $jobs = $wpdb->get_results($query);
                    if(count($jobs) > 0) {
                ?>
                <div class="profilebox-div profilebox-div02 profilebox-div03">
                    <div class="profilebox-header-div">
                        <div class="left-side-header">
                            <a href="<?php echo get_permalink(654); ?>" class="link-btn">הוספת משרה חדשה<i class="icon-arrow-left"></i></a>
                            <a href="<?php echo get_permalink(654); ?>" class="icon-btn"><i class="edit-plus-icon"></i></a>
                        </div>
                        <div class="right-side-header">
                            <h3 class="ml-40">המשרות שלי</h3>
                        </div>
                    </div>
                    <div class="clearfix"></div>

                    <div class="profilebox-body-div profilebox-body-div02">
                        <p class="notes-p">* לידיעתך, אפשרות הקפצה של משרה פעילה תופיע רק כעבור שבועיים מיום הפרסום.</p>
                        <div class="table-responsive table-custom-root">
                            <table class="table-custom">
                                <tr>
                                    <th class="width-10">מס' משרה</th>
                                    <th class="width-10">תחילת פרסום</th>
                                    <th class="width-10">סיום פרסום</th>
                                    <th class="width-10">איש קשר</th>
                                    <th class="width-25">כותרת המשרה</th>
                                    <th class="width-10">סטטוס משרה</th>
                                    <th class="width-20">פעולות</th>
                                </tr>
                                <?php
                                    foreach($jobs as $job) {
                                ?>
                                    <tr>
                                        <td><?php echo $job->job_id; ?></td>
                                        <td>1.1.19</td>
                                        <td>31.1.19</td>
                                        <td><?php echo $job->contact_name; ?></td>
                                        <td><?php echo $job->job_title; ?></td>
                                        <td>פעילה באתר</td>
                                        <td>
                                            <div class="last-action-col">
                                                <a href="#" class="link-btn link-icon-btn">הסרת משרה <i class="icon-arrow-left"></i></a>
                                                <a href="#" class="link-btn link-icon-btn bouncing-icon-btn">הקפצת משרה <i class="icon-arrow-left"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                <?php
                                    } // end of foreach loop
                                ?>
                            </table>
                        </div>                          
                    </div>
                    
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
                <?php 
                    }
                    //get business contacts
                    $user = $_SESSION['login_user_detail'];
                    global $wpdb;
                    $advertise_table = $wpdb->prefix.'advertise_business';
                    $gallery_table = $wpdb->prefix.'business_gallery';
                    $user_table = $wpdb->prefix.'business_users';
                    $query = 'SELECT AB.*, BG.name as image_name FROM '.$user_table.' BR INNER JOIN '.$advertise_table.' AB ON AB.user_id = BR.id INNER JOIN '.$gallery_table.' BG ON BG.business_id = AB.id WHERE BR.id = '.$user->id.' AND BG.image_type LIKE "logo" ';
                    $details = $wpdb->get_results($query);
                    if(count($details) > 0) {
                        $business_details = $details[0];
                ?>
                <div class="profilebox-div profilebox-div04">
                    <div class="profilebox-header-div">
                        <div class="left-side-header">
                            <a href="<?php echo get_permalink(542).'?b='.$business_details->id; ?>" class="link-btn">עריכת פרטי העסק <i class="icon-arrow-left"></i></a>
                            <a href="<?php echo get_permalink(542).'?b='.$business_details->id; ?>" class="icon-btn"><i class="icon-edit"></i></a>
                        </div>
                        <div class="right-side-header">
                            <h3>פרטי העסק בלוח עסקים בעמקים</h3>
                        </div>
                    </div>                    
                    <div class="profilebox-body-div">
                        <div class="row-details-div pt-30">
                            <div class="thumb-img-col-div">
                                <div class="image-show">
                                <?php
                                $business_logo  = 'http://faceland.co.il/wp-content/uploads/2019/08/Asset-20.png';
                                if ($business_details->image_name != '') {
                                    $business_logo = content_url('businessGallery/').$business_details->image_name;
                                }
                                ?>
                                    <img src="<?php echo $business_logo; ?>" class="img-responsive" alt="Business Logo" />
                                </div>
                            </div>
                            <div class="details-col-div">
                                <div class="row-div mb-15">
                                    <h5>שם בעל/ת העסק</h5>
                                    <p><?php echo $business_details->owner_name; ?></p>
                                </div>
                                <div class="row-div mb-15">
                                    <h5>אודות העסק </h5>
                                    <p><?php echo $business_details->about; ?></p>
                                </div>
                                <div class="row-div mb-15">
                                    <h5>שירותים / מוצרים </h5>
                                    <p><?php echo $business_details->services; ?></p>
                                </div>
                                <div class="row-div mb-15">
                                    <h5>הודעות ומבצעים </h5>
                                    <p><?php echo $business_details->promotions; ?></p>
                                </div>
                            </div>
                        </div>
                        <div class="row-details-div">
                            <div class="category-row-div">
                                <div class="category-col-div">
                                    <div class="row-div mb-15">
                                        <h5>קטגוריה 1</h5>
                                        <p>קטגוריה </p>
                                        <p>תת קטגוריה</p>
                                    </div>
                                </div>
                                <div class="category-col-div">
                                    <div class="row-div mb-15">
                                        <h5>קטגוריה 2</h5>
                                        <p>קטגוריה </p>
                                        <p>תת קטגוריה</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row-details-div">
                            <div class="more-business-row-div">
                                <div class="row-div">
                                    <h5>עוד על העסק</h5>
                                    <p>כתובת אתר אינטרנט: <a href="<?php echo $business_details->web_address; ?>"><?php echo $business_details->web_address; ?></a></p>
                                    <p>קישור לפייסבוק: <a href="<?php echo $business_details->facebook_link; ?>"><?php echo $business_details->facebook_link; ?></a></p>
                                    <p>קישור לאינסטגרם: <a href="<?php echo $business_details->insta_link; ?>"><?php echo $business_details->insta_link; ?></a></p>
                                    <p>מה קישור היישום -: <a href="<?php echo $business_details->whatsapp_link; ?>"><?php echo $business_details->whatsapp_link; ?></a></p>
                                    <p>קישור ליו-טיוב: <a href="<?php echo $business_details->youtube_link; ?>"><?php echo $business_details->youtube_link; ?></a></p>
                                </div>
                            </div>
                        </div>
                        <div class="row-details-div">
                            <div class="more-business-row-div">
                                <div class="row-div">
                                    <h5>פרטים ליצירת קשר</h5>
                                    <p>טלפון: <?php echo $business_details->phone_publish; ?> </p>
                                    <p>דואר אלקטרוני: <?php echo $business_details->email_publish; ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php 
                    } 
                ?>                
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</section>

<?php } else {  ob_start();
    echo '<script> location.href="'.get_site_url().'" </script>';
} get_footer(); ?>
<?php
/**
 * The template for displaying the footer.
 *
 * @package OceanWP WordPress theme
 */ ?>

        </main><!-- #main -->

        <?php do_action( 'ocean_after_main' ); ?>

        <?php do_action( 'ocean_before_footer' ); ?>

        <?php
        // Elementor `footer` location
        if ( ! function_exists( 'elementor_theme_do_location' ) || ! elementor_theme_do_location( 'footer' ) ) { ?>

            <?php do_action( 'ocean_footer' ); ?>
            
        <?php } ?>

        <?php do_action( 'ocean_after_footer' ); ?>
                
    </div><!-- #wrap -->

    <?php do_action( 'ocean_after_wrap' ); ?>

</div><!-- #outer-wrap -->
<div id="toast"><div id="desc"></div></div>
<?php do_action( 'ocean_after_outer_wrap' ); ?>

<?php
// If is not sticky footer
if ( ! class_exists( 'Ocean_Sticky_Footer' ) ) {
    get_template_part( 'partials/scroll-top' );
} ?>

<?php
// Search overlay style
if ( 'overlay' == oceanwp_menu_search_style() ) {
    get_template_part( 'partials/header/search-overlay' );
} ?>

<?php
// If sidebar mobile menu style
if ( 'sidebar' == oceanwp_mobile_menu_style() ) {
    
    // Mobile panel close button
    if ( get_theme_mod( 'ocean_mobile_menu_close_btn', true ) ) {
        get_template_part( 'partials/mobile/mobile-sidr-close' );
    } ?>

    <?php
    // Mobile Menu (if defined)
    get_template_part( 'partials/mobile/mobile-nav' ); ?>

    <?php
    // Mobile search form
    if ( get_theme_mod( 'ocean_mobile_menu_search', true ) ) {
        get_template_part( 'partials/mobile/mobile-search' );
    }

} ?>

<?php
// If full screen mobile menu style
if ( 'fullscreen' == oceanwp_mobile_menu_style() ) {
    get_template_part( 'partials/mobile/mobile-fullscreen' );
} ?>


<?php wp_footer(); ?>
 
<script src="<?php bloginfo('template_url'); ?>/assets/js/advance.js"> </script>


<?php 
/** When successfully register user toast will be appear on home page */
    if (array_key_exists('user', $_REQUEST) && $_REQUEST['user'] == 'success') {
        echo '<script>jQuery("#desc").html("תודה על הרשמתך לאתר מעברים בעמק!"); launch_toast();</script>';
    }
/** When successfully register user toast will be appear on home page */
    if (array_key_exists('user', $_REQUEST) && $_REQUEST['user'] == 'update') {
        echo '<script>jQuery("#desc").html("Successfully Update Record!"); launch_toast();</script>';
    }
/** When advertise business is not exists */
    if (array_key_exists('need', $_REQUEST) && $_REQUEST['need'] == 'business') {
        echo '<script>jQuery("#desc").html("You need to advertise business first!"); launch_toast();</script>';
    }
/** When erro comens while edit or insert */
    if (array_key_exists('error', $_REQUEST) && $_REQUEST['error'] == 'error') {
        echo '<script>jQuery("#desc").html("Error while performing action!"); launch_toast();</script>';
    }
/** When successfully add business contact toast will be appear */
    // if (array_key_exists('user', $_REQUEST) && $_REQUEST['user'] == 'success') {
    //     echo '<script>jQuery("#desc").html("Successfully Register"); launch_toast();</script>';
    // }
?>
</body>
</html>
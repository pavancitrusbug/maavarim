<?php
/**
 * Template Name: template-business-detail
 */


get_header(); 
// Function to check string starting 
// with given substring 
function startsWith ($string, $startString) 
{ 
    $len = strlen($startString); 
    return (substr($string, 0, $len) === $startString); 
} 
$businessArry = [];
if(!array_key_exists('b', $_GET)) {
    echo '<script> location.href="' . get_site_url() . '" </script>';
    die;
} else {
    //get business contacts
    $business_ecryp_id = $_GET['b'];
    global $wpdb;
    $advertise_business_table = $wpdb->prefix.'advertise_business';
    $query1 = 'SELECT id FROM '.$advertise_business_table.' WHERE business_ecryp_id LIKE "'.$business_ecryp_id.'" ';
    $business = $wpdb->get_results($query1);
    if (count($business) > 0) {
        $businessID = $business[0]->id;
		$user_table = $wpdb->prefix.'business_users';
        $business_gallery = $wpdb->prefix.'business_gallery';
        $business_users = $wpdb->prefix.'business_users';
        $query = 'SELECT BU.business_name as BUname, BU.business_service as BUservices, BU.locality as BUlocality, AB.*, BG.name as image_name, BG.image_type as image_type, BG.id as image_id FROM ' . $advertise_business_table . ' AB INNER JOIN ' . $user_table . ' BU ON BU.id = AB.user_id INNER JOIN '.$business_gallery.' BG ON AB.id = BG.business_id WHERE AB.id = '.$businessID.' ';
        $businessDetail = $wpdb->get_results($query);        
        foreach($businessDetail as $business) {
            $businessArry['business_name'] = $business->BUname;
            $businessArry['business_service'] = $business->BUservices;
            $businessArry['business_locality'] = $business->BUlocality;
            $businessArry['bid'] = $business->id;
            $businessArry['owner_name'] = $business->owner_name;
            $businessArry['about'] = $business->about;
            $businessArry['services'] = $business->services;
            $businessArry['promotions'] = $business->promotions;
            $businessArry['web_address'] = (startsWith($business->web_address, 'http')) ? $business->web_address : 'http://'.$business->web_address;
            $businessArry['facebook_link'] = (startsWith($business->facebook_link, 'http')) ? $business->facebook_link : 'http://'.$business->facebook_link;
            $businessArry['insta_link'] = (startsWith($business->insta_link, 'http')) ? $business->insta_link : 'http://'.$business->insta_link;
            $businessArry['whatsapp_link'] = (startsWith($business->whatsapp_link, 'http')) ? $business->whatsapp_link : 'http://'.$business->whatsapp_link;
            $businessArry['youtube_link'] = (startsWith($business->youtube_link, 'http')) ? $business->youtube_link : 'http://'.$business->youtube_link;
            $businessArry['email_publish'] = $business->email_publish;
            $businessArry['phone_publish'] = $business->phone_publish;
            $businessArry['images'][$business->image_type][$business->image_id] = $business->image_name;                                    
        }
    } else {
        echo '<script> location.href="' . get_site_url() . '" </script>';
        die;
    }
}
?>
<script src="https://jqueryvalidation.org/files/lib/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/custom.validation.js" ></script>
    <section class="company-section">

        <div class="company-div company-div-header">
            <div class="container">
                <div class="clearfix"></div>
                <div class="business-white-forms01 elementor-element elementor-element-00da0ce elementor-widget elementor-widget-shortcode" data-id="00da0ce" data-element_type="widget" data-widget_type="shortcode.default">
                        <div class="elementor-widget-container">
                            <div class="elementor-shortcode"><div class="inner_banner mainbanner-box-section">
                                <div class="header_section_seperate_menu">
                                    <div class="section_top">
                                        <i class="icon-campus"></i>
                                    </div>
                                    <div class="section_botton">
                                        <p>קמפוס מעברים</p>
                                        <i class="icon-arrow-left"></i>
                                    </div>
                                </div>
                                <div class="header_section_banner">
                                    <?php $img = get_the_post_thumbnail_url(675); ?>
                                    <img src="<?php echo $img; ?>" class="header_section_image" alt="" title="">		
                                    <div class="banner-indextop">
                                        <h2><?php echo $businessArry['business_name']; ?></h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- ***** Search Filters ***** -->
        <?php echo do_shortcode('[business_search_bar]'); ?>

        <div class="company-details-body">
            <div class="container">
                <div class="clearfix"></div>
                <div class="grid-lg-row">
                    <div class="grid-lg-25 grid-lg-auto rightside-section">                    
                        <div class="img-thumb-group-top">
                            <?php
                                $business_logo  = 'http://faceland.co.il/wp-content/uploads/2019/08/Asset-20.png';
                                if (array_key_exists('logo', $businessArry['images'])) {
                                    foreach($businessArry['images']['logo'] as $key=>$img) {
                                        $business_logo = content_url('businessGallery/').$img;
                                    }
                                }
                            ?>
                            <img src="<?php echo $business_logo; ?>" class="img-responsive img-company" alt="">
                        </div>
                        <div class="content-detailsrow-div border-bottom01">
                            <h3>עוד על העסק:</h3>
                            <div class="btn-group-div">
                                <?php if (!empty($businessArry['web_address'])) { ?>
                                <div class="btn-group-row">
                                    <a class="btn btn-add btn-social" target="_blank" href="<?php echo $businessArry['web_address'] ?>" >
                                        <span class="right-span"><i class="social-icon icon-s01"></i><span class="txtspan">אתר אינטרנט</span></span> 
                                        <span class="left-span"><i class="icon-arrow-left"></i></span>
                                    </a>
                                </div>
                                <?php } if (!empty($businessArry['facebook_link'])) { ?>
                                <div class="btn-group-row">
                                    <a class="btn btn-add btn-social" target="_blank" href="<?php echo $businessArry['facebook_link'] ?>" >
                                        <span class="right-span"><i class="social-icon icon-s02"></i><span class="txtspan">פייסבוק</span></span>
                                        <span class="left-span"><i class="icon-arrow-left"></i></span>
                                    </a>
                                </div>
                                <?php } if (!empty($businessArry['insta_link'])) { ?>
                                <div class="btn-group-row">
                                    <a class="btn btn-add btn-social" target="_blank" href="<?php echo $businessArry['insta_link'] ?>" >
                                        <span class="right-span"><i class="social-icon icon-s03"></i><span class="txtspan">אינסטגרם</span></span> 
                                        <span class="left-span"><i class="icon-arrow-left"></i></span>
                                    </a>
                                </div>
                                <?php } if (!empty($businessArry['youtube_link'])) { ?>
                                <div class="btn-group-row">
                                    <a class="btn btn-add btn-social" target="_blank" href="<?php echo $businessArry['youtube_link'] ?>" >
                                        <span class="right-span"><i class="social-icon icon-s04"></i><span class="txtspan">YouTube</span></span> 
                                        <span class="left-span"><i class="icon-arrow-left"></i></span>
                                    </a>
                                </div>
                                <?php } ?>
                            </div>
                        </div>                        
                        <div class="content-detailsrow-div border-bottom01">
                            <h3>מוזמנים ליצור קשר!</h3>
                            <div class="btn-group-div">
                                <div class="text-row">
                                    <p><a href="tel:<?php echo $businessArry['phone_publish'] ?>"><?php echo $businessArry['phone_publish'] ?></a></p>
                                    <p><a href="mailto:<?php echo $businessArry['email_publish'] ?>"><?php echo $businessArry['email_publish'] ?></a></p>
                                </div>
                                <?php if (!empty($businessArry['whatsapp_link'])) { ?>
                                <div class="btn-group-row">
                                    <a class="btn btn-add btn-social" target="_blank" href="<?php echo $businessArry['whatsapp_link'] ?>" >
                                        <span class="right-span"><i class="social-icon icon-s04"></i><span class="txtspan">וואטסאפ</span></span> 
                                        <span class="left-span"><i class="icon-arrow-left"></i></span>
                                    </a>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="content-detailsrow-div content-formrow-div">
                            <h3>אפשר גם לפנות דרך האתר:</h3>
                            <div class="business-white-forms01">                                
                                <div class="white-body-form-div ">

                                    <form id="business_detail_contact_form" name="business_detail_contact_form">
                                        <input type="hidden" value="<?php echo $businessArry['bid']; ?>" name="BD_business" />
                                        <div class="white-row-color-div">
                                            <div class="form-row">
                                                <div class="grid-auto grid-100">
                                                    <div class="form-group">
                                                        <label class="control-label"> שם: <span class="required">*</span></label>
                                                        <div class="input-field-group">
                                                            <input type="text" class="required form-control input-md" name="BD_name">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="grid-auto grid-100">
                                                    <div class="form-group">
                                                        <label class="control-label"> טלפון:</label>
                                                        <div class="input-field-group">
                                                            <input type="text" class="form-control input-md" name="BD_phone">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="grid-auto grid-100">
                                                    <div class="form-group">
                                                        <label class="control-label"> אימייל: <span class="required">*</span></label>
                                                        <div class="input-field-group">
                                                            <input type="email" class="required form-control input-md" name="BD_email">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="grid-auto grid-100">
                                                    <div class="form-group">
                                                        <label class="control-label"> הערות:</label>
                                                        <div class="input-field-group">
                                                            <textarea name="BD_remark"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>  
                                        <div class="btn-row-div">
                                            <button class="btn btn-submit submit_business_detail_form" type="button">שלח<i class="icon-arrow-left"></i></button>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="grid-lg-75 grid-lg-auto leftside-section">
                        <div class="describe-root-txt-div">
                            <div class="row-repeat">
                                <h3><?php echo $businessArry['business_name']; ?></h3>
                                <h4><?php echo $businessArry['business_service']; ?></h4>
                                <p><?php echo $businessArry['business_locality']; ?></p>
                            </div>
                            <div class="row-repeat">
                                <h4>:קסעה ת/לעב</h4>
                                <p><?php echo $businessArry['owner_name']; ?></p>
                            </div>
                            <div class="row-repeat">
                                <h4>:קסעה תודוא</h4>
                                <p><?php echo $businessArry['about']; ?></p>
                            </div>
                            <div class="row-repeat">
                                <h4>:םירצומ / םיתוריש</h4>
                                <p><?php echo $businessArry['services']; ?></p>
                            </div>
                            <div class="row-repeat">
                                <h4>הודעות ומבצעים: </h4>
                                <p><?php echo $businessArry['promotions']; ?></p>
                            </div>
                        </div>
                        <div class="gallery-grid-div">
                            <div class="grid-lg-row">
                                <?php
                                    if (array_key_exists('logo', $businessArry['images'])) {
                                        foreach($businessArry['images']['gallery'] as $key=>$img) {
                                            $business_gallery = content_url('businessGallery/').$img;
                                            echo '
                                                <div class="grid-lg-33 grid-lg-auto">
                                                    <div class="img-thumb">
                                                        <img src="'.$business_gallery.'" alt="gallery-bs" class="img-responsive img-gallery" />
                                                    </div>
                                                </div>
                                            ';
                                        }
                                    }
                                ?> 
                            </div>    
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </section>
    <script>
    jQuery(document).ready(function(){
        jQuery('.submit_business_detail_form').on('click', function(){
            // jQuery('#business_detail_contact_form').valid();
            
            if (jQuery('#business_detail_contact_form').valid()) {
                var businessDetailContactFromData = jQuery('#' + btnDataValue).serializeArray();
                jQuery.ajax({
                    url: DATABSE_PROCESS_URL,
                    type: 'post',
                    data: {
                        business_detail_contact_from_data: businessDetailContactFromData                        
                    },
                    cache: false,
                    dataType: "json",
                    success: function (response) {
                        if (response.success == true) {
                            console.log(response.message);
                            getAllExistingBusinessesContacts(response.data);
                            jQuery('#desc').html(response.message);
                            launch_toast();
                            jQuery('#' + btnDataValue).empty();
                        } else {
                            jQuery('#desc').html(response.message);
                            launch_toast();
                        }
                    }
                });
            }
        });
    });
    </script>
<?php get_footer(); ?>
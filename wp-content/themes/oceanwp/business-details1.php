<?php
/**
 * Template Name: business-detail1
 */


get_header(); ?>

    <section class="company-section">

        <div class="company-div company-div-header">
            <div class="container">

                <div class="clearfix"></div>

                <div class="business-white-forms01">

                    <div class="white-body-form-div white-body-space-form-div">

                        <div class="white-row-color-div form-white-minus-300">
                        
                            <div class="form-row-title form-col-10">
                                <div class="form-title-heading">
                                    <span class="form-title">שופיח</span>
                                </div>	
                            </div>

                            <div class="form-row form-row-20 form-col-90">
                                <div class="grid-auto grid-33">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">:קסעה םוחת</label>
                                        <div class="input-field-group">
                                            <select>
                                                <option value="">םימוחתה לכ</option>
                                                <option value="">םימוחתה לכ</option>
                                                <option value="">םימוחתה לכ</option>
                                            </select>
                                            <span class="dropdown-arrow"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="grid-auto grid-33">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="business_contact_role">:רצומ / תוריש</label>
                                        <div class="input-field-group">
                                            <select >
                                                <option value="">םירצומ / םיתורישה לכ</option>
                                                <option value="">םירצומ / םיתורישה לכ</option>
                                                <option value="">םירצומ / םיתורישה לכ</option>
                                            </select>
                                            <span class="dropdown-arrow"></span>
                                        </div>
                                    </div>
                                </div>						
                                <div class="grid-auto grid-33">
                                    <div class="form-group">
                                        <label class="control-label">:ישפוח טסקט</label>
                                        <div class="input-field-group">
                                            <input type="text" placeholder="...ןאכ ללמה תא סנכה" class="form-control input-md">
                                        </div>
                                    </div>
                                </div>						
                            </div>
                          						
                        </div>
                      
                        <div class="form-row form-row-btn-300">
                            <div class="button-abs-bl-auto">
                                <div class="btn-submit-group">
                                    <button class="btn btn-submit" id="business_contact_save" name="business_contact_save" data-value="field0" type="button">קסע שופיח<i class="icon-arrow-left"></i></button>
                                </div>
                            </div>
                        </div>		

                        <div class="clearfix"></div>
                            
                        
                    </div>  

                </div>

                <div class="clearfix"></div>

            </div>
        </div>

        <div class="company-div company-div-body">
            <div class="container">

                <div class="clearfix"></div>

                <div class="grid-lg-row">

                    <div class="grid-lg-25 grid-lg-auto">
                        <div class="find-bs-card-box">
                            <div class="img-thumb-group">
                                <img src="http://faceland.co.il/wp-content/themes/oceanwp/assets/img/find-bs01.jpg" alt="find-bs" class="img-responsive img-find" />
                            </div>
                            <div class="text-content">
                                <h3>םיעוריא</h3>
                            </div>
                        </div>
                    </div>

                    <div class="grid-lg-25 grid-lg-auto">
                        <div class="find-bs-card-box">
                            <div class="img-thumb-group">
                                <img src="http://faceland.co.il/wp-content/themes/oceanwp/assets/img/find-bs01.jpg" alt="find-bs" class="img-responsive img-find" />
                            </div>
                            <div class="text-content">
                                <h3>הנוזת ,תואירב המילשמ האופרו</h3>
                            </div>
                        </div>
                    </div>

                    <div class="grid-lg-25 grid-lg-auto">
                        <div class="find-bs-card-box">
                            <div class="img-thumb-group">
                                <img src="http://faceland.co.il/wp-content/themes/oceanwp/assets/img/find-bs01.jpg" alt="find-bs" class="img-responsive img-find" />
                            </div>
                            <div class="text-content">
                                <h3>הדילו ןוירה</h3>
                            </div>
                        </div>
                    </div>

                    <div class="grid-lg-25 grid-lg-auto">
                        <div class="find-bs-card-box">
                            <div class="img-thumb-group">
                                <img src="http://faceland.co.il/wp-content/themes/oceanwp/assets/img/find-bs01.jpg" alt="find-bs" class="img-responsive img-find" />
                            </div>
                            <div class="text-content">
                                <h3>תולעפהו תואנדס ,םיגוח</h3>
                            </div>
                        </div>
                    </div>

                    <div class="grid-lg-25 grid-lg-auto">
                        <div class="find-bs-card-box">
                            <div class="img-thumb-group">
                                <img src="http://faceland.co.il/wp-content/themes/oceanwp/assets/img/find-bs01.jpg" alt="find-bs" class="img-responsive img-find" />
                            </div>
                            <div class="text-content">
                                <h3>םייח ילעבו תואלקח</h3>
                            </div>
                        </div>
                    </div>

                    <div class="grid-lg-25 grid-lg-auto">
                        <div class="find-bs-card-box">
                            <div class="img-thumb-group">
                                <img src="http://faceland.co.il/wp-content/themes/oceanwp/assets/img/find-bs01.jpg" alt="find-bs" class="img-responsive img-find" />
                            </div>
                            <div class="text-content">
                                <h3>הקינורטקלאו למשח</h3>
                            </div>
                        </div>
                    </div>

                    <div class="grid-lg-25 grid-lg-auto">
                        <div class="find-bs-card-box">
                            <div class="img-thumb-group">
                                <img src="http://faceland.co.il/wp-content/themes/oceanwp/assets/img/find-bs01.jpg" alt="find-bs" class="img-responsive img-find" />
                            </div>
                            <div class="text-content">
                                <h3>יפויו חופיט</h3>
                            </div>
                        </div>
                    </div>

                    <div class="grid-lg-25 grid-lg-auto">
                        <div class="find-bs-card-box">
                            <div class="img-thumb-group">
                                <img src="http://faceland.co.il/wp-content/themes/oceanwp/assets/img/find-bs01.jpg" alt="find-bs" class="img-responsive img-find" />
                            </div>
                            <div class="text-content">
                                <h3>םייקסע םיתורישו ץועיי</h3>
                            </div>
                        </div>
                    </div>

                    <div class="grid-lg-25 grid-lg-auto">
                        <div class="find-bs-card-box">
                            <div class="img-thumb-group">
                                <img src="http://faceland.co.il/wp-content/themes/oceanwp/assets/img/find-bs01.jpg" alt="find-bs" class="img-responsive img-find" />
                            </div>
                            <div class="text-content">
                                <h3>רצחלו הניגל ,תיבל</h3>
                            </div>
                        </div>
                    </div>

                    <div class="grid-lg-25 grid-lg-auto">
                        <div class="find-bs-card-box">
                            <div class="img-thumb-group">
                                <img src="http://faceland.co.il/wp-content/themes/oceanwp/assets/img/find-bs01.jpg" alt="find-bs" class="img-responsive img-find" />
                            </div>
                            <div class="text-content">
                                <h3>תונדעסמו ןוזמ</h3>
                            </div>
                        </div>
                    </div>

                </div>    

                <div class="clearfix"></div>

            </div>
        </div>

        <div class="company-div company-div-body company-articles-body">
            <div class="container">

                <div class="clearfix"></div>

                <div class="box-section-06 articles-section">

                    <div class="grid-lg-row">
                            
                        <div class="grid-lg-50 grid-lg-auto">
                            
                            <div class=" post-article-box">
                                <div class="img-thumb-right">
                                    <img src="http://faceland.co.il/wp-content/themes/oceanwp/assets/img/post.jpg" class="img-responsive img-post" alt="">
                                </div>
                                <div class="content-txt-left">
                                    <h2>םירייוצמ תורצוא – קימיל</h2>
                                    <h6>דועו תונתמלו דרשמל ,תיבל םירייואמ ריינ ירצומ גתומ</h6>
                                    <p><a href="tel:0528657448">0528657448</a></p>
                                    <p>םיכנעתהו עובלג תדלומ בשומ</p>
                                    <a href="#" class="link-arrow"><i class="icon-arrow-left"></i></a>
                                </div>
                            </div>
                            
                        </div>

                        <div class="grid-lg-50 grid-lg-auto">
                            
                            <div class=" post-article-box">
                                <div class="img-thumb-right">
                                    <img src="http://faceland.co.il/wp-content/themes/oceanwp/assets/img/post.jpg" class="img-responsive img-post" alt="">
                                </div>
                                <div class="content-txt-left">
                                    <h2>םירייוצמ תורצוא – קימיל</h2>
                                    <h6> דועו תונתמלו דרשמל ,תיבל םירייואמ ריינ ירצומ גתומ</h6>
                                    <p><a href="tel:0528657448">0528657448</a></p>
                                    <p> איגש ת.א2000 קמע קמעה לדגמ ,ךורב רפכ תמוצ , לאערזי</p>
                                    <a href="#" class="link-arrow"><i class="icon-arrow-left"></i></a>
                                </div>
                            </div>
                            
                        </div>

                        <div class="grid-lg-50 grid-lg-auto">
                            
                            <div class=" post-article-box">
                                <div class="img-thumb-right">
                                    <img src="http://faceland.co.il/wp-content/themes/oceanwp/assets/img/post.jpg" class="img-responsive img-post" alt="">
                                </div>
                                <div class="content-txt-left">
                                    <h2>םירייוצמ תורצוא – קימיל</h2>
                                    <h6>דועו תונתמלו דרשמל ,תיבל םירייואמ ריינ ירצומ גתומ</h6>
                                    <p><a href="tel:0528657448">0528657448</a></p>
                                    <p>םיכנעתהו עובלג תדלומ בשומ</p>
                                    <a href="#" class="link-arrow"><i class="icon-arrow-left"></i></a>
                                </div>
                            </div>
                            
                        </div>

                        <div class="grid-lg-50 grid-lg-auto">
                            
                            <div class=" post-article-box">
                                <div class="img-thumb-right">
                                    <img src="http://faceland.co.il/wp-content/themes/oceanwp/assets/img/post.jpg" class="img-responsive img-post" alt="">
                                </div>
                                <div class="content-txt-left">
                                    <h2>םירייוצמ תורצוא – קימיל</h2>
                                    <h6>דועו תונתמלו דרשמל ,תיבל םירייואמ ריינ ירצומ גתומ</h6>
                                    <p><a href="tel:0528657448">0528657448</a></p>
                                    <p>םיכנעתהו עובלג תדלומ בשומ</p>
                                    <a href="#" class="link-arrow"><i class="icon-arrow-left"></i></a>
                                </div>
                            </div>
                            
                        </div>

                    </div>

                    <div class="grid-lg-row pagination-lg-row">
                        <div class="grid-lg-100 grid-lg-auto">

                            <div class="pagination-box">
                                <ul class="pagination-ul">
                                    <li class="prev-li"><a href="#" class="prev-link"><i class="icon-arrow-left"></i></a></li>
                                    <li class="active"><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a href="#">4</a></li>
                                    <li class="next-li"><a href="#" class="next-link"><i class="icon-arrow-left"></i></a></li>
                                </ul>
                                
                            </div>  
                            
                        </div>
                    </div>    
                    
                </div>  

                <div class="clearfix"></div>

            </div>
        </div>

        <div class="company-details-body">
            <div class="container">

                <div class="clearfix"></div>

                <div class="grid-lg-row">

                    <div class="grid-lg-25 grid-lg-auto rightside-section">
                    
                        <div class="img-thumb-group-top">
                            <img src="http://faceland.co.il/wp-content/themes/oceanwp/assets/img/post.jpg" class="img-responsive img-company" alt="">
                        </div>

                        <div class="content-detailsrow-div border-bottom01">
                            <h3>:קסעה לע דוע</h3>
                            <div class="btn-group-div">
                                <div class="btn-group-row">
                                    <a class="btn btn-add btn-social" >
                                        <span class="right-span"><i class="social-icon icon-s01"></i><span class="txtspan">טנרטניא רתא</span></span> 
                                        <span class="left-span"><i class="icon-arrow-left"></i></span>
                                    </a>
                                </div>
                                <div class="btn-group-row">
                                    <a class="btn btn-add btn-social" >
                                        <span class="right-span"><i class="social-icon icon-s02"></i><span class="txtspan">קובסייפ</span></span>
                                        <span class="left-span"><i class="icon-arrow-left"></i></span>
                                    </a>
                                </div>
                                <div class="btn-group-row">
                                    <a class="btn btn-add btn-social" >
                                        <span class="right-span"><i class="social-icon icon-s03"></i><span class="txtspan">םרגטסניא</span></span> 
                                        <span class="left-span"><i class="icon-arrow-left"></i></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        
                        <div class="content-detailsrow-div border-bottom01">
                            <h3>!רשק רוציל םינמזומ</h3>
                            <div class="btn-group-div">
                                <div class="text-row">
                                    <p><a href="tel:052-1234567">052-1234567</a></p>
                                    <p><a href="mailto:limorbit@gmail.com">limorbit@gmail.com</a></p>
                                </div>
                                <div class="btn-group-row">
                                    <a class="btn btn-add btn-social" >
                                        <span class="right-span"><i class="social-icon icon-s04"></i><span class="txtspan">פאסטאוו</span></span> 
                                        <span class="left-span"><i class="icon-arrow-left"></i></span>
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="content-detailsrow-div content-formrow-div">
                            <h3>:רתאה ךרד תונפל םג רשפא</h3>

                            <div class="business-white-forms01">
                                <div class="white-body-form-div ">
                                    <div class="white-row-color-div">
                                        <div class="form-row">
                                            <div class="grid-auto grid-100">
                                                <div class="form-group">
                                                    <label class="control-label"> :םש</label>
                                                    <div class="input-field-group">
                                                        <input type="text" class="form-control input-md">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="grid-auto grid-100">
                                                <div class="form-group">
                                                    <label class="control-label"> :ןופלט</label>
                                                    <div class="input-field-group">
                                                        <input type="text" class="form-control input-md">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="grid-auto grid-100">
                                                <div class="form-group">
                                                    <label class="control-label"> :ל״אוד</label>
                                                    <div class="input-field-group">
                                                        <input type="text" class="form-control input-md">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="grid-auto grid-100">
                                                <div class="form-group">
                                                    <label class="control-label"> :תורועה</label>
                                                    <div class="input-field-group">
                                                        <textarea></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>    
                                    </div>  
                                    <div class="btn-row-div">
                                        <button class="btn btn-submit" type="submit">חלש<i class="icon-arrow-left"></i></button>
                                    </div>  
                                </div>
                            </div>    

                        </div>    

                    </div>

                    <div class="grid-lg-75 grid-lg-auto leftside-section">

                        <div class="describe-root-txt-div">
                            <div class="row-repeat">
                                <h3>םירייוצמ תורצוא – קימיל</h3>
                                <h4>תונתמלו דרשמל ,תיבל םירייואמ ריינ ירצומ גתומ</h4>
                                <p>תדלומ בשומ</p>
                            </div>
                            <div class="row-repeat">
                                <h4>:קסעה ת/לעב</h4>
                                <p>ןוסלטיפ רומיל</p>
                            </div>
                            <div class="row-repeat">
                                <h4>:קסעה תודוא</h4>
                                <p>מה לש םיירוקמ םיבוציעו םירויאב םיוולמ קימיל לש ריינה ירצומ  ,בוציע ,רויאל התבהא תא תבלשמה ןוסלטיפ ןטיב רומיל תבצעמהו תרייא -ו הריצי diy .בל יבוש םירויאבו דפקומ בוציעב םיישומיש םירצומ ידכל </p>
                            </div>
                            <div class="row-repeat">
                                <h4>:םירצומ / םיתוריש</h4>
                                <p>ללימיק חנות מקוונת וסטודיו במושב מולדת בו ניתן למצוא פריטים מיוחדים מנייר לשדרוג פינת העבודה והבית, לחדרי הילדים, לעבודות יצירה ולמתנות מיוחדות לאנשים שאוהבים במיוחד. מה בחנות: פנקסים, מחברות, לוחות תכנון, דפי מדבקות, גלויות מאויירות, הדפסים ופוסטרים למסגור ותלייה, ניירות לעטיפה ולעבודות יצירה, מארזי מתנה מהודרים, קבצי פרינטבלס דיגיטליים להורדה ולהדפסה ביתית ועוד. </p>
                            </div>
                            <div class="row-repeat">
                                <h4>הודעות ומבצעים: </h4>
                                <p>חדש בלימיק - מבחר מארזי מתנה שווים ומפנקים לכבוד החג עם מבחרמשובח של מוצרי נייר מתוקים במחיר משתלם ומשלוח עד הבית. לפרטים נוספים והזמנות היכנסו לאתר או צרו קשר</p>
                            </div>
                        </div>

                        <div class="gallery-grid-div">
                            <div class="grid-lg-row">
                                <div class="grid-lg-33 grid-lg-auto">
                                    <div class="img-thumb">
                                        <img src="http://faceland.co.il/wp-content/themes/oceanwp/assets/img/company-gallery.jpg" alt="gallery-bs" class="img-responsive img-gallery" />
                                    </div>
                                </div>  
                                <div class="grid-lg-33 grid-lg-auto">
                                    <div class="img-thumb">
                                        <img src="http://faceland.co.il/wp-content/themes/oceanwp/assets/img/company-gallery.jpg" alt="gallery-bs" class="img-responsive img-gallery" />
                                    </div>
                                </div>  
                                <div class="grid-lg-33 grid-lg-auto">
                                    <div class="img-thumb">
                                        <img src="http://faceland.co.il/wp-content/themes/oceanwp/assets/img/company-gallery.jpg" alt="gallery-bs" class="img-responsive img-gallery" />
                                    </div>
                                </div>  
                                <div class="grid-lg-33 grid-lg-auto">
                                    <div class="img-thumb">
                                        <img src="http://faceland.co.il/wp-content/themes/oceanwp/assets/img/company-gallery.jpg" alt="gallery-bs" class="img-responsive img-gallery" />
                                    </div>
                                </div>  
                                <div class="grid-lg-33 grid-lg-auto">
                                    <div class="img-thumb">
                                        <img src="http://faceland.co.il/wp-content/themes/oceanwp/assets/img/company-gallery.jpg" alt="gallery-bs" class="img-responsive img-gallery" />
                                    </div>
                                </div>  
                                <div class="grid-lg-33 grid-lg-auto">
                                    <div class="img-thumb">
                                        <img src="http://faceland.co.il/wp-content/themes/oceanwp/assets/img/company-gallery.jpg" alt="gallery-bs" class="img-responsive img-gallery" />
                                    </div>
                                </div>    
                            </div>    
                        </div>

                    </div>

                </div>

                <div class="clearfix"></div>

            </div>
        </div>

        <div class="campus-main-div">
            <div class="container">

                <div class="clearfix"></div>

                <div class="campus-honor-box-div">

                    <div class="img-thumb-right">
                        <img src="http://faceland.co.il/wp-content/themes/oceanwp/assets/img/campus-img.jpg" class="img-responsive img-post" alt="">
                    </div>
                    <div class="content-txt-left">

                        <div class="campus_post_datetime">
                            <span class="digit-number">24</span> <span class="txt-span">ינוי</span>
                        </div>

                        <div class="content-p40">
                            <h2>תויונמדזה רוציל - תויורשפא שוגפל .תיתרבח הריירק</h2>
                            <p>יתרבחה רזגמב הדובע ישפחמל שגפמ</p>
                            <p>תעפי ץוביק קמעה ןואיזומ</p>
                            <p><span> יעיבר םוי</span> | <span>10.7.2019</span></p>
                            <p><span>19:45</span><span> תוסנכתה </span>|<span> 20:00</span><span> םיליחתמ </span></p>
                        </div>

                        <div class="button-abs-bl-auto">
                            <div class="btn-submit-group">
                                <button class="btn btn-submit" id="business_contact_save" name="business_contact_save" data-value="field0" type="button">רשמו אותי לאתר<i class="icon-arrow-left"></i></button>
                            </div>
                        </div>
                    
                    </div>
                
                </div>

                <div class="clearfix"></div>

                <div class="light-box-content-div">
                    <div class="light-header-div">
                        <h3>  שגפמה תינכות:</h3>
                    </div>
                    <div class="light-body-div">
                        <p>לודגה הריירקה לוהינ םזימ לש הפתושו המיקמ ,הריירק לוהינל תיחמומו תימזי ,באב-ןויחוא העונ לש האצרה</p>
                        <p>תויורשפאהו הדובעה קוש לש תוחתפתהו תומגמב קוסעת האצרהה ."הריירק יל ושעת העונו הנד" לארשיב</p>
                        <p>.הבוט הסנרפו קופיס ,האנה תרמשמה הריירק תריציל ,יתרבחה רזגמב תוברל ,ונל שיש תושדחה</p>
                        <p>ןופצה רוזאב יתרבחה רזגמב םידיקפתו הריירק</p>
                        <p> דוסי" תינכות להנמ ,תויכוניח תוינכת םזי - גרבניו רמועاساس.ירוביצה תורישל הרשכה תינכות - "</p>
                        <p>.םיקמעה רוזאב יתקוסעת-ילכלכ חותיפל תינכות "קמעב ירבעמ" ל"כנמ - ךובסיו תאיל</p>
                        <p>.שארמ המשרה תשרדנ ךא ,תולע אלל שגפמה</p>
                        <h5><span> ילזרב ןח :םיפסונ םיטרפל</span><span class="hide-none-mobile"> |</span><span> 052-12345678 </span><span class="hide-none-mobile">|</span><span> chen@mhk.co.il </span></h5>
                    </div>
                </div>

            </div>
        </div>

        <div class="job-post-root-top">
            <div class="business-white-forms01 new-job-form01 new-job-post-form01">
            
                <!-- -->
                <div class="container">

                    <div class="business-form-root-div">
                        <div class="bs-form-div">

                            <div class="white-header-top addBusiness-form-top color-update-newjob01">
                                <div class="addBusiness-first-line">
                                    <h2>םישורדה חולב תורשמ םוסרפ ךרוצל םושיר ךשמה</h2>
                                </div>
                                <div class="addBusiness-second-line">
                                    <span>.רשק ישנא/שיא רידגהל שי ,ךלש ליימל םידמעומ לש םייח תורוק תלבק ךרוצל</span>
                                </div>
                            </div>

                            <div class="white-body-form-div">
                                <div class="white-row-color-div business_details_body">

                                    <div class="inner-heading-div">
                                        <h3>  ׳סמ רשק שיא 1 </h3>
                                    </div>

                                    <div class="form-row">
                                        <div class="grid-auto grid-33">
                                            <div class="form-group">
                                                <label class="control-label" for="">םש</label>
                                                <div class="input-field-group">
                                                    <input id="" name="" type="text" placeholder="" class="required form-control input-md" >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="grid-auto grid-33">
                                            <div class="form-group">
                                                <label class="col-md-4 control-label" for="">דיקפת</label>
                                                <div class="input-field-group">
                                                    <select id="" name="" class="required">
                                                        <option value="">המישר</option>
                                                        <option value="1">המישר</option>
                                                        <option value="2">המישר</option>
                                                    </select>
                                                    <span class="dropdown-arrow"></span>
                                                </div>
                                            </div>
                                        </div>						
                                        <div class="grid-auto grid-33">
                                            <div class="form-group">
                                                <label class="control-label" for="">ןופלט</label>
                                                <div class="input-field-group">
                                                    <input id="" type="text" placeholder="" class="required form-control input-md">
                                                </div>
                                            </div>
                                        </div>						
                                    </div>

                                    <div class="form-row">
                                        <div class="grid-auto grid-50">
                                            <div class="form-group">
                                                <label class="col-md-4 control-label" for="">םייח תורוק תלבקל ל"אוד</label>
                                                <div class="input-field-group">
                                                    <input id="" name="" type="text" placeholder="ל״אוד סנכה" class="required form-control input-md">
                                                </div>
                                            </div>
                                        </div>						
                                        <div class="grid-auto grid-50">
                                            <div class="form-group">
                                                <label class="col-md-4 control-label" for="">רשק תריציל ל"אוד</label>
                                                <div class="input-field-group">
                                                    <input id="" name="" type="text" placeholder="ל״אוד סנכה" class="required form-control input-md">
                                                </div>
                                            </div>
                                        </div>			
                                    </div>
                                    
                                    <div class="form-row">
                                        <div class="grid-auto grid-100">
                                            <div class="form-group">
                                                <div class="input-field-group checkbox-group auto-checkbox-group">
                                                    <div class="checkbox-col">
                                                        <label for="" class="wpuf-checkbox-inline">
                                                            <input type="checkbox" id="" name="" value="1">
                                                            קמעב םירבעממ םיפסונ םינוכדעו רטלזוינ לבקל ת/ןיינועמ ינא
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>	
                                
                                    <div class="form-row">
                                    
                                        <div class="grid-auto grid-100">
                                            <div class="form-group btn-add-group-row">
                                                <div class="padding-top-auto">
                                                    <button class="btn btn-add" id="" class="business_contact_add_new"> <i class="edit-plus-icon icon-black pl-5"></i>ףסונ שיא תפסוה <i class="icon-arrow-left"></i></button>
                                                </div>	
                                            </div>
                                        </div>		

                                    </div>

                                    <div class="btn-row-div btn-new-job-submit">
                                        <button class="btn btn-submit btn-job-submit" name="new_job_create" type="submit">השדח הרשמ םוסרפ <i class="icon-arrow-left"></i></button>
                                    </div>	
                                    
                                </div>
                                
                                <div class="clearfix"></div>				
                            </div>

                            <div class="clearfix"></div>
                            
                        </div>
                       
                    </div>

                </div>

                <!-- -->

            </div>    
        </div>

        <div class="article-prof-info-root">
            <div class="campus-main-div article-prof-info-main-div">
                <div class="container">

                    <div class="clearfix"></div>

                    <div class="light-box-content-div">
                        <div class="light-header-div">
                            <h3> כתיבת קורות חיים הכנה לכותרת שתי שורות</h3>
                        </div>
                        <div class="light-body-div">
                            <div class="img-thumb-div">
                                <img src="http://faceland.co.il/wp-content/themes/oceanwp/assets/img/article-prof-info.jpg" class="img-responsive img-article" alt="article and professional information">
                            </div>
                            <p class="mb-40">לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. מוסן מנת. להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.</p>
                            <p class="mb-40">קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק.</p>
                            <p class="mb-40">ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.</p>
                            <p class="mb-40">גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצ סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.</p>
                            <p class="mb-40">הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.</p>
                            <p class="mb-40">קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי</p>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="news-root-div">
            <div class="news-inner-div">
                <div class="container">

                    <div class="grid-lg-row">
                        <div class="grid-lg-25 grid-lg-auto">
                            <div class="news-white-box">
                                <div class="top-new-heading"> <a href="#"><h3> גמלאים מחזקים את צבא הגנה לישראל </h3></a></div>
                                <div class="middle-content-prag"> <p> תכנית תעסוחיל – של"מ מבית עמותת לאופק  הינה תכנית... </p></div>
                                <div class="btn-arrow-icon"><a class="arrow-link" href="#"><i class="icon-arrow-left"></i></a></div>
                            </div>
                        </div>
                        <div class="grid-lg-25 grid-lg-auto">
                            <div class="news-white-box">
                                <div class="top-new-heading"> <a href="#"><h3> גמלאים מחזקים את צבא הגנה לישראל </h3></a></div>
                                <div class="middle-content-prag"> <p> תכנית תעסוחיל – של"מ מבית עמותת לאופק  הינה תכנית... </p></div>
                                <div class="btn-arrow-icon"><a class="arrow-link" href="#"><i class="icon-arrow-left"></i></a></div>
                            </div>
                        </div>
                        <div class="grid-lg-25 grid-lg-auto">
                            <div class="news-white-box">
                                <div class="top-new-heading"> <a href="#"><h3> גמלאים מחזקים את צבא הגנה לישראל </h3></a></div>
                                <div class="middle-content-prag"> <p> תכנית תעסוחיל – של"מ מבית עמותת לאופק  הינה תכנית... </p></div>
                                <div class="btn-arrow-icon"><a class="arrow-link" href="#"><i class="icon-arrow-left"></i></a></div>
                            </div>
                        </div>
                        <div class="grid-lg-25 grid-lg-auto">
                            <div class="news-white-box">
                                <div class="top-new-heading"> <a href="#"><h3> גמלאים מחזקים את צבא הגנה לישראל </h3></a></div>
                                <div class="middle-content-prag"> <p> תכנית תעסוחיל – של"מ מבית עמותת לאופק  הינה תכנית... </p></div>
                                <div class="btn-arrow-icon"><a class="arrow-link" href="#"><i class="icon-arrow-left"></i></a></div>
                            </div>
                        </div>
                        <div class="grid-lg-25 grid-lg-auto">
                            <div class="news-white-box">
                                <div class="top-new-heading"> <a href="#"><h3> גמלאים מחזקים את צבא הגנה לישראל </h3></a></div>
                                <div class="middle-content-prag"> <p> תכנית תעסוחיל – של"מ מבית עמותת לאופק  הינה תכנית... </p></div>
                                <div class="btn-arrow-icon"><a class="arrow-link" href="#"><i class="icon-arrow-left"></i></a></div>
                            </div>
                        </div>
                        <div class="grid-lg-25 grid-lg-auto">
                            <div class="news-white-box">
                                <div class="top-new-heading"> <a href="#"><h3> גמלאים מחזקים את צבא הגנה לישראל </h3></a></div>
                                <div class="middle-content-prag"> <p> תכנית תעסוחיל – של"מ מבית עמותת לאופק  הינה תכנית... </p></div>
                                <div class="btn-arrow-icon"><a class="arrow-link" href="#"><i class="icon-arrow-left"></i></a></div>
                            </div>
                        </div>
                        <div class="grid-lg-25 grid-lg-auto">
                            <div class="news-white-box">
                                <div class="top-new-heading"> <a href="#"><h3> גמלאים מחזקים את צבא הגנה לישראל </h3></a></div>
                                <div class="middle-content-prag"> <p> תכנית תעסוחיל – של"מ מבית עמותת לאופק  הינה תכנית... </p></div>
                                <div class="btn-arrow-icon"><a class="arrow-link" href="#"><i class="icon-arrow-left"></i></a></div>
                            </div>
                        </div>

                        <div class="grid-lg-25 grid-lg-auto">
                            <div class="news-white-box">
                                <div class="top-new-heading"> <a href="#"><h3> גמלאים מחזקים את צבא הגנה לישראל </h3></a></div>
                                <div class="middle-content-prag"> <p> תכנית תעסוחיל – של"מ מבית עמותת לאופק  הינה תכנית... </p></div>
                                <div class="btn-arrow-icon"><a class="arrow-link" href="#"><i class="icon-arrow-left"></i></a></div>
                            </div>
                        </div>

                        <div class="grid-lg-25 grid-lg-auto">
                            <div class="news-white-box">
                                <div class="top-new-heading"> <a href="#"><h3> גמלאים מחזקים את צבא הגנה לישראל </h3></a></div>
                                <div class="middle-content-prag"> <p> תכנית תעסוחיל – של"מ מבית עמותת לאופק  הינה תכנית... </p></div>
                                <div class="btn-arrow-icon"><a class="arrow-link" href="#"><i class="icon-arrow-left"></i></a></div>
                            </div>
                        </div>

                        <div class="grid-lg-25 grid-lg-auto">
                            <div class="news-white-box">
                                <div class="top-new-heading"> <a href="#"><h3> גמלאים מחזקים את צבא הגנה לישראל </h3></a></div>
                                <div class="middle-content-prag"> <p> תכנית תעסוחיל – של"מ מבית עמותת לאופק  הינה תכנית... </p></div>
                                <div class="btn-arrow-icon"><a class="arrow-link" href="#"><i class="icon-arrow-left"></i></a></div>
                            </div>
                        </div>

                        <div class="grid-lg-25 grid-lg-auto">
                            <div class="news-white-box">
                                <div class="top-new-heading"> <a href="#"><h3> גמלאים מחזקים את צבא הגנה לישראל </h3></a></div>
                                <div class="middle-content-prag"> <p> תכנית תעסוחיל – של"מ מבית עמותת לאופק  הינה תכנית... </p></div>
                                <div class="btn-arrow-icon"><a class="arrow-link" href="#"><i class="icon-arrow-left"></i></a></div>
                            </div>
                        </div>

                        <div class="grid-lg-25 grid-lg-auto">
                            <div class="news-white-box">
                                <div class="top-new-heading"> <a href="#"><h3> גמלאים מחזקים את צבא הגנה לישראל </h3></a></div>
                                <div class="middle-content-prag"> <p> תכנית תעסוחיל – של"מ מבית עמותת לאופק  הינה תכנית... </p></div>
                                <div class="btn-arrow-icon"><a class="arrow-link" href="#"><i class="icon-arrow-left"></i></a></div>
                            </div>
                        </div>
                        

                    </div> 
                    
                    <div class="grid-lg-row pagination-lg-row pagination-whitecolor-row">
                        <div class="grid-lg-100 grid-lg-auto">

                            <div class="pagination-box">
                                <ul class="pagination-ul">
                                    <li class="prev-li"><a href="#" class="prev-link"><i class="icon-arrow-left"></i></a></li>
                                    <li class="active"><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a href="#">4</a></li>
                                    <li class="next-li"><a href="#" class="next-link"><i class="icon-arrow-left"></i></a></li>
                                </ul>
                                
                            </div>  
                            
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="company-div company-div-body company-articles-body article-prof-info-body2">
            <div class="container">

                <div class="clearfix"></div>

                <div class="box-section-06 articles-section article-prof-info2">

                    <div class="grid-lg-row">
                            
                        <div class="grid-lg-50 grid-lg-auto">
                            
                            <div class=" post-article-box">
                                <div class="img-thumb-right">
                                    <img src="http://faceland.co.il/wp-content/themes/oceanwp/assets/img/article-prof-info.jpg" class="img-responsive img-post" alt=""> 
                                </div>
                                <div class="content-txt-left">
                                    <h2>כתיבת קורות חיים הכנה לכותרת שתי שורות</h2>
                                    <p>קורות החיים שלך הם כרטיס הביקור שלך לחיפוש עבודה, ומהווים הליך שיווק ראשוני... </p>
                                    <a href="#" class="link-arrow"><i class="icon-arrow-left"></i></a>
                                </div>
                            </div>
                            
                        </div>

                        <div class="grid-lg-50 grid-lg-auto">
                            
                            <div class=" post-article-box">
                                <div class="img-thumb-right">
                                    <img src="http://faceland.co.il/wp-content/themes/oceanwp/assets/img/article-prof-info.jpg" class="img-responsive img-post" alt="">
                                </div>
                                <div class="content-txt-left">
                                    <h2>כתיבת קורות חיים הכנה לכותרת שתי שורות</h2>
                                    <p>קורות החיים שלך הם כרטיס הביקור שלך לחיפוש עבודה, ומהווים הליך שיווק ראשוני... </p>
                                    <a href="#" class="link-arrow"><i class="icon-arrow-left"></i></a>
                                </div>
                            </div>
                            
                        </div>

                        <div class="grid-lg-50 grid-lg-auto">
                            
                            <div class=" post-article-box">
                                <div class="img-thumb-right">
                                    <img src="http://faceland.co.il/wp-content/themes/oceanwp/assets/img/article-prof-info.jpg" class="img-responsive img-post" alt="">
                                </div>
                                <div class="content-txt-left">
                                    <h2>כתיבת קורות חיים הכנה לכותרת שתי שורות</h2>
                                    <p>קורות החיים שלך הם כרטיס הביקור שלך לחיפוש עבודה, ומהווים הליך שיווק ראשוני... </p>
                                    <a href="#" class="link-arrow"><i class="icon-arrow-left"></i></a>
                                </div>
                            </div>
                            
                        </div>

                        <div class="grid-lg-50 grid-lg-auto">
                            
                        <div class=" post-article-box">
                                <div class="img-thumb-right">
                                    <img src="http://faceland.co.il/wp-content/themes/oceanwp/assets/img/article-prof-info.jpg" class="img-responsive img-post" alt="">
                                </div>
                                <div class="content-txt-left">
                                    <h2>כתיבת קורות חיים הכנה לכותרת שתי שורות</h2>
                                    <p>קורות החיים שלך הם כרטיס הביקור שלך לחיפוש עבודה, ומהווים הליך שיווק ראשוני... </p>
                                    <a href="#" class="link-arrow"><i class="icon-arrow-left"></i></a>
                                </div>
                            </div>
                            
                        </div>

                    </div>

                    <div class="grid-lg-row pagination-lg-row">
                        <div class="grid-lg-100 grid-lg-auto">

                            <div class="pagination-box">
                                <ul class="pagination-ul">
                                    <li class="prev-li"><a href="#" class="prev-link"><i class="icon-arrow-left"></i></a></li>
                                    <li class="active"><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a href="#">4</a></li>
                                    <li class="next-li"><a href="#" class="next-link"><i class="icon-arrow-left"></i></a></li>
                                </ul>
                                
                            </div>  
                            
                        </div>
                    </div>    
                    
                </div>  

                <div class="clearfix"></div>

            </div>
        </div>

        <div class="campus-transitions-root-div">
            <div class="campus-transitions-inner-div box-section-04 campus-section">
                <div class="container">

                    <div class="campus_items">

                        <div class="grid-lg-row">

                            <div class="grid-lg-25 grid-lg-auto">
                                <div class="campus_box">
                                    <div class="campus_post_datetime">
                                        <span class="digit-number">24</span> <span class="txt-span">ינוי</span>
                                    </div>
                                    <div class="overlay_block">
                                        <p>פרטים נוספים</p>
                                        <p><i class="icon-arrow-left"></i></p>
                                    </div>
                                    <div class="section_top">
                                        <img src="http://faceland.co.il/wp-content/themes/oceanwp/assets/img/campus-img.jpg" title="" alt="" class="campus_img">
                                    </div>
                                    <div class="section_bottom">
                                        <h3><span class="span-block"> שוגפל .תיתרבח הריירק</span><span class="span-block">תויונמדזה רוציל - תויורשפא</span></h3>
                                        <p>יתרבחה רזגמב הדובע ישפחמל שגפמ קמעב םירבעמ ,קמעה לדגמ </p>
                                        <span class="campus_time">
                                            <span> יעיבר םוי</span> <span>24.6.19</span> <span>19:45</span>
                                        </span>
                                    </div>
                                </div>
                            </div>    

                            <div class="grid-lg-25 grid-lg-auto">
                                <div class="campus_box">
                                    <div class="campus_post_datetime">
                                        <span class="digit-number">2</span> <span class="txt-span">ילוי</span>
                                    </div>
                                    <div class="overlay_block">
                                        <p>פרטים נוספים</p>
                                        <p><i class="icon-arrow-left"></i></p>
                                    </div>
                                    <div class="section_top">
                                        <img src="http://13.127.108.199/dev/wordpress/mavvarim/wp-content/uploads/2019/08/campus3.png" title="" alt="" class="campus_img">
                                    </div>
                                    <div class="section_bottom">
                                        <h3><span class="span-block"> ילטיגיד קוויש סרוק</span></h3>
                                        <p>רצק טפשמ הנשמ תרתוכ  קמעב םירבעמ ,קמעה לדגמ </p>
                                        <span class="campus_time">
                                            <span>ישימח םוי</span> <span>2.7.19</span> <span>08:00-13:00</span>
                                        </span>
                                    </div>
                                </div>
                            </div>    

                            <div class="grid-lg-25 grid-lg-auto">
                                <div class="campus_box">
                                    <div class="campus_post_datetime">
                                        <span class="digit-number">14</span> <span class="txt-span">גוא</span>
                                    </div>
                                    <div class="overlay_block">
                                        <p>פרטים נוספים</p>
                                        <p><i class="icon-arrow-left"></i></p>
                                    </div>
                                    <div class="section_top">
                                        <img src="http://13.127.108.199/dev/wordpress/mavvarim/wp-content/uploads/2019/08/campus2.png" title="" alt="" class="campus_img">
                                    </div>
                                    <div class="section_bottom">
                                        <h3><span class="span-block"> ןידאקניל תנדס </span></h3>
                                        <p> יתש ךרואב לבא רצק טפשמ הנשמ תרתוכ   ךירצ םא תורוש  תונייעמה קמע </p>
                                        <span class="campus_time">
                                            <span>  ינש םוי </span> <span>14.8.19</span> <span>20:00</span>
                                        </span>
                                    </div>
                                </div>
                            </div>   

                            <div class="grid-lg-25 grid-lg-auto">
                                <div class="campus_box">
                                    <div class="campus_post_datetime">
                                        <span class="digit-number">30</span> <span class="txt-span">קוא</span>
                                    </div>
                                    <div class="overlay_block">
                                        <p>פרטים נוספים</p>
                                        <p><i class="icon-arrow-left"></i></p>
                                    </div>
                                    <div class="section_top">
                                        <img src="http://13.127.108.199/dev/wordpress/mavvarim/wp-content/uploads/2019/08/campus1.png" title="" alt="" class="campus_img">
                                    </div>
                                    <div class="section_bottom">
                                        <h3><span class="span-block"> זנשקנוק קובסייפ תנדס </span></h3>
                                        <p>רצק טפשמ הנשמ תרתוכ ודיגמ הצעומה יניינב </p>
                                        <span class="campus_time">
                                            <span> ישימח םוי </span> <span>30.10.19</span> <span>11:00</span>
                                        </span>
                                    </div>
                                </div>
                            </div>     

                            <div class="grid-lg-25 grid-lg-auto">
                                <div class="campus_box">
                                    <div class="campus_post_datetime">
                                        <span class="digit-number">24</span> <span class="txt-span">ינוי</span>
                                    </div>
                                    <div class="overlay_block">
                                        <p>פרטים נוספים</p>
                                        <p><i class="icon-arrow-left"></i></p>
                                    </div>
                                    <div class="section_top">
                                        <img src="http://13.127.108.199/dev/wordpress/mavvarim/wp-content/uploads/2019/08/campus4.png" title="" alt="" class="campus_img">
                                    </div>
                                    <div class="section_bottom">
                                        <h3><span class="span-block"> םיקסע ילעבל םוליצ תנדס </span></h3>
                                        <p> רצק טפשמ הנשמ תרתוכ קמעב םירבעמ ,קמעה לדגמ </p>
                                        <span class="campus_time">
                                            <span>  ישימח םוי </span> <span>11:00</span>
                                        </span>
                                    </div>
                                </div>
                            </div>    

                            <div class="grid-lg-25 grid-lg-auto">
                                <div class="campus_box">
                                    <div class="campus_post_datetime">
                                        <span class="digit-number">2</span> <span class="txt-span">ילוי</span>
                                    </div>
                                    <div class="overlay_block">
                                        <p>פרטים נוספים</p>
                                        <p><i class="icon-arrow-left"></i></p>
                                    </div>
                                    <div class="section_top">
                                        <img src="http://13.127.108.199/dev/wordpress/mavvarim/wp-content/uploads/2019/08/campus3.png" title="" alt="" class="campus_img">
                                    </div>
                                    <div class="section_bottom">
                                        <h3><span class="span-block"> ילטיגיד קוויש סרוק</span></h3>
                                        <p>רצק טפשמ הנשמ תרתוכ  קמעב םירבעמ ,קמעה לדגמ </p>
                                        <span class="campus_time">
                                            <span>ישימח םוי</span> <span>2.7.19</span> <span>08:00-13:00</span>
                                        </span>
                                    </div>
                                </div>
                            </div>    

                            <div class="grid-lg-25 grid-lg-auto">
                                <div class="campus_box">
                                    <div class="campus_post_datetime">
                                        <span class="digit-number">14</span> <span class="txt-span">גוא</span>
                                    </div>
                                    <div class="overlay_block">
                                        <p>פרטים נוספים</p>
                                        <p><i class="icon-arrow-left"></i></p>
                                    </div>
                                    <div class="section_top">
                                        <img src="http://13.127.108.199/dev/wordpress/mavvarim/wp-content/uploads/2019/08/campus2.png" title="" alt="" class="campus_img">
                                    </div>
                                    <div class="section_bottom">
                                        <h3><span class="span-block"> ןידאקניל תנדס </span></h3>
                                        <p> יתש ךרואב לבא רצק טפשמ הנשמ תרתוכ   ךירצ םא תורוש  תונייעמה קמע </p>
                                        <span class="campus_time">
                                            <span>  ינש םוי </span> <span>14.8.19</span> <span>20:00</span>
                                        </span>
                                    </div>
                                </div>
                            </div>   

                            <div class="grid-lg-25 grid-lg-auto">
                                <div class="campus_box">
                                    <div class="campus_post_datetime">
                                        <span class="digit-number">30</span> <span class="txt-span">קוא</span>
                                    </div>
                                    <div class="overlay_block">
                                        <p>פרטים נוספים</p>
                                        <p><i class="icon-arrow-left"></i></p>
                                    </div>
                                    <div class="section_top">
                                        <img src="http://13.127.108.199/dev/wordpress/mavvarim/wp-content/uploads/2019/08/campus1.png" title="" alt="" class="campus_img">
                                    </div>
                                    <div class="section_bottom">
                                        <h3><span class="span-block"> זנשקנוק קובסייפ תנדס </span></h3>
                                        <p>רצק טפשמ הנשמ תרתוכ ודיגמ הצעומה יניינב </p>
                                        <span class="campus_time">
                                            <span> ישימח םוי </span> <span>30.10.19</span> <span>11:00</span>
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <div class="grid-lg-25 grid-lg-auto">
                                <div class="campus_box">
                                    <div class="campus_post_datetime">
                                        <span class="digit-number">24</span> <span class="txt-span">ינוי</span>
                                    </div>
                                    <div class="overlay_block">
                                        <p>פרטים נוספים</p>
                                        <p><i class="icon-arrow-left"></i></p>
                                    </div>
                                    <div class="section_top">
                                        <img src="http://13.127.108.199/dev/wordpress/mavvarim/wp-content/uploads/2019/08/campus4.png" title="" alt="" class="campus_img">
                                    </div>
                                    <div class="section_bottom">
                                        <h3><span class="span-block"> םיקסע ילעבל םוליצ תנדס </span></h3>
                                        <p> רצק טפשמ הנשמ תרתוכ קמעב םירבעמ ,קמעה לדגמ </p>
                                        <span class="campus_time">
                                            <span>  ישימח םוי </span> <span>11:00</span>
                                        </span>
                                    </div>
                                </div>
                            </div>    

                            <div class="grid-lg-25 grid-lg-auto">
                                <div class="campus_box">
                                    <div class="campus_post_datetime">
                                        <span class="digit-number">2</span> <span class="txt-span">ילוי</span>
                                    </div>
                                    <div class="overlay_block">
                                        <p>פרטים נוספים</p>
                                        <p><i class="icon-arrow-left"></i></p>
                                    </div>
                                    <div class="section_top">
                                        <img src="http://13.127.108.199/dev/wordpress/mavvarim/wp-content/uploads/2019/08/campus3.png" title="" alt="" class="campus_img">
                                    </div>
                                    <div class="section_bottom">
                                        <h3><span class="span-block"> ילטיגיד קוויש סרוק</span></h3>
                                        <p>רצק טפשמ הנשמ תרתוכ  קמעב םירבעמ ,קמעה לדגמ </p>
                                        <span class="campus_time">
                                            <span>ישימח םוי</span> <span>2.7.19</span> <span>08:00-13:00</span>
                                        </span>
                                    </div>
                                </div>
                            </div>    

                            <div class="grid-lg-25 grid-lg-auto">
                                <div class="campus_box">
                                    <div class="campus_post_datetime">
                                        <span class="digit-number">14</span> <span class="txt-span">גוא</span>
                                    </div>
                                    <div class="overlay_block">
                                        <p>פרטים נוספים</p>
                                        <p><i class="icon-arrow-left"></i></p>
                                    </div>
                                    <div class="section_top">
                                        <img src="http://13.127.108.199/dev/wordpress/mavvarim/wp-content/uploads/2019/08/campus2.png" title="" alt="" class="campus_img">
                                    </div>
                                    <div class="section_bottom">
                                        <h3><span class="span-block"> ןידאקניל תנדס </span></h3>
                                        <p> יתש ךרואב לבא רצק טפשמ הנשמ תרתוכ   ךירצ םא תורוש  תונייעמה קמע </p>
                                        <span class="campus_time">
                                            <span>  ינש םוי </span> <span>14.8.19</span> <span>20:00</span>
                                        </span>
                                    </div>
                                </div>
                            </div>   

                            <div class="grid-lg-25 grid-lg-auto">
                                <div class="campus_box">
                                    <div class="campus_post_datetime">
                                        <span class="digit-number">30</span> <span class="txt-span">קוא</span>
                                    </div>
                                    <div class="overlay_block">
                                        <p>פרטים נוספים</p>
                                        <p><i class="icon-arrow-left"></i></p>
                                    </div>
                                    <div class="section_top">
                                        <img src="http://13.127.108.199/dev/wordpress/mavvarim/wp-content/uploads/2019/08/campus1.png" title="" alt="" class="campus_img">
                                    </div>
                                    <div class="section_bottom">
                                        <h3><span class="span-block"> זנשקנוק קובסייפ תנדס </span></h3>
                                        <p>רצק טפשמ הנשמ תרתוכ ודיגמ הצעומה יניינב </p>
                                        <span class="campus_time">
                                            <span> ישימח םוי </span> <span>30.10.19</span> <span>11:00</span>
                                        </span>
                                    </div>
                                </div>
                            </div>

                        </div>    
                    </div>

                </div>    
            </div>        
        </div>       

        <div class="company-div nextjob-root-div">
            <div class="container">

                <div class="clearfix"></div>

                <div class="tab-container">

                    <div class="tablist-row">
                        <div class="tab-heading collapsible">
                            <div class="tab-box text-heading">
                                <h3> חיפוש <span class="dropdown-arrow"></span> </h3>
                                
                            </div>
                        </div>
                        <div class="tab tab-list-left content">
                            
                            <div class="tab-box margin-right-14">
                                <button class="tablinks" onclick="opentab(event, 'tab1')" id="defaultOpen"><span class="span-block">עבודה מועדפת</span><span class="span-block">חיילים משוחררים</span></button>
                            </div>
                            <div class="tab-box">
                                <button class="tablinks" onclick="opentab(event, 'tab2')"><span class="span-block"></span>משרות אם<span class="span-block">הורים עובדים</span></button>
                            </div>    
                            <div class="tab-box">
                                <button class="tablinks" onclick="opentab(event, 'tab3')"><span class="span-block">משרות</span><span class="span-block">לסטודנטים</span></button>
                            </div>    
                            <div class="tab-box">
                                <button class="tablinks" onclick="opentab(event, 'tab4')"><span class="span-block">משרות</span><span class="span-block">בחקלאות</span></button>
                            </div>    
                            <div class="tab-box">
                                <button class="tablinks" onclick="opentab(event, 'tab5')"><span class="span-block">ניהול</span><span class="span-block">בקהילה</span></button>
                            </div>    
                            <div class="tab-box">
                                <button class="tablinks" onclick="opentab(event, 'tab6')"><span class="span-block">משרות </span><span class="span-block">לבכירים</span></button>
                            </div>    
                        </div>
                    </div>    

                    <div id="tab1" class="tabcontent">

                    <div class="business-white-forms01">
    
                        <div class="white-body-form-div white-body-space-form-div">

                            <div class="white-row-color-div form-white-minus-300 form-white2">
                                <div class="form-row form-row-100">
                                    <div class="grid-auto grid-20">
                                        <div class="form-group">
                                            <label class="col-md-4 control-label">תחום מקצוע:</label>
                                            <div class="input-field-group">
                                                <select>
                                                    <option value="">מקצוע 1</option>
                                                    <option value="">מקצוע 2</option>
                                                    <option value="">מקצוע 3</option>
                                                    <option value="">מקצוע 4</option>
                                                    <option value="">מקצוע 5</option>
                                                    <option value="">מקצוע 6</option>
                                                    <option value="">מקצוע 7</option>
                                                    <option value="">מקצוע 8</option>
                                                </select>
                                                <span class="dropdown-arrow"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="grid-auto grid-20">
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="">תפקיד:</label>
                                            <div class="input-field-group">
                                                <select >
                                                    <option value="">כל התפקידים</option>
                                                    <option value="">כל התפקידים</option>
                                                    <option value="">כל התפקידים</option>
                                                </select>
                                                <span class="dropdown-arrow"></span>
                                            </div>
                                        </div>
                                    </div>						
                                    <div class="grid-auto grid-20">
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="">היקף משרה:</label>
                                            <div class="input-field-group">
                                                <select >
                                                    <option value="">משרה מלאה</option>
                                                    <option value="">משרה מלאה</option>
                                                    <option value="">משרה מלאה</option>
                                                </select>
                                                <span class="dropdown-arrow"></span>
                                            </div>
                                        </div>
                                    </div>	
                                    
                                    <div class="grid-auto grid-20">
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="">אזור:</label>
                                            <div class="input-field-group">
                                                <select >
                                                    <option value="">כל האזורים</option>
                                                    <option value="">כל האזורים</option>
                                                    <option value="">כל האזורים</option>
                                                </select>
                                                <span class="dropdown-arrow"></span>
                                            </div>
                                        </div>
                                    </div>	

                                    <div class="grid-auto grid-20">
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="">סוג משרה:</label>
                                            <div class="input-field-group">
                                                <select >
                                                    <option value="">כל הסוגים</option>
                                                    <option value="">כל הסוגים</option>
                                                    <option value="">כל הסוגים</option>
                                                </select>
                                                <span class="dropdown-arrow"></span>
                                            </div>
                                        </div>
                                    </div>	
                                </div>
                            </div>

                            <div class="clearfix"></div>

                            <div class="white-row-color-div form-white-minus-300">
                            
                                <div class="form-row form-row-15 form-col-100">
                                    <div class="grid-auto grid-100">
                                        <div class="form-group">
                                            <label class="control-label">טקסט חופשי:</label>
                                            <div class="input-field-group">
                                                <input type="text" placeholder="הכנס את המלל כאן..." class="form-control input-md">
                                            </div>
                                        </div>
                                    </div>						
                                </div>
                                                    
                            </div>
                        
                            <div class="form-row form-row-btn-300">
                                <div class="button-abs-bl-auto">
                                    <div class="btn-submit-group">
                                        <button class="btn btn-submit" id="" name="" data-value="field0" type="button">חיפוש משרה<i class="icon-arrow-left"></i></button>
                                    </div>
                                </div>
                            </div>		

                            <div class="clearfix"></div>
                                
                            
                        </div>  

                    </div>
                        
                    </div>

                    <div id="tab2" class="tabcontent">
                        
                    </div>

                    <div id="tab3" class="tabcontent">

                    </div>

                    <div id="tab4" class="tabcontent">

                    </div>

                    <div id="tab5" class="tabcontent">

                    </div>

                    <div id="tab6" class="tabcontent">

                    </div>

                </div>

                <div class="clearfix"></div>

            </div>
        </div> 

        <div class="article-prof-info-root nextjob-prof-info-root">
            <div class="campus-main-div article-prof-info-main-div nextjob-prof-info-main-div">
                <div class="container">

                    <div class="clearfix"></div>

                    <div class="light-box-content-div light-box-content-btn-div">
                        <div class="light-header-div collapsible2">
                            <div class="right-light-header">
                                <h3>לקיבוץ בעמק יזרעאל דרוש/ה רכז/ת מועדון נוער ומדריכה לחינוך הגל הרך</h3>
                            </div>
                            <div class="left-light-header">
                                <span class="text-span">10.7.2019</span>
                                <a  class="link-btn"><i class="edit-plus-icon edit-plus-black"></i></a>
                            </div>
                        </div>
                        <div class="collapse-div">

                            <div class="light-body-div light-body-div01">
                                <div class="div-row-line">
                                    <div class="col01"><h6>55676</h6></div>
                                    <div class="col02"> <p> עמק יזרעאל (עפולה, נצרת, מגדל העמק, טבעון)</p></div>
                                </div>
                                
                            </div>

                            <div class="light-body-div">
                                
                                <div class="div-row-line">
                                    <div class="auto-row">
                                        <div class="auto-col"><span class="font16 bold-span">תחום מקצוע</span><span class="font16 semibold-span">הוראה, חינוך והדרכה </span></div>
                                        <div class="auto-col"><span class="font16 bold-span">תפקיד</span><span class="font16 semibold-span"> רכז </span></div>
                                        <div class="auto-col"><span class="font16 bold-span">השכלה</span><span class="font16 semibold-span"> תואר ראשון </span></div>
                                        <div class="auto-col"><span class="font16 bold-span">מס׳ שנות ניסיון</span><span class="font16 semibold-span"> 2 </span></div>
                                        <div class="auto-col"><span class="font16 bold-span"> היקף משרה </span><span class="font16 semibold-span"> חלקי </span></div>
                                    </div>
                                </div>
                                <div class="div-row-line">
                                    <div class="auto-row">
                                        <div class="auto-col auto-col-50">
                                            <h3 class="font16 bold-span"> דרישות ֿ(השכלה, יכולות וניסיון קודם): </h3>
                                            <p>מנהל/ת פיתוח מקצועי – מורי תכנית הילה.</p>
                                            <p>מנהל התכנית יוציא לפועל של תכניות מורכבות בתחום הפיתוח המקצועי. במסגרת זו – תאום </p>
                                            <p>הנושאים הפדגוגיים, המנהליים והניהוליים מול הגופים המזמינים.</p>
                                            <p>אחריות תקציבית.</p>
                                            <p>אחריות על פיתוח התחום מול גורמי משרד החינוך והשותפים לפרויקט.</p>
                                            <p>כפיפות למנהל תחום השדה החינוכי בק.מ.ה</p>
                                            <p>50-80% משרה.</p>
                                        </div>
                                        <div class="auto-col auto-col-50">
                                            <h3 class="font16 bold-span"> דרישות ֿ(השכלה, יכולות וניסיון קודם): </h3>
                                            <p>qיכולת תכנון וביצוע תוך ירידה לפרטים, כולל ניהול שורות תקציב תוך שימוש במערכות מחשוב מתאימות.</p>
                                            <p>ניסיון בהוצאה לפועל של השתלמויות</p>
                                            <p>הכרות עם תחום הפיתוח המקצועי של עובדי הוראה – רצוי.</p>
                                            <p>תואר ראשון הכרחי, תואר שני יתרון.</p>
                                        </div>

                                        <div class="clearfix"></div>

                                        <div class="auto-col auto-col-50">
                                            <h3 class="font16 bold-span"> הערות </h3>
                                            <p>כפיפות למנהל אגף הנוער מגדל העמק.</p>
                                        </div>
                                    </div>
                                </div>  
                                <div class="div-row-line">
                                    
                                    <div class="auto-row checklist-auto-new">
                                        <h3 class="font16 bold-span"> שאלות לבדיקת התאמה לתפקיד: </h3>
                                        <div class="auto-col padd-left-auto"> 
                                            <div class="form-group checklist-group-new">
                                                <label class="control-label">האם יש לך ניסיון עבודה עם בני נוער?</label>
                                                <div class="input-field-group checkbox-group auto-checkbox-group">
                                                    <div class="checkbox-col">
                                                        <label for="job_position_favorite" class="wpuf-checkbox-inline">
                                                            <input type="checkbox" id="job_position_favorite" name="job_position[]" value="1">
                                                            כן
                                                        </label>
                                                    </div>
                                                    <div class="checkbox-col">
                                                        <label for="job_position_parent" class="wpuf-checkbox-inline">
                                                            <input type="checkbox" id="job_position_parent" name="job_position[]" value="2">
                                                            לא
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="auto-col padd-left-auto"> 
                                            <div class="form-group checklist-group-new">
                                                <label class="control-label">האם יש לך ניסיון עבודה עם בני נוער?</label>
                                                <div class="input-field-group checkbox-group auto-checkbox-group">
                                                    <div class="checkbox-col">
                                                        <label for="job_position_favorite" class="wpuf-checkbox-inline">
                                                            <input type="checkbox" id="job_position_favorite" name="job_position[]" value="1">
                                                            כן
                                                        </label>
                                                    </div>
                                                    <div class="checkbox-col">
                                                        <label for="job_position_parent" class="wpuf-checkbox-inline">
                                                            <input type="checkbox" id="job_position_parent" name="job_position[]" value="2">
                                                            לא
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="auto-col padd-left-auto"> 
                                            <div class="form-group checklist-group-new">
                                                <label class="control-label">האם יש לך ניסיון עבודה עם בני נוער?</label>
                                                <div class="input-field-group checkbox-group auto-checkbox-group">
                                                    <div class="checkbox-col">
                                                        <label for="job_position_favorite" class="wpuf-checkbox-inline">
                                                            <input type="checkbox" id="job_position_favorite" name="job_position[]" value="1">
                                                            כן
                                                        </label>
                                                    </div>
                                                    <div class="checkbox-col">
                                                        <label for="job_position_parent" class="wpuf-checkbox-inline">
                                                            <input type="checkbox" id="job_position_parent" name="job_position[]" value="2">
                                                            לא
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>    
                            </div>

                            <div class="light-button-div">
                                <div class="btn-div4"> 
                                    <button class="btn btn-color-4" id=""> <i class="send-icon icon-black pl-5"></i> שליחת קורות חיים <i class="icon-arrow-left"></i></button>    
                                </div>
                                <div class="btn-div4"> 
                                    <button class="btn btn-color-4" id=""> <i class="chain-icon icon-black pl-5"></i>העתק לינק משרה <i class="icon-arrow-left"></i></button>    
                                </div>
                                <div class="btn-div4"> 
                                    <button class="btn btn-color-4" id=""> <i class="social-icon icon-s04 icon-black pl-5"></i>שלח משרה בוואטסאפ <i class="icon-arrow-left"></i></button>    
                                </div>
                                <div class="btn-div4"> 
                                    <button class="btn btn-color-4" id=""> <i class="social-icon icon-s02 icon-black pl-5"></i> שלח משרה בפייסבוק <i class="icon-arrow-left"></i></button>    
                                </div>
                            </div>
                        </div>
                        
                    </div>

                    <div class="light-box-content-div light-box-content-btn-div">
                        <div class="light-header-div collapsible2">
                            <div class="right-light-header">
                                <h3>לקיבוץ בעמק יזרעאל דרוש/ה רכז/ת מועדון נוער ומדריכה לחינוך הגל הרך</h3>
                            </div>
                            <div class="left-light-header">
                                <span class="text-span">10.7.2019</span>
                                <a  class="link-btn"><i class="edit-plus-icon edit-plus-black"></i></a>
                            </div>
                        </div>
                        <div class="collapse-div">

                            <div class="light-body-div light-body-div01">
                                <div class="div-row-line">
                                    <div class="col01"><h6>55676</h6></div>
                                    <div class="col02"> <p> עמק יזרעאל (עפולה, נצרת, מגדל העמק, טבעון)</p></div>
                                </div>
                                
                            </div>

                            <div class="light-body-div">
                                
                                <div class="div-row-line">
                                    <div class="auto-row">
                                        <div class="auto-col"><span class="font16 bold-span">תחום מקצוע</span><span class="font16 semibold-span">הוראה, חינוך והדרכה </span></div>
                                        <div class="auto-col"><span class="font16 bold-span">תפקיד</span><span class="font16 semibold-span"> רכז </span></div>
                                        <div class="auto-col"><span class="font16 bold-span">השכלה</span><span class="font16 semibold-span"> תואר ראשון </span></div>
                                        <div class="auto-col"><span class="font16 bold-span">מס׳ שנות ניסיון</span><span class="font16 semibold-span"> 2 </span></div>
                                        <div class="auto-col"><span class="font16 bold-span"> היקף משרה </span><span class="font16 semibold-span"> חלקי </span></div>
                                    </div>
                                </div>
                                <div class="div-row-line">
                                    <div class="auto-row">
                                        <div class="auto-col auto-col-50">
                                            <h3 class="font16 bold-span"> דרישות ֿ(השכלה, יכולות וניסיון קודם): </h3>
                                            <p>מנהל/ת פיתוח מקצועי – מורי תכנית הילה.</p>
                                            <p>מנהל התכנית יוציא לפועל של תכניות מורכבות בתחום הפיתוח המקצועי. במסגרת זו – תאום </p>
                                            <p>הנושאים הפדגוגיים, המנהליים והניהוליים מול הגופים המזמינים.</p>
                                            <p>אחריות תקציבית.</p>
                                            <p>אחריות על פיתוח התחום מול גורמי משרד החינוך והשותפים לפרויקט.</p>
                                            <p>כפיפות למנהל תחום השדה החינוכי בק.מ.ה</p>
                                            <p>50-80% משרה.</p>
                                        </div>
                                        <div class="auto-col auto-col-50">
                                            <h3 class="font16 bold-span"> דרישות ֿ(השכלה, יכולות וניסיון קודם): </h3>
                                            <p>qיכולת תכנון וביצוע תוך ירידה לפרטים, כולל ניהול שורות תקציב תוך שימוש במערכות מחשוב מתאימות.</p>
                                            <p>ניסיון בהוצאה לפועל של השתלמויות</p>
                                            <p>הכרות עם תחום הפיתוח המקצועי של עובדי הוראה – רצוי.</p>
                                            <p>תואר ראשון הכרחי, תואר שני יתרון.</p>
                                        </div>

                                        <div class="clearfix"></div>

                                        <div class="auto-col auto-col-50">
                                            <h3 class="font16 bold-span"> הערות </h3>
                                            <p>כפיפות למנהל אגף הנוער מגדל העמק.</p>
                                        </div>
                                    </div>
                                </div>  
                                <div class="div-row-line">
                                    
                                    <div class="auto-row checklist-auto-new">
                                        <h3 class="font16 bold-span"> שאלות לבדיקת התאמה לתפקיד: </h3>
                                        <div class="auto-col padd-left-auto"> 
                                            <div class="form-group checklist-group-new">
                                                <label class="control-label">האם יש לך ניסיון עבודה עם בני נוער?</label>
                                                <div class="input-field-group checkbox-group auto-checkbox-group">
                                                    <div class="checkbox-col">
                                                        <label for="job_position_favorite" class="wpuf-checkbox-inline">
                                                            <input type="checkbox" id="job_position_favorite" name="job_position[]" value="1">
                                                            כן
                                                        </label>
                                                    </div>
                                                    <div class="checkbox-col">
                                                        <label for="job_position_parent" class="wpuf-checkbox-inline">
                                                            <input type="checkbox" id="job_position_parent" name="job_position[]" value="2">
                                                            לא
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="auto-col padd-left-auto"> 
                                            <div class="form-group checklist-group-new">
                                                <label class="control-label">האם יש לך ניסיון עבודה עם בני נוער?</label>
                                                <div class="input-field-group checkbox-group auto-checkbox-group">
                                                    <div class="checkbox-col">
                                                        <label for="job_position_favorite" class="wpuf-checkbox-inline">
                                                            <input type="checkbox" id="job_position_favorite" name="job_position[]" value="1">
                                                            כן
                                                        </label>
                                                    </div>
                                                    <div class="checkbox-col">
                                                        <label for="job_position_parent" class="wpuf-checkbox-inline">
                                                            <input type="checkbox" id="job_position_parent" name="job_position[]" value="2">
                                                            לא
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="auto-col padd-left-auto"> 
                                            <div class="form-group checklist-group-new">
                                                <label class="control-label">האם יש לך ניסיון עבודה עם בני נוער?</label>
                                                <div class="input-field-group checkbox-group auto-checkbox-group">
                                                    <div class="checkbox-col">
                                                        <label for="job_position_favorite" class="wpuf-checkbox-inline">
                                                            <input type="checkbox" id="job_position_favorite" name="job_position[]" value="1">
                                                            כן
                                                        </label>
                                                    </div>
                                                    <div class="checkbox-col">
                                                        <label for="job_position_parent" class="wpuf-checkbox-inline">
                                                            <input type="checkbox" id="job_position_parent" name="job_position[]" value="2">
                                                            לא
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>    
                            </div>
                            <div class="light-button-div">
                                <div class="btn-div4"> 
                                    <button class="btn btn-color-4" id=""> <i class="send-icon icon-black pl-5"></i> שליחת קורות חיים <i class="icon-arrow-left"></i></button>    
                                </div>
                                <div class="btn-div4"> 
                                    <button class="btn btn-color-4" id=""> <i class="chain-icon icon-black pl-5"></i>העתק לינק משרה <i class="icon-arrow-left"></i></button>    
                                </div>
                                <div class="btn-div4"> 
                                    <button class="btn btn-color-4" id=""> <i class="social-icon icon-s04 icon-black pl-5"></i>שלח משרה בוואטסאפ <i class="icon-arrow-left"></i></button>    
                                </div>
                                <div class="btn-div4"> 
                                    <button class="btn btn-color-4" id=""> <i class="social-icon icon-s02 icon-black pl-5"></i> שלח משרה בפייסבוק <i class="icon-arrow-left"></i></button>    
                                </div>
                            </div>
                        </div>
                        
                    </div>

                    <div class="light-box-content-div light-box-content-btn-div">
                        <div class="light-header-div collapsible2 active">
                            <div class="right-light-header">
                                <h3>דרוש/ה מנהל/ת פיתוח מקצועי לתכנית 'הילה' במכללת אורנים JB-31</h3>
                            </div>
                            <div class="left-light-header">
                                <span class="text-span">10.7.2019</span>
                                <a  class="link-btn"><i class="edit-plus-icon edit-plus-black"></i></a>
                            </div>
                        </div>
                        <div class="collapse-div content-show-hide" style="max-height: 100%;">

                            <div class="light-body-div light-body-div01">
                                <div class="div-row-line">
                                    <div class="col01"><h6>55676</h6></div>
                                    <div class="col02"> <p> עמק יזרעאל (עפולה, נצרת, מגדל העמק, טבעון)</p></div>
                                </div>
                                <a href="#" class="btn btn-like"><i class="like-icon icon-black"></i></a>
                            </div>

                            <div class="light-body-div">
                                
                                <div class="div-row-line">
                                    <div class="auto-row">
                                        <div class="auto-col"><span class="font16 bold-span">תחום מקצוע</span><span class="font16 semibold-span">הוראה, חינוך והדרכה </span></div>
                                        <div class="auto-col"><span class="font16 bold-span">תפקיד</span><span class="font16 semibold-span"> רכז </span></div>
                                        <div class="auto-col"><span class="font16 bold-span">השכלה</span><span class="font16 semibold-span"> תואר ראשון </span></div>
                                        <div class="auto-col"><span class="font16 bold-span">מס׳ שנות ניסיון</span><span class="font16 semibold-span"> 2 </span></div>
                                        <div class="auto-col"><span class="font16 bold-span"> היקף משרה </span><span class="font16 semibold-span"> חלקי </span></div>
                                    </div>
                                </div>
                                <div class="div-row-line">
                                    <div class="auto-row">
                                        <div class="auto-col auto-col-50">
                                            <h3 class="font16 bold-span"> דרישות ֿ(השכלה, יכולות וניסיון קודם): </h3>
                                            <p>מנהל/ת פיתוח מקצועי – מורי תכנית הילה.</p>
                                            <p>מנהל התכנית יוציא לפועל של תכניות מורכבות בתחום הפיתוח המקצועי. במסגרת זו – תאום </p>
                                            <p>הנושאים הפדגוגיים, המנהליים והניהוליים מול הגופים המזמינים.</p>
                                            <p>אחריות תקציבית.</p>
                                            <p>אחריות על פיתוח התחום מול גורמי משרד החינוך והשותפים לפרויקט.</p>
                                            <p>כפיפות למנהל תחום השדה החינוכי בק.מ.ה</p>
                                            <p>50-80% משרה.</p>
                                        </div>
                                        <div class="auto-col auto-col-50">
                                            <h3 class="font16 bold-span"> דרישות ֿ(השכלה, יכולות וניסיון קודם): </h3>
                                            <p>qיכולת תכנון וביצוע תוך ירידה לפרטים, כולל ניהול שורות תקציב תוך שימוש במערכות מחשוב מתאימות.</p>
                                            <p>ניסיון בהוצאה לפועל של השתלמויות</p>
                                            <p>הכרות עם תחום הפיתוח המקצועי של עובדי הוראה – רצוי.</p>
                                            <p>תואר ראשון הכרחי, תואר שני יתרון.</p>
                                        </div>

                                        <div class="clearfix"></div>

                                        <div class="auto-col auto-col-50">
                                            <h3 class="font16 bold-span"> הערות </h3>
                                            <p>כפיפות למנהל אגף הנוער מגדל העמק.</p>
                                        </div>
                                    </div>
                                </div>  
                                <div class="div-row-line">
                                    
                                    <div class="auto-row checklist-auto-new">
                                        <h3 class="font16 bold-span"> שאלות לבדיקת התאמה לתפקיד: </h3>
                                        <div class="auto-col padd-left-auto"> 
                                            <div class="form-group checklist-group-new">
                                                <label class="control-label">האם יש לך ניסיון עבודה עם בני נוער?</label>
                                                <div class="input-field-group checkbox-group auto-checkbox-group">
                                                    <div class="checkbox-col">
                                                        <label for="job_position_favorite" class="wpuf-checkbox-inline">
                                                            <input type="checkbox" id="job_position_favorite" name="job_position[]" value="1">
                                                            כן
                                                        </label>
                                                    </div>
                                                    <div class="checkbox-col">
                                                        <label for="job_position_parent" class="wpuf-checkbox-inline">
                                                            <input type="checkbox" id="job_position_parent" name="job_position[]" value="2">
                                                            לא
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="auto-col padd-left-auto"> 
                                            <div class="form-group checklist-group-new">
                                                <label class="control-label">האם יש לך ניסיון עבודה עם בני נוער?</label>
                                                <div class="input-field-group checkbox-group auto-checkbox-group">
                                                    <div class="checkbox-col">
                                                        <label for="job_position_favorite" class="wpuf-checkbox-inline">
                                                            <input type="checkbox" id="job_position_favorite" name="job_position[]" value="1">
                                                            כן
                                                        </label>
                                                    </div>
                                                    <div class="checkbox-col">
                                                        <label for="job_position_parent" class="wpuf-checkbox-inline">
                                                            <input type="checkbox" id="job_position_parent" name="job_position[]" value="2">
                                                            לא
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="auto-col padd-left-auto"> 
                                            <div class="form-group checklist-group-new">
                                                <label class="control-label">האם יש לך ניסיון עבודה עם בני נוער?</label>
                                                <div class="input-field-group checkbox-group auto-checkbox-group">
                                                    <div class="checkbox-col">
                                                        <label for="job_position_favorite" class="wpuf-checkbox-inline">
                                                            <input type="checkbox" id="job_position_favorite" name="job_position[]" value="1">
                                                            כן
                                                        </label>
                                                    </div>
                                                    <div class="checkbox-col">
                                                        <label for="job_position_parent" class="wpuf-checkbox-inline">
                                                            <input type="checkbox" id="job_position_parent" name="job_position[]" value="2">
                                                            לא
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>    
                            </div>
                            <div class="light-button-div">
                                <div class="btn-div4"> 
                                    <button class="btn btn-color-4" id=""> <i class="send-icon icon-black pl-5"></i> שליחת קורות חיים <i class="icon-arrow-left"></i></button>    
                                </div>
                                <div class="btn-div4"> 
                                    <button class="btn btn-color-4" id=""> <i class="chain-icon icon-black pl-5"></i>העתק לינק משרה <i class="icon-arrow-left"></i></button>    
                                </div>
                                <div class="btn-div4"> 
                                    <button class="btn btn-color-4" id=""> <i class="social-icon icon-s04 icon-black pl-5"></i>שלח משרה בוואטסאפ <i class="icon-arrow-left"></i></button>    
                                </div>
                                <div class="btn-div4"> 
                                    <button class="btn btn-color-4" id=""> <i class="social-icon icon-s02 icon-black pl-5"></i> שלח משרה בפייסבוק <i class="icon-arrow-left"></i></button>    
                                </div>
                            </div>
                        </div>
                        
                    </div>

                </div>
            </div>
        </div>


        
        <script>

        function opentab(evt, tabName) {
            var i, tabcontent, tablinks;
            tabcontent = document.getElementsByClassName("tabcontent");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }
            tablinks = document.getElementsByClassName("tablinks");
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" active", "");
            }
            document.getElementById(tabName).style.display = "block";
            evt.currentTarget.className += " active";
        }

        document.getElementById("defaultOpen").click();

        
        var coll = document.getElementsByClassName("collapsible");
        var i;


        for (i = 0; i < coll.length; i++) {
            
        coll[i].addEventListener("click", function() {
            this.classList.toggle("active");
            jQuery(".content").toggleClass("active-content");
        });
        }

        var coll2 = document.getElementsByClassName("collapsible2");
        var i2;

        for (i2 = 0; i2 < coll2.length; i2++) {
        coll2[i2].addEventListener("click", function() {
            this.classList.toggle("active");
            
            var content2 = this.nextElementSibling;
            if (content2.style.maxHeight){
                content2.style.maxHeight = null;
                jQuery(".collapse-div").removeClass("content-show-hide");
            } else {
                jQuery(".collapse-div").addClass("content-show-hide");
                content2.style.maxHeight = content2.scrollHeight + "px";
            } 
        });
        }
        

        </script>



    </section>


<?php get_footer(); ?>